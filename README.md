
# The Zoo of Lambda-Calculus Reduction Strategies, and Coq

The code was tested in The Coq Proof Assistant, version 8.18.0.

Compile the whole project with `./make.sh`.

You can run CoqIDE conveniently with `./run_coqide.sh&`.
The files will be opened with the dependency order.

To search for the phrase " cbn " in project files you can use this command :
```
grep " cbn " `cat _CoqProject | tail -n +2` -n
```

Contributions are welcomed.
Don't hesitate to contact [Tomasz Drab](https://ii.uni.wroc.pl/~tdr/) if you have an idea how to improve the repository, simplify the code etc.

## Updated figures

![Figure 1.](figures/figure1.svg)

<br/>

![Figure 5.](figures/figure5.svg)

<br/>

![Contrices](figures/contrices.svg)
![Figure 6.](figures/figure6.svg)

<br/>

![Figure 7.](figures/figure7.svg)

## Presentation

[![The Zoo of Lambda-Calculus Reduction Strategies, and Coq.](figures/thumbnail.png)](https://www.youtube.com/watch?v=XURF1EYnu4E)
