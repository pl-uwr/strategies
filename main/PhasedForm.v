Require Import Utf8.
Require Import Coq.Program.Basics Coq.Classes.Morphisms.
Require Import Coq.Lists.List.
Require Import Coq.Strings.String.
Require Import Preliminaries Common Term Context Decomposition Phased Decider Decompose.
Import ListNotations.

Ltac strategy_tauto    S_phased   := unfold S_phased     ; cbv;      union_tauto.
Ltac strategy_tauto_in S_phased H := unfold S_phased in H; cbv in H; union_tauto_in H.

Ltac std_phased_lemma_var_case S_phased :=
  split;
  [ intros [C c] H; var_frames_tac H
  | apply true_iff_iff, normal_var];
  strategy_tauto S_phased.

Ltac std_phased_lemma_lam_strong_case S_phased IHnt IHt:=
  let C := fresh "C" in let c  := fresh "c" in
  let H := fresh "H" in let H0 := fresh "H" in let H1 := fresh "H" in
  split; [intros [C c] H; lam_frames_tac H; strategy_tauto S_phased|];
  split; intros H;
  [apply IHnt in H|apply IHnt]; clear IHnt;
  [apply normal_lam; split;[strategy_tauto S_phased|]
  |apply normal_lam, proj2 in H];
  [intros C c H0 H1|intros [[C c] [H0 H1]]];
  specialize (IHt (_,_) H0);
  [|specialize (H _ _ H0)];
  apply H; clear H;
  [|right; apply IHt, H1];
  exists (C, c); split;[exact H0|];
  destruct H1 as [H1|H1]; [strategy_tauto_in S_phased H1|];
  apply IHt, H1.

Ltac neutral_form_tac normal_form_lemma normal_form :=
  symmetry; intros t; split;
  [ intros H t2 H0;
    apply normal_form_lemma;
    apply normal_form_lemma in H0;
    try split; assumption
  | intros H;
    let H0 := fresh "H" in
    enough (H0 : normal_form (app t (var "x"))) by apply H0;
    apply normal_form_lemma, H, normal_form_lemma, I ].

Lemma head_phased_lemma : phased_lemma head head_phased hnf.
Proof.
  phased_lemma_by_induction head.
  + std_phased_lemma_var_case head_phased.
  + std_phased_lemma_lam_strong_case head_phased IHnt IHt.
  + split; [|split].
    - intros [C c] H. app_frames_tac H; split; try (strategy_tauto head_phased).
      * intros [[_ H] H0].
        assert (H1 : cbn (x, c)) by exact (conj H H0).
        left. right. split.
        { apply normal_only_β_contraction_app.
          intros H2.
          assert (H3 : whnf (plug x c)) by exact (or_introl H2).
          apply normal_cbn_whnf in H3.
          apply H3. eexists. split; [|exact H1]. reflexivity. }
        apply cbn_is_head, H1.
      * intros [[[[] _]|[H [H0 H1]]]|[]].
        repeat split; [|assumption].
        apply normal_only_β_contraction_app in H.
        destruct x as [|[]]; try exact H0.
        exfalso. apply H; clear H. split; constructor.
    - intros H.
      apply normal_app. repeat split.
      * intros [[[_ [H0 _]]|[_ []]]|[]].
        exact (rigid_abstraction_disjoint t1 H H0).
      * intros C c H0 [[[[] _]|[_ H1]]|[]].
        apply IHt1; [apply hnf_grammar; left; exact H|].
        eexists (_, _). split; [|exact H1].
        simpl. f_equal. exact H0.
      * intros C c H0 [H1|[]]. cbv in H1; tauto.
    - intros H.
      apply normal_union_strategy, proj1,
            normal_form_sequence_strategy in H.
      assert(H0 := proj2 H).
      apply proj1, normal_only_β_contraction_app in H.
      apply IHt1, hnf_grammar in H0.
      destruct H0 as [H0|H0]; [exact H0|].
      exfalso. apply H; clear H.
      repeat split.
      eapply abs_of_full, (abs_of_monotone _ _ (subset_full _)), H0.
Qed.

Corollary head_phased_form : head == head_phased.
Proof. exact (phased_lemma_first_corollary _ _ _ head_phased_lemma). Qed.

Corollary normal_head_hnf : normal_form head == hnf.
Proof. exact (phased_lemma_second_corollary _ _ _ head_phased_lemma). Qed.

Corollary neutral_head_inert : neutral_form head == rigid.
Proof. neutral_form_tac normal_head_hnf hnf. Qed.

Lemma hnf_is_normal_hs : hnf ⊆ normal_form hs.
Proof.
  intros t. induction t; simpl.
  + intros _. apply normal_var. intros [_ H]. exact H.
  + intros H. specialize (IHt H); clear H. apply normal_lam. split.
    - exact (@proj2 _ _).
    - intros C c H [[_ HC] Hc]. apply IHt. exists (C, c). exact (conj H (conj HC Hc)).
  + intros rigid_t1.
    apply normal_app. repeat split.
    - intros [_ [abs_t1 _]].
      exact (rigid_abstraction_disjoint _ rigid_t1 abs_t1).
    - intros C c H [[_ HC] Hc].
      apply rigid_is_hnf in rigid_t1.
      apply (IHt1 rigid_t1). exists (C, c). repeat split; assumption.
    - intros _ _ _ [[[] _] _].
Qed.

Theorem normal_hs_hnf : normal_form hs == hnf.
Proof.
  apply two_inclusions. split; [|apply hnf_is_normal_hs].
  exact (transitivity
    (normal_form_variance _ _ head_is_hs)
    (proj1 (proj1 (two_inclusions _ _) normal_head_hnf))).
Qed.

Lemma hs_phased_form : hs == hs_phased.
Proof. strategies_equal_on_all_terms_tac; split; cbv; tauto. Qed.

Lemma ihs_phased_lemma : phased_lemma ihs ihs_phased hnf.
Proof.
  phased_lemma_by_induction ihs.
  + std_phased_lemma_var_case ihs_phased.
  + std_phased_lemma_lam_strong_case ihs_phased IHnt IHt.
  + split; [|split].
    - intros [C c] H. app_frames_tac H; split; try (strategy_tauto ihs_phased).
      * intros [_ H].
        left. right. split; [apply IHt1, (abs_of_hnf_is_hnf _ (proj1 H))|].
        split; [constructor|].
        apply hnfβ_contrex_is_β_contrex, H.
      * intros [[[]|[H [_ [H0 _]]]]|[]].
        apply IHt1 in H.
        repeat split. apply abs_of_hnf_abs_hnf. split; assumption.
    - intros H.
      apply normal_app. repeat split.
      * intros [[[]|[_ [_ [H0 _]]]]|[]].
        exact (rigid_abstraction_disjoint t1 H H0).
      * intros C c H0 [[H1|[_ [[] _]]]|[]].
        apply IHt1; [apply hnf_grammar; left; exact H|].
        eexists (_, _). split; [|exact H1].
        simpl. f_equal. exact H0.
      * intros C c H0 [H1|[]]. cbv in H1; tauto.
    - intros H.
      apply normal_union_strategy, proj1,
            normal_form_sequence_strategy in H.
      assert(H0 := H).
      apply proj1, IHt1, hnf_grammar in H.
      apply proj2, normal_only_β_contraction_app in H0.
      destruct H as [H|H]; [exact H|].
      exfalso. apply H0; clear H0.
      repeat split.
      eapply abs_of_full, (abs_of_monotone _ _ (subset_full _)), H.
Qed.

Corollary ihs_phased_form : ihs == ihs_phased.
Proof. exact (phased_lemma_first_corollary _ _ _ ihs_phased_lemma). Qed.

Corollary normal_ihs_hnf : normal_form ihs == hnf.
Proof. exact (phased_lemma_second_corollary _ _ _ ihs_phased_lemma). Qed.

Lemma low_phased_lemma : phased_lemma low low_phased2 wnf.
Proof.
  phased_lemma_by_induction low.
  + std_phased_lemma_var_case low_phased2.
  + split.
    - intros [C c] H.
      lam_frames_tac H; cbv; unfold plug in *; tauto.
    - apply true_iff_iff, normal_lam.
      split; [|intros C c H; subst]; cbv; tauto.
  + split; [|split].
    - intros [C c] H. app_frames_tac H; split; try (cbv; tauto).
      * intros H.
        right. split.
        { apply IHt1, abs_is_wnf, H. }
        left. exact H.
      * intros [[inert_t1 HC] Hc].
        right. split.
        { apply IHt1, inert_is_wnf, inert_t1. }
        right. split.
        { apply normal_only_β_contraction_app. intros abs_t1.
          apply inert_rigid_wnf, proj1 in inert_t1.
          exact (rigid_abstraction_disjoint _ inert_t1 abs_t1). }
        exact (conj HC Hc).
      * intros [[]|[wnf_t1 [[[] _]|[rigid_t1 [HC Hc]]]]].
        refine (conj (conj _ HC) Hc).
        apply normal_only_β_contraction_app in rigid_t1.
        apply IHt1, wnf_grammar in wnf_t1.
        destruct wnf_t1 as [abs_t1|inert_t1];
        [contradiction (rigid_t1 abs_t1)|exact inert_t1].
    - intros [inert_t1 wnf_t2].
      normal_form_sequence_strategy_tac.
      * apply IHt1, wnf_grammar, (or_intror inert_t1).
      * apply normal_only_β_contraction_app.
        exact (λ abs_t1, rigid_abstraction_disjoint _
              (proj1 (proj1 (inert_rigid_wnf _) inert_t1)) abs_t1).
      * apply IHt2, wnf_t2.
    - intros H.
      apply normal_form_sequence_strategy in H. destruct H as [wnf_t1 H].
      apply normal_form_sequence_strategy in H. destruct H as [rigid_t1 wnf_t2].
      apply IHt1 in wnf_t1.
      apply IHt2 in wnf_t2.
      apply normal_only_β_contraction_app in rigid_t1.
      split; [|exact wnf_t2].
      apply wnf_grammar in wnf_t1.
      destruct wnf_t1 as [abs_t1|inert_t1]; [|exact inert_t1].
      contradiction (rigid_t1 abs_t1).
Qed.

Corollary low_phased_form2 : low == low_phased2.
Proof. exact (phased_lemma_first_corollary _ _ _ low_phased_lemma). Qed.

Corollary low_phased_form : low == low_phased.
Proof.
  etransitivity; [exact low_phased_form2|].
  etransitivity; [apply sequence_strategy_assoc|].
  etransitivity; [|symmetry; apply sequence_strategy_assoc].
  apply sequence_strategy_proper; [|reflexivity].
  apply left_weak_strategy_β_contraction_commutative, low_is_weak.
Qed.

Corollary normal_low_wnf : normal_form low == wnf.
Proof. exact (phased_lemma_second_corollary _ _ _ low_phased_lemma). Qed.

Lemma l_cbw_phased_lemma : phased_lemma l_cbw l_cbw_phased wnf.
Proof.
  phased_lemma_by_induction l_cbw.
  + std_phased_lemma_var_case l_cbw_phased.
  + split.
    - intros [C c] H.
      lam_frames_tac H; cbv; unfold plug in *; tauto.
    - apply true_iff_iff, normal_lam.
      split; [|intros C c H; subst]; cbv; tauto.
  + split; [|split].
    - intros [C c] H. app_frames_tac H; split; try (cbv; tauto).
      * intros [_ [abs_t1 wnf_t2]].
        right. split; [apply IHt1, wnf_grammar, (or_introl abs_t1)|].
        right. split; [apply IHt2, wnf_t2|].
        repeat split. assumption.
      * cbn. intros [[? ?] ?]. right. split.
        { apply IHt1, H. }
        left. split; assumption.
    - intros [inert_t1 wnf_t2].
      normal_form_sequence_strategy_tac.
      * apply IHt1, wnf_grammar, (or_intror inert_t1).
      * apply IHt2, wnf_t2.
      * apply normal_only_β_contraction_app.
        exact (λ abs_t1, rigid_abstraction_disjoint _
              (proj1 (proj1 (inert_rigid_wnf _) inert_t1)) abs_t1).
    - intros H.
      apply normal_form_sequence_strategy in H. destruct H as [wnf_t1 H].
      apply normal_form_sequence_strategy in H. destruct H as [wnf_t2 rigid_t1].
      apply IHt1 in wnf_t1.
      apply IHt2 in wnf_t2.
      apply normal_only_β_contraction_app in rigid_t1.
      split; [|exact wnf_t2].
      apply wnf_grammar in wnf_t1.
      destruct wnf_t1 as [abs_t1|inert_t1]; [|exact inert_t1].
      contradiction (rigid_t1 abs_t1).
Qed.

Corollary l_cbw_phased_form : l_cbw == l_cbw_phased.
Proof. exact (phased_lemma_first_corollary _ _ _ l_cbw_phased_lemma). Qed.

Corollary normal_l_cbw_wnf : normal_form l_cbw == wnf.
Proof. exact (phased_lemma_second_corollary _ _ _ l_cbw_phased_lemma). Qed.

Lemma wnf_is_normal_weak : wnf ⊆ normal_form weak.
Proof.
  intros t. induction t; simpl.
  + intros _. apply normal_var. intros [_ H]. exact H.
  + intros _. apply normal_lam. split.
    - exact (@proj2 _ _).
    - intros C c H H0. exact (proj1 (proj1 H0)).
  + intros [H H0].
    apply inert_rigid_wnf in H.
    apply normal_app. repeat split; intros C c ? [[_ H1] H2] + intros [_ H2]; subst.
    - exact (rigid_abstraction_disjoint _ (proj1 H) (proj1 H2)).
    - apply (IHt1 (proj2 H)). exists (C, c). repeat split; assumption.
    - apply (IHt2 H0).        exists (C, c). repeat split; assumption.
Qed.

Theorem normal_weak_wnf : normal_form weak == wnf.
Proof.
  apply two_inclusions. split; [|apply wnf_is_normal_weak].
  exact (transitivity
    (normal_form_variance _ _ (transitivity l_cbw_is_cbw cbw_is_weak))
    (proj1 (proj1 (two_inclusions _ _) normal_l_cbw_wnf))).
Qed.

Corollary neutral_weak_inert :  neutral_form weak == inert.
Proof. neutral_form_tac normal_weak_wnf wnf. Qed.

Lemma weak_phased_form : weak == weak_phased.
Proof. strategies_equal_on_all_terms_tac; split; cbv; tauto. Qed.

Theorem normal_cbw_wnf : normal_form cbw == wnf.
Proof.
  apply two_inclusions. split.
  + etransitivity.
    - eapply normal_form_variance, l_cbw_is_cbw.
    - apply two_inclusions. symmetry. apply normal_l_cbw_wnf.
  + etransitivity.
    - eapply normal_form_variance, cbw_is_weak.
    - apply two_inclusions, normal_weak_wnf.
Qed.

Lemma cbw_phased_form : cbw == cbw_phased.
Proof.
  strategies_equal_on_all_terms_tac; split; try (cbv; tauto); simpl.
  + intros [H [H0 H1]]. right. repeat split; [|assumption].
    apply normal_union_strategy. split.
    - apply normal_left_strategy_app, normal_cbw_wnf, abs_is_wnf, H0.
    - apply normal_right_strategy_app, normal_cbw_wnf, H1.
  + intros [[[]|[]]|[H [_ [H0 _]]]].
    repeat split; [assumption|].
    apply normal_union_strategy, proj2,
          normal_right_strategy_app, normal_cbw_wnf in H.
    exact H.
Qed.

Lemma r_cbw_phased_lemma : phased_lemma r_cbw r_cbw_phased wnf.
Proof.
  phased_lemma_by_induction r_cbw.
  + std_phased_lemma_var_case r_cbw_phased.
  + split.
    - intros [C c] H.
      lam_frames_tac H; cbv; unfold plug in *; tauto.
    - apply true_iff_iff, normal_lam.
      split; [|intros C c H; subst]; cbv; tauto.
  + split; [|split].
    - intros [C c] H. app_frames_tac H; split; try (cbv; tauto).
      * intros [_ [abs_t1 wnf_t2]].
        right. split; [apply IHt2, wnf_t2|].
        right. split; [apply IHt1, wnf_grammar, (or_introl abs_t1)|].
        repeat split. assumption.
      * cbn. intros [[? ?] ?]. right. split.
        { apply IHt2, H. }
        left. split; assumption.
    - intros [inert_t1 wnf_t2].
      normal_form_sequence_strategy_tac.
      * apply IHt2, wnf_t2.
      * apply IHt1, wnf_grammar, (or_intror inert_t1).
      * apply normal_only_β_contraction_app.
        exact (λ abs_t1, rigid_abstraction_disjoint _
              (proj1 (proj1 (inert_rigid_wnf _) inert_t1)) abs_t1).
    - intros H.
      apply normal_form_sequence_strategy in H. destruct H as [wnf_t2 H].
      apply normal_form_sequence_strategy in H. destruct H as [wnf_t1 rigid_t1].
      apply IHt1 in wnf_t1.
      apply IHt2 in wnf_t2.
      apply normal_only_β_contraction_app in rigid_t1.
      split; [|exact wnf_t2].
      apply wnf_grammar in wnf_t1.
      destruct wnf_t1 as [abs_t1|inert_t1]; [|exact inert_t1].
      contradiction (rigid_t1 abs_t1).
Qed.

Corollary r_cbw_phased_form : r_cbw == r_cbw_phased.
Proof. exact (phased_lemma_first_corollary _ _ _ r_cbw_phased_lemma). Qed.

Corollary normal_r_cbw_wnf : normal_form r_cbw == wnf.
Proof. exact (phased_lemma_second_corollary _ _ _ r_cbw_phased_lemma). Qed.

Lemma LL_CBW_on_not_wnf : ∀ C c,
  ¬ wnf (plug C c) → LL_CBW C → β_contrex c → L_CBW C.
Proof.
  intros C c. apply GS_CBW_on_not_wnf; [|apply neu_is_inert|reflexivity].
  etransitivity; [|apply two_inclusions, L_CBW_grammar].
  repeat apply union_variance.
  + reflexivity.
  + apply Lapp_of_monotone; [|reflexivity].
    etransitivity; [apply neu_is_inert|apply inert_is_wnf].
  + apply Rapp_of_monotone; [reflexivity|apply subset_full].
Qed.

Lemma RL_CBW_on_not_wnf : ∀ C c,
  ¬ wnf (plug C c) → RL_CBW C → β_contrex c → L_CBW C.
Proof.
  intros C c. apply GS_CBW_on_not_wnf; [|reflexivity|apply nf_is_wnf].
  etransitivity; [|apply two_inclusions, L_CBW_grammar].
  repeat apply union_variance.
  + reflexivity.
  + apply Lapp_of_monotone; [apply inert_is_wnf|reflexivity].
  + apply Rapp_of_monotone; [reflexivity|apply subset_full].
Qed.

Lemma LR_CBW_on_not_wnf : ∀ C c,
  ¬ wnf (plug C c) → LR_CBW C → β_contrex c → R_CBW C.
Proof.
  intros C c. apply GS_CBW_on_not_wnf; [|apply neu_is_inert|reflexivity].
  etransitivity; [|apply two_inclusions, R_CBW_grammar].
  repeat apply union_variance.
  + reflexivity.
  + apply Lapp_of_monotone; [apply subset_full|reflexivity].
  + apply Rapp_of_monotone; [reflexivity|reflexivity].
Qed.

Lemma RR_CBW_on_not_wnf : ∀ C c,
  ¬ wnf (plug C c) → RR_CBW C → β_contrex c → R_CBW C.
Proof.
  intros C c. apply GS_CBW_on_not_wnf; [|reflexivity|apply nf_is_wnf].
  etransitivity; [|apply two_inclusions, R_CBW_grammar].
  repeat apply union_variance.
  + reflexivity.
  + apply Lapp_of_monotone; [apply subset_full|reflexivity].
  + apply Rapp_of_monotone; [reflexivity|apply nf_is_wnf].
Qed.

Lemma LL_CBW_on_inert : ∀ C c,
  inert (plug C c) → LL_CBW C → β_contrex c → RLL_CBW C.
Proof.
  intros C c H HC Hc.
  eapply GS_CBW_on_inert; try eassumption.
  clear HC. intros HC.
  apply inert_rigid_wnf, proj2, normal_weak_wnf in H.
  exact (H (ex_intro _ (_,_) (conj eq_refl (conj (L_CBW_is_Weak _ HC) Hc)))).
Qed.

Lemma RL_CBW_on_inert : ∀ C c,
  inert (plug C c) → RL_CBW C → β_contrex c → RRL_CBW C.
Proof.
  intros C c H HC Hc.
  eapply GS_CBW_on_inert; try eassumption.
  clear HC. intros HC.
  apply inert_rigid_wnf, proj2, normal_weak_wnf in H.
  exact (H (ex_intro _ (_,_) (conj eq_refl (conj (L_CBW_is_Weak _ HC) Hc)))).
Qed.

Lemma LR_CBW_on_inert : ∀ C c,
  inert (plug C c) → LR_CBW C → β_contrex c → RLR_CBW C.
Proof.
  intros C c H HC Hc.
  eapply GS_CBW_on_inert; try eassumption.
  clear HC. intros HC.
  apply inert_rigid_wnf, proj2, normal_weak_wnf in H.
  exact (H (ex_intro _ (_,_) (conj eq_refl (conj (R_CBW_is_Weak _ HC) Hc)))).
Qed.

Lemma RR_CBW_on_inert : ∀ C c,
  inert (plug C c) → RR_CBW C → β_contrex c → RRR_CBW C.
Proof.
  intros C c H HC Hc.
  eapply GS_CBW_on_inert; try eassumption.
  clear HC. intros HC.
  apply inert_rigid_wnf, proj2, normal_weak_wnf in H.
  exact (H (ex_intro _ (_,_) (conj eq_refl (conj (R_CBW_is_Weak _ HC) Hc)))).
Qed.

Lemma lr_cbw_phased_lemma : phased_lemma lr_cbw lr_cbw_phased nf.
Proof.
  phased_lemma_by_induction no;
  [ std_phased_lemma_var_case lr_cbw_phased
  | std_phased_lemma_lam_strong_case lr_cbw_phased IHnt IHt |].
  split; [|split].
  + intros [C c] H. split.
    - intros [HC Hc]. left.
      destruct (is_wnf (plug C c)) as [H0|H0].
      * right. split; [apply normal_r_cbw_wnf in H0; exact H0|].
        app_frames_tac H.
        { + apply proj1, inert_rigid_wnf, proj1 in H0.
            contradiction (rigid_abstraction_disjoint _ H0 (proj1 Hc)). }
        { destruct HC as [HC|HC].
          + apply proj1, inert_rigid_wnf, proj2, normal_r_cbw_wnf in H0.
            contradiction (H0
              (ex_intro _ (_, _) (conj eq_refl (conj (proj2 HC) Hc)))).
          + left. exact (conj (RGS_CBW_is_GS_CBW _ _ _ _ @@ proj1 HC) Hc). }
        { destruct HC as [HC|HC].
          + apply proj2, normal_r_cbw_wnf in H0.
            contradiction (H0
              (ex_intro _ (_, _) (conj eq_refl (conj (proj2 HC) Hc)))).
          + right. split; [apply IHt1, neu_is_nf, HC|].
            exact (conj (proj2 HC) Hc). }
      * exact (or_introl (conj
          (LR_CBW_on_not_wnf _ _ H0 HC (βwnf_contrex_is_β_contrex _ Hc)) Hc)).
    - intros [H0|H0]; [|app_frames_tac H; contradiction H0].
      destruct H0 as [H0|[H0 H1]]; [apply r_cbw_is_lr_cbw, H0|].
      apply normal_r_cbw_wnf in H0.
      destruct H1 as [H1|[H1 H2]].
      * app_frames_tac H; try contradiction H1.
        refine (conj _ (proj2 H1)).
        right. refine (conj _ (proj2 H0)).
        destruct H1 as [HC Hc].
        apply (LR_CBW_on_inert _ _ (proj1 H0) HC),
          βwnf_contrex_is_β_contrex, Hc.
      * app_frames_tac H; try contradiction H2.
        refine (conj _ (proj2 H2)).
        right. refine (conj _ (proj1 H2)).
        apply neu_rigid_nf. split.
        { apply inert_is_rigid, H0. }
        { apply IHt1, H1. }
  + intros H. assert(H0 := H).
    destruct H0 as [neu_t1 nf_t2].
    apply neu_rigid_nf in neu_t1. destruct neu_t1 as [rigid_t1 nf_t1].
    apply normal_union_strategy.
    split; [|apply normal_down_strategy_app].
    normal_form_sequence_strategy_tac.
    - apply normal_r_cbw_wnf, nf_is_wnf, H.
    - apply IHt1, nf_t1.
    - apply IHt2, nf_t2.
  + intros H.
    apply normal_union_strategy, proj1,
          normal_form_sequence_strategy in H.
    assert (rigid_t1 := proj1 H).
    apply normal_r_cbw_wnf, proj1, inert_rigid_wnf, proj1 in rigid_t1.
    apply proj2, normal_form_sequence_strategy in H.
    assert (nf_t1 := proj1 H).
    apply IHt1 in nf_t1.
    apply proj2, IHt2 in H.
    refine (conj _ H).
    apply neu_rigid_nf. exact (conj rigid_t1 nf_t1).
Qed.

Corollary lr_cbw_phased_form : lr_cbw == lr_cbw_phased.
Proof. exact (phased_lemma_first_corollary _ _ _ lr_cbw_phased_lemma). Qed.

Corollary normal_lr_cbw_nf : normal_form lr_cbw == nf.
Proof. exact (phased_lemma_second_corollary _ _ _ lr_cbw_phased_lemma). Qed.

Lemma ll_cbw_phased_lemma : phased_lemma ll_cbw ll_cbw_phased nf.
Proof.
  phased_lemma_by_induction no;
  [ std_phased_lemma_var_case ll_cbw_phased
  | std_phased_lemma_lam_strong_case ll_cbw_phased IHnt IHt |].
  split; [|split].
  + intros [C c] H. split.
    - intros [HC Hc]. left.
      destruct (is_wnf (plug C c)) as [H0|H0].
      * right. split; [apply normal_l_cbw_wnf in H0; exact H0|].
        app_frames_tac H.
        { + apply proj1, inert_rigid_wnf, proj1 in H0.
            contradiction (rigid_abstraction_disjoint _ H0 (proj1 Hc)). }
        { destruct HC as [HC|HC].
          + apply proj1, inert_rigid_wnf, proj2, normal_l_cbw_wnf in H0.
            contradiction (H0
              (ex_intro _ (_, _) (conj eq_refl (conj (proj2 HC) Hc)))).
          + left. exact (conj (RGS_CBW_is_GS_CBW _ _ _ _ @@ proj1 HC) Hc). }
        { destruct HC as [HC|HC].
          + apply proj2, normal_l_cbw_wnf in H0.
            contradiction (H0
              (ex_intro _ (_, _) (conj eq_refl (conj (proj2 HC) Hc)))).
          + right. split; [apply IHt1, neu_is_nf, HC|].
            exact (conj (proj2 HC) Hc). }
      * left.
        exact (conj
          (LL_CBW_on_not_wnf _ _ H0 HC (βwnf_contrex_is_β_contrex _ Hc)) Hc).
    - intros [H0|H0]; [|app_frames_tac H; contradiction H0].
      destruct H0 as [H0|[H0 H1]]; [apply l_cbw_is_ll_cbw, H0|].
      apply normal_l_cbw_wnf in H0.
      destruct H1 as [H1|[H1 H2]].
      * app_frames_tac H; try contradiction H1.
        refine (conj _ (proj2 H1)).
        right. refine (conj _ (proj2 H0)).
        destruct H1 as [HC Hc].
        apply (LL_CBW_on_inert _ _ (proj1 H0) HC),
          βwnf_contrex_is_β_contrex, Hc.
      * app_frames_tac H; try contradiction H2.
        refine (conj _ (proj2 H2)).
        right. refine (conj _ (proj1 H2)).
        apply neu_rigid_nf. split.
        { apply inert_is_rigid, H0. }
        { apply IHt1, H1. }
  + intros H. assert(H0 := H).
    destruct H0 as [neu_t1 nf_t2].
    apply neu_rigid_nf in neu_t1. destruct neu_t1 as [rigid_t1 nf_t1].
    apply normal_union_strategy.
    split; [|apply normal_down_strategy_app].
    normal_form_sequence_strategy_tac.
    - apply normal_l_cbw_wnf, nf_is_wnf, H.
    - apply IHt1, nf_t1.
    - apply IHt2, nf_t2.
  + intros H.
    apply normal_union_strategy, proj1,
          normal_form_sequence_strategy in H.
    assert (rigid_t1 := proj1 H).
    apply normal_l_cbw_wnf, proj1, inert_rigid_wnf, proj1 in rigid_t1.
    apply proj2, normal_form_sequence_strategy in H.
    assert (nf_t1 := proj1 H).
    apply IHt1 in nf_t1.
    apply proj2, IHt2 in H.
    refine (conj _ H).
    apply neu_rigid_nf. exact (conj rigid_t1 nf_t1).
Qed.

Corollary ll_cbw_phased_form : ll_cbw == ll_cbw_phased.
Proof. exact (phased_lemma_first_corollary _ _ _ ll_cbw_phased_lemma). Qed.

Corollary normal_ll_cbw_nf : normal_form ll_cbw == nf.
Proof. exact (phased_lemma_second_corollary _ _ _ ll_cbw_phased_lemma). Qed.

Lemma rl_cbw_phased_lemma : phased_lemma rl_cbw rl_cbw_phased nf.
Proof.
  phased_lemma_by_induction no;
  [ std_phased_lemma_var_case rl_cbw_phased
  | std_phased_lemma_lam_strong_case rl_cbw_phased IHnt IHt |].
  split; [|split].
  + intros [C c] H. split.
    - intros [HC Hc]. left.
      destruct (is_wnf (plug C c)) as [H0|H0].
      * right. split; [apply normal_l_cbw_wnf in H0; exact H0|].
        app_frames_tac H.
        { + apply proj1, inert_rigid_wnf, proj1 in H0.
            contradiction (rigid_abstraction_disjoint _ H0 (proj1 Hc)). }
        { destruct HC as [HC|HC].
          + apply proj1, inert_rigid_wnf, proj2, normal_l_cbw_wnf in H0.
            contradiction (H0
              (ex_intro _ (_, _) (conj eq_refl (conj (proj2 HC) Hc)))).
          + right. split; [apply IHt2, HC|].
            exact (conj (RGS_CBW_is_GS_CBW _ _ _ _ @@ proj1 HC) Hc). }
        { destruct HC as [HC|HC].
          + apply proj2, normal_l_cbw_wnf in H0.
            contradiction (H0
              (ex_intro _ (_, _) (conj eq_refl (conj (proj2 HC) Hc)))).
          + left. exact (conj (proj2 HC) Hc). }
      * left.
        exact (conj
          (RL_CBW_on_not_wnf _ _ H0 HC (βwnf_contrex_is_β_contrex _ Hc)) Hc).
    - intros [H0|H0]; [|app_frames_tac H; contradiction H0].
      destruct H0 as [H0|[H0 H1]]; [apply l_cbw_is_rl_cbw, H0|].
      apply normal_l_cbw_wnf in H0.
      destruct H1 as [H1|[H1 H2]].
      * app_frames_tac H; try contradiction H1.
        refine (conj _ (proj2 H1)).
        right. exact (conj (proj1 H0) (proj1 H1)).
      * app_frames_tac H; try contradiction H2.
        refine (conj _ (proj2 H2)).
        right.
        apply IHt2 in H1. refine (conj _ H1).
        destruct H2 as [HC Hc].
        apply (RL_CBW_on_inert _ _ (proj1 H0) HC),
          βwnf_contrex_is_β_contrex, Hc.
  + intros H. assert(H0 := H).
    destruct H0 as [neu_t1 nf_t2].
    apply neu_rigid_nf in neu_t1. destruct neu_t1 as [rigid_t1 nf_t1].
    apply normal_union_strategy.
    split; [|apply normal_down_strategy_app].
    normal_form_sequence_strategy_tac.
    - apply normal_l_cbw_wnf, nf_is_wnf, H.
    - apply IHt2, nf_t2.
    - apply IHt1, nf_t1.
  + intros H.
    apply normal_union_strategy, proj1,
          normal_form_sequence_strategy in H.
    assert (rigid_t1 := proj1 H).
    apply normal_l_cbw_wnf, proj1, inert_rigid_wnf, proj1 in rigid_t1.
    apply proj2, normal_form_sequence_strategy in H.
    assert (nf_t2 := proj1 H).
    apply IHt2 in nf_t2.
    apply proj2, IHt1 in H.
    refine (conj _ nf_t2).
    apply neu_rigid_nf. exact (conj rigid_t1 H).
Qed.

Corollary rl_cbw_phased_form : rl_cbw == rl_cbw_phased.
Proof. exact (phased_lemma_first_corollary _ _ _ rl_cbw_phased_lemma). Qed.

Corollary normal_rl_cbw_nf : normal_form rl_cbw == nf.
Proof. exact (phased_lemma_second_corollary _ _ _ rl_cbw_phased_lemma). Qed.

Lemma rr_cbw_phased_lemma : phased_lemma rr_cbw rr_cbw_phased nf.
Proof.
  phased_lemma_by_induction no;
  [ std_phased_lemma_var_case rr_cbw_phased
  | std_phased_lemma_lam_strong_case rr_cbw_phased IHnt IHt |].
  split; [|split].
  + intros [C c] H. split.
    - intros [HC Hc]. left.
      destruct (is_wnf (plug C c)) as [H0|H0].
      * right. split; [apply normal_r_cbw_wnf in H0; exact H0|].
        app_frames_tac H.
        { + apply proj1, inert_rigid_wnf, proj1 in H0.
            contradiction (rigid_abstraction_disjoint _ H0 (proj1 Hc)). }
        { destruct HC as [HC|HC].
          + apply proj1, inert_rigid_wnf, proj2, normal_r_cbw_wnf in H0.
            contradiction (H0
              (ex_intro _ (_, _) (conj eq_refl (conj (proj2 HC) Hc)))).
          + right. split; [apply IHt2, HC|].
            exact (conj (RGS_CBW_is_GS_CBW _ _ _ _ @@ proj1 HC) Hc). }
        { destruct HC as [HC|HC].
          + apply proj2, normal_r_cbw_wnf in H0.
            contradiction (H0
              (ex_intro _ (_, _) (conj eq_refl (conj (proj2 HC) Hc)))).
          + left. exact (conj (proj2 HC) Hc). }
      * left.
        exact (conj
          (RR_CBW_on_not_wnf _ _ H0 HC (βwnf_contrex_is_β_contrex _ Hc)) Hc).
    - intros [H0|H0]; [|app_frames_tac H; contradiction H0].
      destruct H0 as [H0|[H0 H1]]; [apply r_cbw_is_rr_cbw, H0|].
      apply normal_r_cbw_wnf in H0.
      destruct H1 as [H1|[H1 H2]].
      * app_frames_tac H; try contradiction H1.
        refine (conj _ (proj2 H1)).
        right. exact (conj (proj1 H0) (proj1 H1)).
      * app_frames_tac H; try contradiction H2.
        refine (conj _ (proj2 H2)).
        right.
        apply IHt2 in H1. refine (conj _ H1).
        destruct H2 as [HC Hc].
        apply (RR_CBW_on_inert _ _ (proj1 H0) HC),
          βwnf_contrex_is_β_contrex, Hc.
  + intros H. assert(H0 := H).
    destruct H0 as [neu_t1 nf_t2].
    apply neu_rigid_nf in neu_t1. destruct neu_t1 as [rigid_t1 nf_t1].
    apply normal_union_strategy.
    split; [|apply normal_down_strategy_app].
    normal_form_sequence_strategy_tac.
    - apply normal_r_cbw_wnf, nf_is_wnf, H.
    - apply IHt2, nf_t2.
    - apply IHt1, nf_t1.
  + intros H.
    apply normal_union_strategy, proj1,
          normal_form_sequence_strategy in H.
    assert (rigid_t1 := proj1 H).
    apply normal_r_cbw_wnf, proj1, inert_rigid_wnf, proj1 in rigid_t1.
    apply proj2, normal_form_sequence_strategy in H.
    assert (nf_t2 := proj1 H).
    apply IHt2 in nf_t2.
    apply proj2, IHt1 in H.
    refine (conj _ nf_t2).
    apply neu_rigid_nf. exact (conj rigid_t1 H).
Qed.

Corollary rr_cbw_phased_form : rr_cbw == rr_cbw_phased.
Proof. exact (phased_lemma_first_corollary _ _ _ rr_cbw_phased_lemma). Qed.

Corollary normal_rr_cbw_nf : normal_form rr_cbw == nf.
Proof. exact (phased_lemma_second_corollary _ _ _ rr_cbw_phased_lemma). Qed.

Lemma scbw_conservative_extension_cbw : scbw == cbw;; scbw.
Proof.
  apply two_inclusions. split; [|apply phased_substrategy, cbw_is_scbw].
  intros [C c] [HC Hc].
  destruct (is_wnf (plug C c)) as [H|H].
  + right. split.
    { apply normal_cbw_wnf, H. }
    exact (conj HC Hc).
  + left. refine (conj _ Hc).
    eapply GS_CBW_on_not_wnf.
    5: exact HC.
    4: exact H.
    2, 3: reflexivity.
    2: apply βwnf_contrex_is_β_contrex, Hc.
    clear C c HC Hc H.
    intros [|[]]; cbn; union_tauto.
Qed.

Lemma scbw_phased_form : scbw == scbw_phased.
Proof.
  strategies_equal_on_all_terms_tac; split; try (cbv; tauto); cbn.
  + intros [[[_ ?]|[? ?]] ?]; left.
    - left. repeat split; assumption.
    - destruct (is_wnf (plug (Rapp t2 :: x) c)) as [H2|H2].
      * right. split.
        { apply normal_cbw_wnf; split; [apply H2|assumption]. }
        left. split; [apply RGS_CBW_is_GS_CBW|]; assumption.
      * left. split; [|assumption].
        assert(H3 := H).
        apply RGS_CBW_is_GS_CBW, (GS_CBW_on_not_wnf _ _ _ _ c) in H3.
        ++ repeat split. apply H3.
        ++ intros [|[]]; cbv; tauto.
        ++ reflexivity.
        ++ reflexivity.
        ++ intros H4. apply H2. clear H2.
          split; [|assumption].
          apply wnf_grammar in H4.
          destruct H4; [|assumption].
          exfalso.
          apply RGS_CBW_is_Rigid in H; [|apply inert_is_rigid].
          apply (app_of_monotone _ _ (subset_full _) _ _ (subset_full _)) in H1.
          apply app_of_full, app_abs_disjoint in H1.
          exact (Rigid_on_not_abstraction _ H _ H1 H2).
        ++ apply βwnf_contrex_is_β_contrex, H1.
  + intros [[[[_ ?] ?]|[? [[? ?]|[]]]]|[]].
    - split; [|assumption].
      left. split; [constructor|assumption].
    - split; [|assumption].
      apply normal_cbw_wnf in H.
      apply GS_CBW_grammar in H0.
      destruct H0 as [[H0|H0]|H0].
      * left. split; [constructor|assumption].
      * right.
        destruct H as [H H2].
        split; [exact H0|exact H2].
      * apply Lam_of_inv in H0.
        destruct H0 as [x1 [C [? H0]]]; subst.
        destruct H as [[] _].
  + intros [[[_ H]|[H H0]] H1]; left.
    - left. repeat split; assumption.
    - destruct (is_wnf (plug (Lapp t1 :: x) c)) as [H2|H2].
      * right. split.
        { apply normal_cbw_wnf, H2. }
        right. split; assumption.
      * left. split; [|assumption].
        eapply GS_CBW_on_not_wnf in H0.
        ++ repeat split. apply H0.
        ++ intros [|[]]; cbv; tauto.
        ++ reflexivity.
        ++ reflexivity.
        ++ intros H3. apply H2. exact (conj H H3).
        ++ apply βwnf_contrex_is_β_contrex, H1.
  + intros [[[[_ ?] ?]|[? [[]|[? ?]]]]|[]].
    - split; [|assumption].
      left. split; [constructor|assumption].
    - split; [|assumption].
      right. split; [|assumption].
      apply normal_cbw_wnf in H. apply H.
Qed.

Lemma nf_is_normal_scbw : nf ⊆ normal_form scbw.
Proof.
  intros t. induction t; simpl.
  + intros _. apply normal_var. intros [_ H]. exact H.
  + intros H. specialize (IHt H); clear H. apply normal_lam. split.
    - exact (@proj2 _ _).
    - intros C c H [[[[] _]|HC] Hc]. apply IHt. exists (C, c).
      exact (conj H (conj HC Hc)).
  + intros [neu_t1 nf_t2].
    apply neu_rigid_nf in neu_t1.
    destruct neu_t1 as [rigid_t1 nf_t1].
    apply normal_app. repeat split;
    intros [_ [abs_t1 _]] + (intros C c H [HC Hc]).
    - exact (rigid_abstraction_disjoint _ rigid_t1 abs_t1).
    - apply (IHt1 nf_t1). exists (C, c).
      refine (conj H (conj _ Hc)).
      destruct HC as [[_ HC]|[HC _]].
      * apply W_CBW_is_GS_CBW, HC.
      * apply RGS_CBW_is_GS_CBW, HC.
    - apply (IHt2 nf_t2). exists (C, c).
      refine (conj H (conj _ Hc)).
      destruct HC as [[_ HC]|[_ HC]].
      * apply W_CBW_is_GS_CBW, HC.
      * apply HC.
Qed.

Theorem normal_scbw_nf : normal_form scbw == nf.
Proof.
  apply two_inclusions. split; [|apply nf_is_normal_scbw].
  refine (transitivity
    (normal_form_variance _ _ lr_cbw_is_scbw)
    (proj1 (proj1 (two_inclusions _ _) normal_lr_cbw_nf))).
Qed.

Lemma no_phased_lemma : phased_lemma no no_phased nf.
Proof.
  phased_lemma_by_induction no;
  [ std_phased_lemma_var_case no_phased
  | std_phased_lemma_lam_strong_case no_phased IHnt IHt |].
  split; [|split].
  + intros [C c] H. app_frames_tac H; split; try (strategy_tauto no_phased).
    - intros [H H0].
      apply RLO_Rigid_LO in H. destruct H as [H H1].
      left. right. split.
      { apply normal_only_β_contraction_app,
              (Rigid_on_not_abstraction _ H).
        invert_by app_of_inv H0; exact id. }
      left. split; assumption.
    - intros [[[[] _]|[H [[H0 H1]|[_ []]]]]|[]].
      split; [|exact H1].
      destruct x as [|[| |]]; try exact H0.
      apply H; clear H.
      eexists ([], _). split; [reflexivity|]. repeat split.
    - intros [[H H1] H2].
      apply neu_rigid_nf in H. destruct H as [H H0].
      left. right. split.
      { apply normal_only_β_contraction_app.
        exact (λ H', rigid_abstraction_disjoint _ H H'). }
      right. split; [apply IHt1, H0|].
      split; assumption.
    - intros [[[[] _]|[H [[]|[H0 [H1 H2]]]]]|[]].
      repeat split; try assumption.
      apply IHt1, nf_grammar in H0.
      destruct H0 as [H0|H0].
      { exfalso.
        apply normal_only_β_contraction_app in H. apply H; clear H.
        invert_by abs_of_inv H0. constructor. }
      exact H0.
  + intros [neu_t1 nf_t2].
    apply neu_rigid_nf in neu_t1.
    destruct neu_t1 as [rigid_t1 nf_t1].
    apply normal_union_strategy.
    split; [|apply normal_down_strategy_app].
    normal_form_sequence_strategy_tac.
    - apply normal_only_β_contraction_app.
      exact (λ abs_t1, rigid_abstraction_disjoint _ rigid_t1 abs_t1).
    - apply IHt1, nf_t1.
    - apply IHt2, nf_t2.
  + intros H.
    apply normal_union_strategy, proj1, normal_form_sequence_strategy in H.
    destruct H as [rigid_t1 H].
    apply normal_only_β_contraction_app in rigid_t1.
    apply normal_form_sequence_strategy in H. destruct H as [nf_t1 nf_t2].
    apply IHt1, nf_grammar in nf_t1.
    apply IHt2 in nf_t2.
    destruct nf_t1 as [abs_t1|neu_t1].
    { exfalso. eapply rigid_t1, (abs_of_monotone _ _ (subset_full _)), abs_t1. }
    exact (conj neu_t1 nf_t2).
Qed.

Corollary no_phased_form : no == no_phased.
Proof. exact (phased_lemma_first_corollary _ _ _ no_phased_lemma). Qed.

Corollary normal_no_nf : normal_form no == nf.
Proof. exact (phased_lemma_second_corollary _ _ _ no_phased_lemma). Qed.

Lemma nf_is_normal_full_β : nf ⊆ normal_form full_β.
Proof.
  intros t. induction t; simpl.
  + intros _. apply normal_var. intros [_ H]. exact H.
  + intros H. apply normal_lam. split.
    - exact (@proj2 _ _).
    - intros C c H0 H1. apply (IHt H).
      exists (C, c). split; [exact H0|apply H1].
  + intros [H H0].
    apply neu_rigid_nf in H.
    apply normal_app. repeat split; try (intros C c H1); intros [_ H2].
    - exact (rigid_abstraction_disjoint _ (proj1 H) (proj1 H2)).
    - apply (IHt1 (proj2 H)). exists (C, c). repeat split; assumption.
    - apply (IHt2 H0).        exists (C, c). repeat split; assumption.
Qed.

Theorem normal_full_β_nf : normal_form full_β == nf.
Proof.
  apply two_inclusions. split; [|apply nf_is_normal_full_β].
  exact (transitivity (normal_form_variance _ _ (cartesian_product_monotone
      _ _ (subset_full LO) _ _ (PreOrder_Reflexive _)))
    (proj1 (proj1 (two_inclusions _ _) normal_no_nf))).
Qed.

Corollary neutral_full_β_neu : neutral_form full_β == neu.
Proof. neutral_form_tac normal_full_β_nf nf. Qed.

Lemma full_β_phased_form : full_β == full_β_phased.
Proof. strategies_equal_on_all_terms_tac; split; cbv; tauto. Qed.

Lemma lis_phased_lemma : phased_lemma lis lis_phased nf.
Proof.
  phased_lemma_by_induction lis;
  [ std_phased_lemma_var_case lis_phased
  | std_phased_lemma_lam_strong_case lis_phased IHnt IHt |].
  split; [|split].
  + intros [C c] H. app_frames_tac H; split.
    - intros [_ [Hc _]].
      left. right. split.
      { apply normal_left_strategy_app, normal_ihs_hnf, abs_of_hnf_is_hnf, Hc. }
      left. repeat split.
      eapply abs_of_full, (abs_of_monotone _ _ (subset_full _)), Hc.
    - intros [[[]|[hnf_t1 [[_ [abs_t1 _]]|[_ [[]|[_ []]]]]]]|[]].
      apply normal_left_strategy_app, normal_ihs_hnf, hnf_grammar in hnf_t1.
      destruct hnf_t1 as [rigid_t1|hnf_t1];
      [contradiction (rigid_abstraction_disjoint _ rigid_t1 abs_t1)|].
      repeat split. exact hnf_t1.
    - intros [[HC|HC] Hc].
      * left. left. exact (conj HC Hc).
      * left. right. split.
        { apply normal_left_strategy_app, normal_ihs_hnf, hnf_grammar.
          left. apply plug_RLS_rigid, HC. }
        right. split.
        { apply normal_only_β_contraction_app. intros abs_t.
          refine (rigid_abstraction_disjoint _ _ abs_t).
          apply plug_RLS_rigid, HC. }
        left. refine (conj _ Hc). apply RLS_is_LS, HC.
    - intros [[[HC Hc]|[hnf_t1 [[[] _]|[rigid_t1 [[HC Hc]|[_ []]]]]]]|[]];
      refine (conj _ Hc).
      * left. exact HC.
      * simpl in  hnf_t1. apply normal_left_strategy_app, normal_ihs_hnf in hnf_t1.
        simpl in rigid_t1. apply normal_only_β_contraction_app in rigid_t1.
        apply LS_grammar in HC.
        destruct HC as [[HC|HC]|HC]; [|left; exact HC|right; exact HC].
        destruct x as [|[]]; try (contradiction HC).
        contradiction (rigid_t1 I).
    - intros [[neu_t1 HC] Hc].
      left. right. split.
      { apply normal_left_strategy_app, normal_ihs_hnf, nf_is_hnf, neu_is_nf, neu_t1. }
      right. split.
      { apply normal_only_β_contraction_app. intros abs_t1.
        apply neu_is_inert, inert_is_rigid in neu_t1.
        exact (rigid_abstraction_disjoint _ neu_t1 abs_t1). }
      right. split.
      { apply IHt1, neu_is_nf, neu_t1. }
      exact (conj HC Hc).
    - intros [[[]|[ _  [[[] _]|[rigid_t1 [[]|[nf_t1 [HC Hc]]]]]]]|[]].
      apply IHt1 in nf_t1.
      apply normal_only_β_contraction_app in rigid_t1.
      refine (conj (conj _ HC) Hc).
      apply nf_grammar in nf_t1. destruct nf_t1 as [abs_t1|neu_t1]; [|exact neu_t1].
      exfalso. eapply rigid_t1, abs_of_full, (abs_of_monotone _ _ (subset_full _)), abs_t1.
  + intros [neu_t1 nf_t2].
    apply neu_rigid_nf in neu_t1.
    destruct neu_t1 as [rigid_t1 nf_t1].
    assert (hnf_t1 := nf_t1). apply nf_is_hnf in hnf_t1.
    apply normal_union_strategy.
    split; [|apply normal_down_strategy_app].
    normal_form_sequence_strategy_tac.
    - apply normal_left_strategy_app, normal_ihs_hnf, hnf_t1.
    - apply normal_only_β_contraction_app. intros abs_t1.
      exact (rigid_abstraction_disjoint _ rigid_t1 abs_t1).
    - apply IHt1, nf_t1.
    - apply IHt2, nf_t2.
  + intros H.
    apply normal_union_strategy, proj1,
          normal_form_sequence_strategy, proj2,
          normal_form_sequence_strategy in H. destruct H as [nabs_t1 H].
    apply normal_only_β_contraction_app in nabs_t1.
    apply normal_form_sequence_strategy in H. destruct H as [nf_t1 nf_t2].
    apply IHt1 in nf_t1.
    apply IHt2 in nf_t2.
    split; [|exact nf_t2].
    apply nf_grammar in nf_t1.
    destruct nf_t1 as [abs_t1|neu_t1]; [|exact neu_t1].
    apply (abs_of_monotone _ _ (subset_full _)), abs_of_full in abs_t1.
    contradiction (nabs_t1 abs_t1).
Qed.

Corollary lis_phased_form : lis == lis_phased.
Proof. exact (phased_lemma_first_corollary _ _ _ lis_phased_lemma). Qed.

Corollary normal_lis_nf : normal_form lis == nf.
Proof. exact (phased_lemma_second_corollary _ _ _ lis_phased_lemma). Qed.

Lemma cbwh_phased_lemma : phased_lemma cbwh cbwh_phased whnf.
Proof.
  phased_lemma_by_induction cbwh.
  + split; [intros [C c] H; var_frames_tac H; cbv; union_tauto|].
    split; [|right; constructor].
    intros [[]|_].
    normal_form_sequence_strategy_tac.
    - assumption.
    - apply normal_right_abs_strategy_var.
    - apply normal_only_β_contraction_var.
  + split.
    - intros [C c] H.
      lam_frames_tac H; cbv; unfold plug in *; tauto.
    - split; [|left; constructor].
      intros _.
      normal_form_sequence_strategy_tac.
      * assumption.
      * apply normal_right_abs_strategy_lam.
      * apply normal_only_β_contraction_lam.
  + split; [|split].
    - intros [C c] H.
      app_frames_tac H; try (strategy_tauto cbwh_phased); split.
      * intros [_ [abs_t1 whnf_t2]].
        right. split.
        { apply IHt1, abs_is_whnf, abs_t1. }
        right. split.
        { eapply normal_right_abs_strategy_app, or_intror,
                 normal_right_strategy_app, IHt2, whnf_t2. }
        repeat split; assumption.
      * intros [[]|[_ [[]|[whnf_t2 [_ [abs_t1 _]]]]]].
        apply normal_right_abs_strategy_app in whnf_t2.
        destruct whnf_t2 as [nabs_t1|whnf_t2]; [contradiction (nabs_t1 abs_t1)|].
        eapply normal_right_strategy_app, IHt2 in whnf_t2.
        repeat split; assumption.
      * intros [[abs_t1 HC] Hc].
        right. split.
        { apply IHt1, or_introl, abs_t1. }
        left. repeat split; assumption.
      * intros [[]|[whnf_t1 [[abs_t1 [HC Hc]]|[whnf_t2 [[] Hc]]]]].
        repeat split; assumption.
    - intros [[]|rigid_t1].
      normal_form_sequence_strategy_tac.
      * apply IHt1, or_intror, rigid_t1.
      * apply normal_right_abs_strategy_app. left. intros abs_t1.
        exact (rigid_abstraction_disjoint t1 rigid_t1 abs_t1).
      * apply normal_only_β_contraction_app. intros abs_t1.
        exact (rigid_abstraction_disjoint t1 rigid_t1 abs_t1).
    - intros H. right.
      apply normal_form_sequence_strategy in H. destruct H as [whnf_t1 H].
      apply IHt1 in whnf_t1; clear IHt1.
      apply normal_form_sequence_strategy in H. destruct H as [_ H].
      apply normal_only_β_contraction_app in H.
      destruct whnf_t1 as [abs_t1|rigid_t1]; [contradiction (H abs_t1)|exact rigid_t1].
Qed.

Corollary cbwh_phased_form : cbwh == cbwh_phased.
Proof. exact (phased_lemma_first_corollary _ _ _ cbwh_phased_lemma). Qed.

Corollary normal_cbwh_whnf : normal_form cbwh == whnf.
Proof. exact (phased_lemma_second_corollary _ _ _ cbwh_phased_lemma). Qed.

Lemma sdet_phased_lemma : phased_lemma sdet sdet_phased (abstraction ∪ sdet_stuck).
Proof.
  phased_lemma_by_induction sdet.
  + split; [intros [C c] H; var_frames_tac H; cbv; union_tauto|].
    split; [|right; constructor].
    intros [[]|_].
    normal_form_sequence_strategy_tac.
    - apply normal_left_abs_strategy_var.
    - apply normal_only_βλ_contraction_var.
  + split.
    - intros [C c] H.
      lam_frames_tac H; cbv; unfold plug in *; tauto.
    - split; [|left; constructor].
      intros _.
      normal_form_sequence_strategy_tac.
      * apply normal_left_abs_strategy_lam.
      * apply normal_only_βλ_contraction_lam.
  + split; [|split].
    - intros [C c] H.
      app_frames_tac H; try (cbv; union_tauto); split.
      * intros [_ [abs_t1 abs_t2]].
        right. split.
        { eapply normal_left_abs_strategy_app, or_introl,
                 normal_left_strategy_app, IHt1, or_introl, abs_t1. }
        repeat split; assumption.
      * intros [[]|[_ [_ [? ?]]]].
        repeat split; assumption.
    - intros [[]|[stuck_t1|nabs_t2]];
      normal_form_sequence_strategy_tac.
      * eapply normal_left_abs_strategy_app, or_introl,
               normal_left_strategy_app, IHt1, or_intror, stuck_t1.
      * apply normal_only_βλ_contraction_app, or_introl. intros abs_t1.
        apply (sdet_stuck_abstraction_disjoint _ stuck_t1 abs_t1).
      * eapply normal_left_abs_strategy_app, or_intror, nabs_t2.
      * apply normal_only_βλ_contraction_app, or_intror, nabs_t2.
    - intros H. right.
      apply normal_form_sequence_strategy in H. destruct H as [H0 H].
      apply normal_left_abs_strategy_app in H0.
      destruct H0 as [stuck_t1|H0]; [|right; exact H0].
      eapply normal_left_strategy_app, IHt1 in stuck_t1.
      apply normal_only_βλ_contraction_app in H.
      destruct H as [nabs_t1|H]; [|right; exact H].
      destruct stuck_t1 as [abs_t1|stuck_t1]; [contradiction (nabs_t1 abs_t1)|].
      left. exact stuck_t1.
Qed.

Corollary sdet_phased_form : sdet == sdet_phased.
Proof. exact (phased_lemma_first_corollary _ _ _ sdet_phased_lemma). Qed.

Corollary sdet_phased_form2 : sdet == sdet_phased2.
Proof.
  etransitivity; [apply sdet_phased_form|].
  apply left_abs_weak_strategy_βλ_contraction_commutative.
  apply cartesian_product_monotone.
  + etransitivity; [apply SDET_is_CBN|apply CBN_is_Weak].
  + apply βλ_contrex_is_β_contrex.
Qed.

Lemma l_cbv_phased_lemma : phased_lemma l_cbv l_cbv_phased (abstraction ∪ l_cbv_stuck).
Proof.
  phased_lemma_by_induction l_cbv.
  + split; [intros [C c] H; var_frames_tac H; cbv; union_tauto|].
    split; [|right; constructor].
    intros [[]|_].
    normal_form_sequence_strategy_tac.
    - exact Hl.
    - apply normal_right_abs_strategy_var.
    - apply normal_only_βλ_contraction_var.
  + split.
    - intros [C c] H.
      lam_frames_tac H; cbv; unfold plug in *; tauto.
    - split; [|left; constructor].
      intros _.
      normal_form_sequence_strategy_tac.
      * exact Hl.
      * apply normal_right_abs_strategy_lam.
      * apply normal_only_βλ_contraction_lam.
  + split; [|split].
    - intros [C c] H.
      app_frames_tac H; try (cbv; union_tauto); split.
      * intros [_ [abs_t1 abs_t2]].
        right. split.
        { apply IHt1, or_introl, abs_t1. }
        right. split.
        { eapply normal_right_abs_strategy_app, or_intror,
                 normal_right_strategy_app, IHt2, or_introl, abs_t2. }
        repeat split; assumption.
      * intros [[]|[_ [[]|[_ [_ [abs_t1 abs_t2]]]]]].
        repeat split; assumption.
      * intros [[abs_t1 HC] Hc].
        right. split.
        { apply IHt1, or_introl, abs_t1. }
        left. repeat split; assumption.
      * intros [[]|[_ [[? [? ?]]|[_ [[] _]]]]].
        repeat split; assumption.
    - intros [[]|[stuck_t1|[abs_t1 stuck_t2]]];
      normal_form_sequence_strategy_tac.
      * apply IHt1, or_intror, stuck_t1.
      * eapply normal_right_abs_strategy_app, or_introl. intros abs_t1.
        exact (l_cbv_stuck_abstraction_disjoint _ stuck_t1 abs_t1).
      * apply normal_only_βλ_contraction_app, or_introl. intros abs_t1.
        exact (l_cbv_stuck_abstraction_disjoint _ stuck_t1 abs_t1).
      * apply IHt1, or_introl, abs_t1.
      * eapply normal_right_abs_strategy_app, or_intror,
               normal_right_strategy_app, IHt2, or_intror, stuck_t2.
      * apply normal_only_βλ_contraction_app, or_intror. intros abs_t2.
        exact (l_cbv_stuck_abstraction_disjoint _ stuck_t2 abs_t2).
    - intros H. right.
      apply normal_form_sequence_strategy in H. destruct H as [H0 H].
      apply IHt1 in H0.
      destruct H0 as [abs_t1|H0]; [|left; exact H0].
      apply normal_form_sequence_strategy in H. destruct H as [H1 H].
      apply normal_right_abs_strategy_app in H1.
      destruct H1 as [nabs_t1|H1]; [contradiction (nabs_t1 abs_t1)|].
      eapply normal_right_strategy_app, IHt2 in H1.
      destruct H1 as [abs_t2|stuck_t2]; [|right; split; assumption].
      apply normal_only_βλ_contraction_app in H.
      tauto.
Qed.

Corollary l_cbv_phased_form : l_cbv == l_cbv_phased.
Proof. exact (phased_lemma_first_corollary _ _ _ l_cbv_phased_lemma). Qed.

Lemma r_cbv_phased_lemma : phased_lemma r_cbv r_cbv_phased (abstraction ∪ r_cbv_stuck).
Proof.
  phased_lemma_by_induction r_cbv.
  + split; [intros [C c] H; var_frames_tac H; cbv; union_tauto|].
    split; [|right; constructor].
    intros [[]|_].
    normal_form_sequence_strategy_tac.
    - exact Hr.
    - apply normal_left_abs_strategy_var.
    - apply normal_only_βλ_contraction_var.
  + split.
    - intros [C c] H.
      lam_frames_tac H; cbv; unfold plug in *; tauto.
    - split; [|left; constructor].
      intros _.
      normal_form_sequence_strategy_tac.
      * exact Hr.
      * apply normal_left_abs_strategy_lam.
      * apply normal_only_βλ_contraction_lam.
  + split; [|split].
    - intros [C c] H.
      app_frames_tac H; try (cbv; union_tauto); split.
      * intros [_ [abs_t1 abs_t2]].
        right. split.
        { apply IHt2, or_introl, abs_t2. }
        right. split.
        { eapply normal_left_abs_strategy_app, or_introl,
                 normal_left_strategy_app, IHt1, or_introl, abs_t1. }
        repeat split; assumption.
      * intros [[]|[_ [[]|[_ [_ [abs_t1 abs_t2]]]]]].
        repeat split; assumption.
      * intros [[abs_t1 HC] Hc].
        right. split.
        { apply IHt2, or_introl, abs_t1. }
        left. repeat split; assumption.
      * intros [[]|[_ [[? [? ?]]|[_ [[] _]]]]].
        repeat split; assumption.
    - intros [[]|[stuck_t2|[stuck_t1 abs_t2]]];
      normal_form_sequence_strategy_tac.
      * apply IHt2, or_intror, stuck_t2.
      * eapply normal_left_abs_strategy_app, or_intror. intros abs_t2.
        exact (r_cbv_stuck_abstraction_disjoint _ stuck_t2 abs_t2).
      * apply normal_only_βλ_contraction_app, or_intror. intros abs_t2.
        exact (r_cbv_stuck_abstraction_disjoint _ stuck_t2 abs_t2).
      * apply IHt2, or_introl, abs_t2.
      * eapply normal_left_abs_strategy_app, or_introl,
               normal_left_strategy_app, IHt1, or_intror, stuck_t1.
      * apply normal_only_βλ_contraction_app, or_introl. intros abs_t1.
        exact (r_cbv_stuck_abstraction_disjoint _ stuck_t1 abs_t1).
    - intros H. right.
      apply normal_form_sequence_strategy in H. destruct H as [H0 H].
      apply IHt2 in H0.
      destruct H0 as [abs_t2|H0]; [|left; exact H0].
      apply normal_form_sequence_strategy in H. destruct H as [H1 H].
      apply normal_left_abs_strategy_app in H1.
      destruct H1 as [H1|nabs_t2]; [|contradiction (nabs_t2 abs_t2)].
      eapply normal_left_strategy_app, IHt1 in H1.
      destruct H1 as [abs_t1|stuck_t1]; [|right; split; assumption].
      apply normal_only_βλ_contraction_app in H.
      tauto.
Qed.

Corollary r_cbv_phased_form : r_cbv == r_cbv_phased.
Proof. exact (phased_lemma_first_corollary _ _ _ r_cbv_phased_lemma). Qed.

Lemma cbv_phased_form : cbv == cbv_phased.
Proof. strategies_equal_on_all_terms_tac; split; cbv; tauto. Qed.

Lemma li_phased_lemma : phased_lemma li li_phased nf.
Proof.
  phased_lemma_by_induction li;
  [ std_phased_lemma_var_case li_phased
  | std_phased_lemma_lam_strong_case li_phased IHnt IHt |].
  split; [|split].
  + intros [C c] H.
    app_frames_tac H; split; try (strategy_tauto li_phased).
    - intros [_ [abs_nf_t1 nf_t2]].
      apply abs_of_nf_abs_nf in abs_nf_t1.
      destruct abs_nf_t1 as [abs_t1 nf_t1].
      left. right. split; [apply IHt1, nf_t1|].
      right. split; [apply IHt2, nf_t2|].
      repeat split. exact abs_t1.
    - intros [[[]|[nf_t1 [[]|[nf_t2 [_ [abs_t1 _]]]]]]|[]].
      repeat split; [apply abs_of_nf_abs_nf; split|]; tauto.
    - cbn. intros [[? ?] ?].
      left. right.
      split; [apply IHt1, H|].
      left. split; assumption.
  + intros [neu_t1 nf_t2].
    apply neu_rigid_nf in neu_t1.
    destruct neu_t1 as [rigid_t1 nf_t1].
    apply normal_union_strategy.
    split; [|apply normal_down_strategy_app].
    normal_form_sequence_strategy_tac.
    - apply IHt1, nf_t1.
    - apply IHt2, nf_t2.
    - apply normal_only_β_contraction_app. intros abs_t1.
      exact (rigid_abstraction_disjoint _ rigid_t1 abs_t1).
  + intros H.
    apply normal_union_strategy, proj1,
          normal_form_sequence_strategy in H.
    destruct H as [nf_t1 H].
    apply normal_form_sequence_strategy in H.
    destruct H as [nf_t2 rigid_t1].
    apply normal_only_β_contraction_app in rigid_t1.
    apply IHt1 in nf_t1.
    apply IHt2 in nf_t2.
    apply nf_grammar in nf_t1.
    destruct nf_t1 as [abs_t1|neu_t1].
    { exfalso. eapply rigid_t1, (abs_of_monotone _ _ (subset_full _)), abs_t1. }
    exact (conj neu_t1 nf_t2).
Qed.

Corollary li_phased_form : li == li_phased.
Proof. exact (phased_lemma_first_corollary _ _ _ li_phased_lemma). Qed.

Corollary normal_li_nf : normal_form li == nf.
Proof. exact (phased_lemma_second_corollary _ _ _ li_phased_lemma). Qed.

Lemma ri_phased_lemma : phased_lemma ri ri_phased nf.
Proof.
  phased_lemma_by_induction ri;
  [ std_phased_lemma_var_case ri_phased
  | std_phased_lemma_lam_strong_case ri_phased IHnt IHt |].
  split; [|split].
  + intros [C c] H.
    app_frames_tac H; split; try (strategy_tauto ri_phased).
    - intros [_ [abs_nf_t1 nf_t2]].
      apply abs_of_nf_abs_nf in abs_nf_t1.
      destruct abs_nf_t1 as [abs_t1 nf_t1].
      left. right. split; [apply IHt2, nf_t2|].
      right. split; [apply IHt1, nf_t1|].
      repeat split. exact abs_t1.
    - intros [[[]|[nf_t2 [[]|[nf_t1 [_ [abs_t1 _]]]]]]|[]].
      apply IHt2 in nf_t2.
      apply IHt1 in nf_t1.
      repeat split; [apply abs_of_nf_abs_nf; split|]; tauto.
    - cbn. intros [[? ?] ?].
      left. right.
      split; [apply IHt2, H|].
      left. split; assumption.
  + intros [neu_t1 nf_t2].
    apply neu_rigid_nf in neu_t1.
    destruct neu_t1 as [rigid_t1 nf_t1].
    apply normal_union_strategy. split; [|apply normal_down_strategy_app].
    normal_form_sequence_strategy_tac.
    - apply IHt2, nf_t2.
    - apply IHt1, nf_t1.
    - apply normal_only_β_contraction_app.
      exact (λ abs_t1, rigid_abstraction_disjoint _ rigid_t1 abs_t1).
  + intros H.
    apply normal_union_strategy, proj1,
          normal_form_sequence_strategy in H.
    destruct H as [nf_t2 H].
    apply normal_form_sequence_strategy in H.
    destruct H as [nf_t1 rigid_t1].
    apply normal_only_β_contraction_app in rigid_t1.
    apply IHt2 in nf_t2.
    apply IHt1, nf_grammar in nf_t1.
    destruct nf_t1 as [abs_t1|neu_t1]; [|exact (conj neu_t1 nf_t2)].
    exfalso.
    eapply rigid_t1, (abs_of_monotone _ _ (subset_full _)), abs_t1.
Qed.

Corollary ri_phased_form : ri == ri_phased.
Proof. exact (phased_lemma_first_corollary _ _ _ ri_phased_lemma). Qed.

Corollary normal_ri_nf : normal_form ri == nf.
Proof. exact (phased_lemma_second_corollary _ _ _ ri_phased_lemma). Qed.

Theorem normal_inn_nf : normal_form inn == nf.
Proof.
  apply two_inclusions; split; [|unfold flip]; etransitivity.
  + apply normal_form_variance, cartesian_product_monotone; [apply subset_full|reflexivity].
  + exact (proj1 (proj1 (two_inclusions _ _) normal_li_nf)).
  + exact (proj2 (proj1 (two_inclusions _ _) normal_full_β_nf)).
  + apply normal_form_variance, cartesian_product_monotone; [apply subset_full|].
    etransitivity; [apply nfβnf_contrex_is_βmnf_contrex|].
    etransitivity; [apply βmnf_contrex_is_βwnf_contrex|].
    etransitivity; [apply βwnf_contrex_is_βwhnf_contrex|].
    apply βwhnf_contrex_is_β_contrex.
Qed.

Lemma inn_phased_form : inn == inn_phased.
Proof.
  strategies_equal_on_all_terms_tac; split; try (cbv; tauto; fail); simpl.
  + intros [_ H]. left. right. split.
    - apply normal_union_strategy. split.
      * apply normal_left_strategy_app, normal_inn_nf, nf_grammar, or_introl, H.
      * apply normal_right_strategy_app, normal_inn_nf, H.
    - repeat constructor.
      eapply abs_of_full, (abs_of_monotone _ _ (subset_full _)), H.
  + intros [[[[]|[]]|[nf_t12 [_ [abs_t1 _]]]]|[]].
    apply normal_union_strategy in nf_t12.
    destruct nf_t12 as [nf_t1 nf_t2].
    apply normal_left_strategy_app, normal_inn_nf in nf_t1.
    apply normal_right_strategy_app, normal_inn_nf in nf_t2.
    refine (conj I (conj _ nf_t2)).
    apply abs_of_nf_abs_nf. split; assumption.
Qed.

Theorem normal_cbm_nf : normal_form cbm == nf.
Proof.
  apply two_inclusions; split; [|unfold flip]; etransitivity.
  + apply normal_form_variance, cartesian_product_monotone; [|reflexivity].
    exact L_CBM_is_CBM.
  + exact (proj1 (proj1 (two_inclusions _ _) normal_l_cbm_nf)).
  + exact (proj2 (proj1 (two_inclusions _ _) normal_full_β_nf)).
  + apply normal_form_variance, cartesian_product_monotone; [apply subset_full|].
    etransitivity; [apply βmnf_contrex_is_βwnf_contrex|].
    etransitivity; [apply βwnf_contrex_is_βwhnf_contrex|].
    apply βwhnf_contrex_is_β_contrex.
Qed.

Theorem normal_rigid_cbm_mnf : normal_form rigid_cbm == mnf.
Proof.
  apply two_inclusions; split.
  + etransitivity.
    - apply normal_form_variance, cartesian_product_monotone; [|reflexivity].
      apply L_CBM_is_CBM_aux.
    - exact (proj1 (proj1 (two_inclusions _ _) normal_rl_cbm_mnf)).
  + intros t. induction t; simpl.
    - intros _. apply normal_var. intros [_ H]. exact H.
    - intros _. apply normal_lam. split.
      * exact (@proj2 _ _).
      * intros C c H0. exact (@proj1 _ _).
    - intros [neu_t1 nf_t2].
      apply normal_app. repeat split; try (intros C c H1); intros [H2 H3].
      * apply neu_rigid_nf in neu_t1.
        exact (rigid_abstraction_disjoint _ (proj1 neu_t1) (proj1 H3)).
      * apply IHt1; [apply mnf_grammar; right; exact neu_t1|].
        exists (C, c). repeat split; assumption.
      * simpl in H2.
        destruct H2 as [H2|[_ H2]];
        [apply IHt2; [apply nf_is_mnf, nf_t2|]|apply normal_cbm_nf in nf_t2; apply nf_t2];
        exists (C, c); repeat split; assumption.
Qed.

Lemma cbm_phased_form : cbm == cbm_phased.
Proof.  strategies_equal_on_all_terms_tac; split; cbv; tauto. Qed.

Lemma rigid_cbm_phased_form : rigid_cbm == rigid_cbm_phased.
Proof.
  strategies_equal_on_all_terms_tac; split; try (cbv; tauto; fail); simpl.
  + intros [_ [abs_t1 mnf_t2]]. right. split.
    - apply normal_union_strategy. split.
      * apply normal_left_strategy_app, normal_rigid_cbm_mnf, mnf_grammar, or_introl, abs_t1.
      * apply normal_right_strategy_app, normal_rigid_cbm_mnf, mnf_t2.
    - left. repeat constructor. assumption.
  + intros [[[]|[]]|[mnf_t2 [[_ [abs_t1 _]]|[_ []]]]].
    apply normal_union_strategy, proj2, normal_right_strategy_app, normal_rigid_cbm_mnf in mnf_t2.
    repeat constructor; assumption.
  + destruct (is_mnf (plug x c)) as [mnf_t1|nmnf_t1];
    (intros [[HC|[neu_t1 HC]] Hc]; [left; right; exact (conj HC Hc)|]).
    - right. split.
      { apply normal_union_strategy. split; simpl.
        + apply normal_left_strategy_app, normal_rigid_cbm_mnf, mnf_grammar, or_intror, neu_t1.
        + apply normal_right_strategy_app, normal_rigid_cbm_mnf, mnf_t1. }
      right. split.
      { apply neu_is_inert, inert_is_rigid in neu_t1.
        apply normal_only_β_contraction_app.
        exact (rigid_abstraction_disjoint _ neu_t1). }
      exact (conj HC Hc).
    - apply (CBM_on_not_mnf _ _ nmnf_t1) in HC.
      left. right; exact (conj HC Hc).
  + intros [[[]|[HC Hc]]|[mnf_t1 [[[] _]|[nabs_t1 [HC Hc]]]]].
    - exact (conj (or_introl HC) Hc).
    - apply normal_union_strategy, proj1, normal_left_strategy_app,
            normal_rigid_cbm_mnf, mnf_grammar in mnf_t1.
      apply normal_only_β_contraction_app in nabs_t1.
      destruct mnf_t1 as [abs_t1|neu_t1]; [contradiction (nabs_t1 abs_t1)|].
      exact (conj (or_intror (conj neu_t1 HC)) Hc).
Qed.

Theorem weak_head_normal_forms : ∀ s, In s [cbn; cbwh] →
  normal_form s == whnf.
Proof.
  intros s [H|[H|[]]]; subst; [exact normal_cbn_whnf|exact normal_cbwh_whnf].
Qed.

Theorem weak_normal_forms : ∀ s, In s [l_cbw; r_cbw; cbw; low; weak] →
  normal_form s == wnf.
Proof.
  intros s [H|[H|[H|[H|[H|[]]]]]]; subst.
  + exact normal_l_cbw_wnf.
  + exact normal_r_cbw_wnf.
  + exact normal_cbw_wnf.
  + exact normal_low_wnf.
  + exact normal_weak_wnf.
Qed.

Theorem middle_normal_forms : ∀ s, In s [rl_cbm; rr_cbm; rigid_cbm] →
  normal_form s == mnf.
Proof.
  intros s [H|[H|[H|[]]]]; subst.
  + exact normal_rl_cbm_mnf.
  + exact normal_rr_cbm_mnf.
  + exact normal_rigid_cbm_mnf.
Qed.

Theorem head_normal_forms : ∀ s, In s [head; ihs] →
  normal_form s == hnf.
Proof.
  intros s [H|[H|[]]]; subst; [exact normal_head_hnf|exact normal_ihs_hnf].
Qed.

Theorem full_normal_forms : ∀ s, In s [no; lis; ll_cbw; lr_cbw; rl_cbw; rr_cbw;
  scbw; l_cbm; r_cbm; cbm; li; ri; inn; full_β] → normal_form s == nf.
Proof.
  intros s [H|[H|[H|[H|[H|[H|[H|[H|[H|[H|[H|[H|[H|[H|[]]]]]]]]]]]]]]]; subst.
  + exact normal_no_nf.
  + exact normal_lis_nf.
  + exact normal_ll_cbw_nf.
  + exact normal_lr_cbw_nf.
  + exact normal_rl_cbw_nf.
  + exact normal_rr_cbw_nf.
  + exact normal_scbw_nf.
  + exact normal_l_cbm_nf.
  + exact normal_r_cbm_nf.
  + exact normal_cbm_nf.
  + exact normal_li_nf.
  + exact normal_ri_nf.
  + exact normal_inn_nf.
  + exact normal_full_β_nf.
Qed.
