Require Import Utf8.
Require Import Coq.Lists.List.
Require Import Coq.Strings.String.
Require Import Preliminaries Common Term Context Decomposition Phased
               PhasedForm Decider Decompose.
Import ListNotations.

Theorem cbn_det : det_strategy cbn.
Proof.
  intros t. induction t;
  intros [C c] [C' c'] [H H0] [H' H0'].
  + var_frames_tac H. var_frames_tac H'. reflexivity.
  + lam_frames_tac H;
    [contradiction (proj2 H0)|contradiction (proj1 (proj1 H0))].
  + apply cbn_phased_form in H0;
    apply cbn_phased_form in H0'.
    app_frames_tac H; [| |cbv in H0; tauto].
    - destruct H0 as [[]|[H0 [_ [H1 _]]]].
      app_frames_tac H'; [reflexivity|exfalso|cbv in H0'; tauto].
      destruct H0' as [H0'|[_ [[] _]]].
      destruct (abstraction_inv _ H1) as [? [t1' H2]].
      symmetry in H2. lam_frames_tac H2.
      * destruct H0' as [_ []].
      * destruct H0' as [[[] _] _].
    - destruct H0 as [H0|[_ [[] _]]].
      app_frames_tac H'; [exfalso| |cbv in H0'; tauto].
      { destruct H0' as [[]|[H0' _]].
        apply H0'.
        eexists. split; [|exact H0]. reflexivity. }
      destruct H0' as [H0'|[_ [[] _]]].
      assert (H2 := (IHt1 (x, c) (x0, c') (conj eq_refl H0) (conj H1 H0'))).
      inversion H2. subst. reflexivity.
Qed.

Ltac std_det_tac :=
  intros t; induction t as [| |];
  intros [C c] [C' c'] [H H0] [H' H0'];
  [ var_frames_tac H; var_frames_tac H'; reflexivity
  | lam_frames_tac H;  [contradiction (proj2 H0) |];
    lam_frames_tac H'; [contradiction (proj2 H0')|] |].

Lemma only_β_contraction_det : det_strategy β.
Proof.
  intros t [C c] [C' c'] [H [H0 H1]] [H' [H0' H1']].
  apply Hole_inv in H0. apply Hole_inv in H0'. subst.
  simpl in H'. subst. reflexivity.
Qed.

Lemma det_empty_strategy : det_strategy ∅.
Proof. intros _ _ _ [_ []]. Qed.

Lemma union_down_strategy_app : ∀ s s' d t1 t2,
  app t1 t2 = recompose d → (s ∪ ↓ s') d → s d.
Proof.
  intros s s' d t1 t2 H H0.
  destruct H0 as [H0|H0]; [exact H0|].
  destruct d as [[|[| |]] c]; try (contradiction H0).
  inversion H.
Qed.

Example left_strategy_det : ∀ s, det_strategy s ↔ det_strategy (↙ s).
Proof.
  intros s. split.
  + intros det_s t d d' [H H0] [H' H0']. subst.
    destruct d  as [[|[]] c ]; try (contradiction H0).
    destruct d' as [[|[]] c']; try (contradiction H0').
    inversion H'.
    specialize (det_s _ (_, _) (_, _) (conj eq_refl H0) (conj H1 H0')).
    apply pair_equal_spec in det_s. destruct det_s. subst. reflexivity.
  + intros det_ls t [C c] [C' c'] [H H0] [H' H0']. subst.
    enough (H : (Rapp (var "x") :: C, c) = (Rapp (var "x") :: C', c')).
    { inversion H. tauto. }
    apply (det_ls (app (plug C c) (var "x")) (Rapp _ :: C, c) (Rapp _ :: C', c'));
    split; [reflexivity|assumption|simpl; f_equal; exact H'|assumption].
Qed.

Example left_strategy_det_arrow : family_arrow ↙ det_strategy det_strategy.
Proof. intros s. apply left_strategy_det. Qed.

Lemma left_strategy_det_on : ∀ t1 t2 s,
  (∃≤1 d, decomposes_into_in (app t1 t2) d (↙ s)) ↔
   ∃≤1 d, decomposes_into_in      t1     d    s.
Proof.
  intros t1 t2 s. split; intros H [C c] [C' c'] [H0 H1] [H0' H1'].
  + specialize (H (Rapp t2::C, c) (Rapp t2::C', c')
      (conj (f_equal _ H0) H1) (conj (f_equal _ H0') H1')).
    inversion H. subst. reflexivity.
  + destruct C  as [|[| |]]; try contradiction H1.
    destruct C' as [|[| |]]; try contradiction H1'.
    inversion H0. inversion H0'. subst.
    specialize (H (_,_) (_,_) (conj eq_refl H1) (conj H5 H1')).
    inversion H. subst. reflexivity.
Qed.

Lemma right_strategy_det_on : ∀ t1 t2 s,
  (∃≤1 d, decomposes_into_in (app t1 t2) d (↘ s)) ↔
   ∃≤1 d, decomposes_into_in         t2  d    s.
Proof.
  intros t1 t2 s. split; intros H [C c] [C' c'] [H0 H1] [H0' H1'].
  + specialize (H (Lapp t1::C, c) (Lapp t1::C', c')
      (conj (f_equal _ H0) H1) (conj (f_equal _ H0') H1')).
    inversion H. subst. reflexivity.
  + destruct C  as [|[| |]]; try contradiction H1.
    destruct C' as [|[| |]]; try contradiction H1'.
    inversion H0. inversion H0'. subst.
    specialize (H (_,_) (_,_) (conj eq_refl H1) (conj H6 H1')).
    inversion H. subst. reflexivity.
Qed.

Lemma sequence_det_on : ∀ t r s,
  (∃≤1 d, decomposes_into_in t d r) →
  (∃≤1 d, decomposes_into_in t d s) →
  (∃≤1 d, decomposes_into_in t d (r;; s)).
Proof.
  intros t s ss Hs Hss d d' [H [H0|[H1 H0]]] [H' [H0'|[H1' H0']]].
  + exact (Hs  d d' (conj H H0) (conj H' H0')).
  + contradiction (H1' (ex_intro _ _ (conj (eq_trans (eq_sym H') H ) H0 ))).
  + contradiction (H1  (ex_intro _ _ (conj (eq_trans (eq_sym H ) H') H0'))).
  + exact (Hss d d' (conj H H0) (conj H' H0')).
Qed.

Theorem no_det : det_strategy no.
Proof.
  std_det_tac.
  + specialize (IHt (x0, c) (x1, c') (conj eq_refl H0) (conj H1 H0')).
    apply pair_equal_spec in IHt.
    f_equal; [f_equal|]; apply IHt.
  + apply no_phased_form, (union_down_strategy_app _ _ _ _ _ H)  in H0.
    apply no_phased_form, (union_down_strategy_app _ _ _ _ _ H') in H0'.
    apply ( left_strategy_det_on t1 t2) in IHt1.
    apply (right_strategy_det_on t1 t2) in IHt2.
    exact (
      (sequence_det_on _ _ _ (only_β_contraction_det (app t1 t2))
      (sequence_det_on _ _ _ IHt1
      IHt2)) _ _ (conj H H0) (conj H' H0')).
Qed.

Theorem head_det : det_strategy head.
Proof. exact (det_strategy_variance _ _ head_is_no no_det). Qed.

Theorem ihs_det : det_strategy ihs.
Proof.
  std_det_tac.
  + destruct H0  as [[_ H0 ] H2 ].
    destruct H0' as [[_ H0'] H2'].
    specialize (IHt (x0, c) (x1, c') (conj eq_refl (conj H0  H2))
                                     (conj H1      (conj H0' H2'))).
    apply pair_equal_spec in IHt.
    f_equal; [f_equal|]; apply IHt.
  + apply ihs_phased_form, (union_down_strategy_app _ _ _ _ _ H)  in H0.
    apply ihs_phased_form, (union_down_strategy_app _ _ _ _ _ H') in H0'.
    apply ( left_strategy_det_on t1 t2) in IHt1.
    exact (
      (sequence_det_on _ _ _ IHt1
      (only_β_contraction_det (app t1 t2))
      ) _ _ (conj H H0) (conj H' H0')).
Qed.

Theorem lis_det : det_strategy lis.
Proof.
  std_det_tac.
  + specialize (IHt (x0, c) (x1, c') (conj eq_refl H0)
                                     (conj H1      H0')).
    apply pair_equal_spec in IHt.
    f_equal; [f_equal|]; apply IHt.
  + apply lis_phased_form, (union_down_strategy_app _ _ _ _ _ H)  in H0.
    apply lis_phased_form, (union_down_strategy_app _ _ _ _ _ H') in H0'.
    apply ( left_strategy_det_on t1 t2) in IHt1.
    apply (right_strategy_det_on t1 t2) in IHt2.
    exact (
      (sequence_det_on _ _ _ (proj1 (left_strategy_det _) ihs_det _)
      (sequence_det_on _ _ _ (only_β_contraction_det (app t1 t2))
      (sequence_det_on _ _ _ IHt1 IHt2
      ))) _ _ (conj H H0) (conj H' H0')).
Qed.

Theorem li_det : det_strategy li.
Proof.
  std_det_tac.
  + destruct H0  as [[_ H0 ] H2 ].
    destruct H0' as [[_ H0'] H2'].
    specialize (IHt (x0, c) (x1, c') (conj eq_refl (conj H0  H2))
                                     (conj H1      (conj H0' H2'))).
    apply pair_equal_spec in IHt.
    f_equal; [f_equal|]; apply IHt.
  + apply li_phased_form, (union_down_strategy_app _ _ _ _ _ H)  in H0.
    apply li_phased_form, (union_down_strategy_app _ _ _ _ _ H') in H0'.
    apply ( left_strategy_det_on t1 t2) in IHt1.
    apply (right_strategy_det_on t1 t2) in IHt2.
    exact (
      (sequence_det_on _ _ _ IHt1
      (sequence_det_on _ _ _ IHt2
      (only_β_contraction_det (app t1 t2)))) _ _ (conj H H0) (conj H' H0')).
Qed.

Theorem ri_det : det_strategy ri.
Proof.
  std_det_tac.
  + destruct H0  as [[_ H0 ] H2 ].
    destruct H0' as [[_ H0'] H2'].
    specialize (IHt (x0, c) (x1, c') (conj eq_refl (conj H0  H2))
                                     (conj H1      (conj H0' H2'))).
    apply pair_equal_spec in IHt.
    f_equal; [f_equal|]; apply IHt.
  + apply ri_phased_form, (union_down_strategy_app _ _ _ _ _ H)  in H0.
    apply ri_phased_form, (union_down_strategy_app _ _ _ _ _ H') in H0'.
    apply ( left_strategy_det_on t1 t2) in IHt1.
    apply (right_strategy_det_on t1 t2) in IHt2.
    exact (
      (sequence_det_on _ _ _ IHt2
      (sequence_det_on _ _ _ IHt1
      (only_β_contraction_det (app t1 t2))
      )) _ _ (conj H H0) (conj H' H0')).
Qed.

Theorem low_det : det_strategy low.
Proof.
  std_det_tac.
  + destruct H0  as [[_ H0 ] H2 ].
    destruct H0' as [[_ H0'] H2'].
    specialize (IHt (x0, c) (x1, c') (conj eq_refl (conj H0  H2))
                                     (conj H1      (conj H0' H2'))).
    apply pair_equal_spec in IHt.
    f_equal; [f_equal|]; apply IHt.
  + apply low_phased_form in H0.
    apply low_phased_form in H0'.
    apply ( left_strategy_det_on t1 t2) in IHt1.
    apply (right_strategy_det_on t1 t2) in IHt2.
    exact (
      (sequence_det_on _ _ _ (only_β_contraction_det (app t1 t2))
      (sequence_det_on _ _ _ IHt1 IHt2
      )) _ _ (conj H H0) (conj H' H0')).
Qed.

Theorem l_cbw_det : det_strategy l_cbw.
Proof.
  std_det_tac.
  + destruct H0  as [[_ H0 ] H2 ].
    destruct H0' as [[_ H0'] H2'].
    specialize (IHt (x0, c) (x1, c') (conj eq_refl (conj H0  H2))
                                     (conj H1      (conj H0' H2'))).
    apply pair_equal_spec in IHt.
    f_equal; [f_equal|]; apply IHt.
  + apply l_cbw_phased_form in H0.
    apply l_cbw_phased_form in H0'.
    apply ( left_strategy_det_on t1 t2) in IHt1.
    apply (right_strategy_det_on t1 t2) in IHt2.
    exact (
      (sequence_det_on _ _ _ IHt1
      (sequence_det_on _ _ _ IHt2
      (only_β_contraction_det (app t1 t2))
      )) _ _ (conj H H0) (conj H' H0')).
Qed.

Theorem r_cbw_det : det_strategy r_cbw.
Proof.
  std_det_tac.
  + destruct H0  as [[_ H0 ] H2 ].
    destruct H0' as [[_ H0'] H2'].
    specialize (IHt (x0, c) (x1, c') (conj eq_refl (conj H0  H2))
                                     (conj H1      (conj H0' H2'))).
    apply pair_equal_spec in IHt.
    f_equal; [f_equal|]; apply IHt.
  + apply r_cbw_phased_form in H0.
    apply r_cbw_phased_form in H0'.
    apply ( left_strategy_det_on t1 t2) in IHt1.
    apply (right_strategy_det_on t1 t2) in IHt2.
    exact (
      (sequence_det_on _ _ _ IHt2
      (sequence_det_on _ _ _ IHt1
      (only_β_contraction_det (app t1 t2))
      )) _ _ (conj H H0) (conj H' H0')).
Qed.

Theorem l_cbv_det : det_strategy l_cbv.
Proof. exact (det_strategy_variance _ _ l_cbv_is_l_cbw l_cbw_det). Qed.

Theorem r_cbv_det : det_strategy r_cbv.
Proof. exact (det_strategy_variance _ _ r_cbv_is_r_cbw r_cbw_det). Qed.

Theorem sdet_det : det_strategy sdet.
Proof. exact (det_strategy_variance _ _    sdet_is_cbn   cbn_det). Qed.

Theorem ll_cbw_det : det_strategy ll_cbw.
Proof.
  std_det_tac.
  + destruct H0  as [[[[] _]|HC ] Hc ].
    destruct H0' as [[[[] _]|HC'] Hc'].
    specialize (IHt (x0, c) (x1, c') (conj eq_refl (conj HC  Hc))
                                     (conj H1      (conj HC' Hc'))).
    apply pair_equal_spec in IHt.
    f_equal; [f_equal|]; apply IHt.
  + apply ll_cbw_phased_form, (union_down_strategy_app _ _ _ _ _ H ) in H0.
    apply ll_cbw_phased_form, (union_down_strategy_app _ _ _ _ _ H') in H0'.
    apply ( left_strategy_det_on t1 t2) in IHt1.
    apply (right_strategy_det_on t1 t2) in IHt2.
    exact (
      (sequence_det_on _ _ _ (l_cbw_det _)
      (sequence_det_on _ _ _ IHt1 IHt2
      )) _ _ (conj H H0) (conj H' H0')).
Qed.

Theorem rl_cbw_det : det_strategy rl_cbw.
Proof.
  std_det_tac.
  + destruct H0  as [[[[] _]|HC ] Hc ].
    destruct H0' as [[[[] _]|HC'] Hc'].
    specialize (IHt (x0, c) (x1, c') (conj eq_refl (conj HC  Hc))
                                     (conj H1      (conj HC' Hc'))).
    apply pair_equal_spec in IHt.
    f_equal; [f_equal|]; apply IHt.
  + apply rl_cbw_phased_form, (union_down_strategy_app _ _ _ _ _ H ) in H0.
    apply rl_cbw_phased_form, (union_down_strategy_app _ _ _ _ _ H') in H0'.
    apply ( left_strategy_det_on t1 t2) in IHt1.
    apply (right_strategy_det_on t1 t2) in IHt2.
    exact (
      (sequence_det_on _ _ _ (l_cbw_det _)
      (sequence_det_on _ _ _ IHt2 IHt1
      )) _ _ (conj H H0) (conj H' H0')).
Qed.

Theorem lr_cbw_det : det_strategy lr_cbw.
Proof.
  std_det_tac.
  + destruct H0  as [[[[] _]|HC ] Hc ].
    destruct H0' as [[[[] _]|HC'] Hc'].
    specialize (IHt (x0, c) (x1, c') (conj eq_refl (conj HC  Hc))
                                     (conj H1      (conj HC' Hc'))).
    apply pair_equal_spec in IHt.
    f_equal; [f_equal|]; apply IHt.
  + apply lr_cbw_phased_form, (union_down_strategy_app _ _ _ _ _ H ) in H0.
    apply lr_cbw_phased_form, (union_down_strategy_app _ _ _ _ _ H') in H0'.
    apply ( left_strategy_det_on t1 t2) in IHt1.
    apply (right_strategy_det_on t1 t2) in IHt2.
    exact (
      (sequence_det_on _ _ _ (r_cbw_det _)
      (sequence_det_on _ _ _ IHt1 IHt2
      )) _ _ (conj H H0) (conj H' H0')).
Qed.

Theorem rr_cbw_det : det_strategy rr_cbw.
Proof.
  std_det_tac.
  + destruct H0  as [[[[] _]|HC ] Hc ].
    destruct H0' as [[[[] _]|HC'] Hc'].
    specialize (IHt (x0, c) (x1, c') (conj eq_refl (conj HC  Hc))
                                     (conj H1      (conj HC' Hc'))).
    apply pair_equal_spec in IHt.
    f_equal; [f_equal|]; apply IHt.
  + apply rr_cbw_phased_form, (union_down_strategy_app _ _ _ _ _ H ) in H0.
    apply rr_cbw_phased_form, (union_down_strategy_app _ _ _ _ _ H') in H0'.
    apply ( left_strategy_det_on t1 t2) in IHt1.
    apply (right_strategy_det_on t1 t2) in IHt2.
    exact (
      (sequence_det_on _ _ _ (r_cbw_det _)
      (sequence_det_on _ _ _ IHt2 IHt1
      )) _ _ (conj H H0) (conj H' H0')).
Qed.

Lemma right_abs_strategy_det_on : ∀ t1 t2 s,
  (∃≤1 d, decomposes_into_in         t2  d    s) →
  (∃≤1 d, decomposes_into_in (app t1 t2) d (↘λ s)).
Proof.
  intros t1 t2 s H [C c] [C' c'] [H0 H1] [H0' H1'].
  destruct C  as [|[| |]]; try contradiction H1.
  destruct C' as [|[| |]]; try contradiction H1'.
  apply proj2 in H1. apply proj2 in H1'.
  inversion H0. inversion H0'. subst.
  specialize (H (_,_) (_,_) (conj eq_refl H1) (conj H6 H1')).
    inversion H. subst. reflexivity.
Qed.

Theorem cbwh_det : det_strategy cbwh.
Proof.
  std_det_tac.
  + destruct H0  as [[_ H0 ] H2 ].
    destruct H0' as [[_ H0'] H2'].
    specialize (IHt (x0, c) (x1, c') (conj eq_refl (conj H0  H2))
                                     (conj H1      (conj H0' H2'))).
    apply pair_equal_spec in IHt.
    f_equal; [f_equal|]; apply IHt.
  + apply cbwh_phased_form in H0.
    apply cbwh_phased_form in H0'.
    apply ( left_strategy_det_on t1 t2) in IHt1.
    apply (right_abs_strategy_det_on t1 t2) in IHt2.
    exact (
      (sequence_det_on _ _ _ IHt1
      (sequence_det_on _ _ _ IHt2
      (only_β_contraction_det (app t1 t2))
      )) _ _ (conj H H0) (conj H' H0')).
Qed.

Theorem lr_cbw_special_form : lr_cbw == lr_cbw_special.
Proof.
  etransitivity; [exact lr_cbw_phased_form|].
  apply union_proper; [|reflexivity].
  etransitivity.
  { eapply sequence_strategy_proper; [exact r_cbw_phased_form| reflexivity]. }
  unfold r_cbw_phased.
  etransitivity; [symmetry; apply sequence_strategy_assoc|].
  apply sequence_strategy_proper; [reflexivity|].
  etransitivity.
  { eapply sequence_strategy_proper; [|reflexivity].
    apply left_weak_strategy_β_contraction_commutative.
    etransitivity; [apply r_cbw_is_cbw|apply cbw_is_weak]. }
  etransitivity; [symmetry; apply sequence_strategy_assoc|].
  apply sequence_strategy_proper; [reflexivity|].
  etransitivity; [apply sequence_strategy_assoc|].
  apply sequence_strategy_proper; [|reflexivity].
  etransitivity; [apply phased_left_strategy|].
  apply left_strategy_proper.
  apply deterministic_extension.
  + apply r_cbw_dec.
  + apply lr_cbw_det.
  + apply r_cbw_is_lr_cbw.
Qed.

Theorem l_cbm_rl_cbm_det : det_strategy l_cbm ∧ det_strategy rl_cbm.
Proof.
  apply conj_forall.
  intros t; induction t as [|x t [IHt IHtr]|t1 [_ IHt1r] t2 [IHt2 IHt2r]];
  apply conj_forall; intros [C c];
  apply conj_forall; intros [C' c']; (split;
  intros [H H0] [H' H0']).
  + var_frames_tac H; var_frames_tac H'; reflexivity.
  + var_frames_tac H; var_frames_tac H'; reflexivity.
  + lam_frames_tac H;  [contradiction (proj2 H0) |].
    lam_frames_tac H'; [contradiction (proj2 H0')|].
    specialize (IHt (x0, c) (x1, c') (conj eq_refl H0)
                                     (conj H1      H0')).
    apply pair_equal_spec in IHt.
    f_equal; [f_equal|]; apply IHt.
  + lam_frames_tac H;
    [contradiction (proj2 H0)|contradiction (proj1 H0)].
  + apply l_cbm_phased_form, (union_down_strategy_app _ _ _ _ _ H ), rl_cbm_phased_form in H0.
    apply l_cbm_phased_form, (union_down_strategy_app _ _ _ _ _ H'), rl_cbm_phased_form in H0'.
    apply ( left_strategy_det_on t1 t2) in IHt1r.
    apply (right_strategy_det_on t1 t2) in IHt2r.
    apply (right_strategy_det_on t1 t2) in IHt2.
    exact (
      (sequence_det_on _ _ _ IHt1r
      (sequence_det_on _ _ _ IHt2r
      (sequence_det_on _ _ _ (only_β_contraction_det (app t1 t2))
                             IHt2)))
      _ _ (conj H H0) (conj H' H0')).
  + apply rl_cbm_phased_form in H0.
    apply rl_cbm_phased_form in H0'.
    apply ( left_strategy_det_on t1 t2) in IHt1r.
    apply (right_strategy_det_on t1 t2) in IHt2r.
    apply (right_strategy_det_on t1 t2) in IHt2.
    exact (
      (sequence_det_on _ _ _ IHt1r
      (sequence_det_on _ _ _ IHt2r
      (sequence_det_on _ _ _ (only_β_contraction_det (app t1 t2))
                             IHt2)))
      _ _ (conj H H0) (conj H' H0')).
Qed.

Corollary l_cbm_det : det_strategy l_cbm.
Proof. exact (proj1 l_cbm_rl_cbm_det). Qed.

Theorem r_cbm_rr_cbm_det : det_strategy r_cbm ∧ det_strategy rr_cbm.
Proof.
  apply conj_forall.
  intros t; induction t as [|x t [IHt IHtr]|t1 [_ IHt1r] t2 [IHt2 IHt2r]];
  apply conj_forall; intros [C c];
  apply conj_forall; intros [C' c']; (split;
  intros [H H0] [H' H0']).
  + var_frames_tac H; var_frames_tac H'; reflexivity.
  + var_frames_tac H; var_frames_tac H'; reflexivity.
  + lam_frames_tac H;  [contradiction (proj2 H0) |].
    lam_frames_tac H'; [contradiction (proj2 H0')|].
    specialize (IHt (x0, c) (x1, c') (conj eq_refl H0)
                                     (conj H1      H0')).
    apply pair_equal_spec in IHt.
    f_equal; [f_equal|]; apply IHt.
  + lam_frames_tac H;
    [contradiction (proj2 H0)|contradiction (proj1 H0)].
  + apply r_cbm_phased_form, (union_down_strategy_app _ _ _ _ _ H ), rr_cbm_phased_form in H0.
    apply r_cbm_phased_form, (union_down_strategy_app _ _ _ _ _ H'), rr_cbm_phased_form in H0'.
    apply ( left_strategy_det_on t1 t2) in IHt1r.
    apply (right_strategy_det_on t1 t2) in IHt2r.
    apply (right_strategy_det_on t1 t2) in IHt2.
    exact (
      (sequence_det_on _ _ _ IHt2r
      (sequence_det_on _ _ _ IHt1r
      (sequence_det_on _ _ _ (only_β_contraction_det (app t1 t2))
                             IHt2)))
      _ _ (conj H H0) (conj H' H0')).
  + apply rr_cbm_phased_form in H0.
    apply rr_cbm_phased_form in H0'.
    apply ( left_strategy_det_on t1 t2) in IHt1r.
    apply (right_strategy_det_on t1 t2) in IHt2r.
    apply (right_strategy_det_on t1 t2) in IHt2.
    exact (
      (sequence_det_on _ _ _ IHt2r
      (sequence_det_on _ _ _ IHt1r
      (sequence_det_on _ _ _ (only_β_contraction_det (app t1 t2))
                             IHt2)))
      _ _ (conj H H0) (conj H' H0')).
Qed.

Corollary r_cbm_det : det_strategy r_cbm.
Proof. exact (proj1 r_cbm_rr_cbm_det). Qed.

Theorem deterministic_strategies : ∀ s,
  In s [sdet; cbn; l_cbv; r_cbv; cbwh; l_cbw; r_cbw; low; ihs; head; lis; no;
        ll_cbw; lr_cbw; rl_cbw; rr_cbw; l_cbm; r_cbm; li; ri] → det_strategy s.
Proof.
  intros C [H|[H|[H|[H|[H|[H|[H|[H|[H|[H|[H|[H|[H|[H|[H|[H|[H|[H|[H|[H|[]]]]]]]]]]]]]]]]]]]]]; subst.
  + exact sdet_det.
  + exact cbn_det.
  + exact l_cbv_det.
  + exact r_cbv_det.
  + exact cbwh_det.
  + exact l_cbw_det.
  + exact r_cbw_det.
  + exact low_det.
  + exact ihs_det.
  + exact head_det.
  + exact lis_det.
  + exact no_det.
  + exact ll_cbw_det.
  + exact lr_cbw_det.
  + exact rl_cbw_det.
  + exact rr_cbw_det.
  + exact l_cbm_det.
  + exact r_cbm_det.
  + exact li_det.
  + exact ri_det.
Qed.
