Require Import Utf8.
Require Import Coq.Program.Basics Coq.Classes.Morphisms.
Require Import Coq.Bool.Bool Coq.Lists.List Coq.Strings.String.
Require Import Preliminaries Common Term Context Decomposition Phased.
Import ListNotations.

Definition up_strategy (x : string) (s : strategy) : strategy :=
  λ d, let (C, c) := d in s (Lam x :: C, c).

Definition left_up_strategy (t2 : term) (s : strategy) : strategy :=
  λ d, let (C, c) := d in s (Rapp t2 :: C, c).

Definition right_up_strategy (t1 : term) (s : strategy) : strategy :=
  λ d, let (C, c) := d in s (Lapp t1 :: C, c).

Ltac dec_aux_strategy_tac :=
  intros s dec_s [[|[| |] C] c]; try (right; exact id); exact (dec_s (C, c)).

Lemma dec_down_strategy : ∀ s, dec_set s → dec_set (↓ s).
Proof. dec_aux_strategy_tac. Qed.

Lemma dec_left_strategy : ∀ s, dec_set s → dec_set (↙ s).
Proof. dec_aux_strategy_tac. Qed.

Lemma dec_right_strategy : ∀ s, dec_set s → dec_set (↘ s).
Proof. dec_aux_strategy_tac. Qed.

Lemma dec_up_strategy : ∀ x s, dec_set s → dec_set (up_strategy x s).
Proof. intros x s dec_s [C c]. exact (dec_s _). Qed.

Lemma dec_left_up_strategy : ∀ t2 s, dec_set s → dec_set (left_up_strategy t2 s).
Proof. intros t2 s dec_s [C c]. exact (dec_s _). Qed.

Lemma dec_right_up_strategy : ∀ t1 s, dec_set s → dec_set (right_up_strategy t1 s).
Proof. intros t1 s dec_s [C c]. exact (dec_s _). Qed.

Theorem dec_strategy_dec_normal : ∀ s,
  dec_set s → ∀ t, { d | decomposes_into_in t d s } + {normal_form s t}.
Proof.
  intros s dec_s t. revert dec_s. revert s.
  induction t; intros s dec_s;
  [destruct (dec_s ([], var x)) as [H|H]
  |destruct (dec_s ([], lam x t)) as [H|H]
  |destruct (dec_s ([], app t1 t2)) as [H|H]];
  try (left; eexists ([], _); refine (conj _ H); reflexivity).
  + right. apply normal_var, H.
  + specialize (IHt _ (dec_up_strategy x _ dec_s)).
    destruct IHt as [[[C c] [H0 H1]]|IHt].
    { left. eexists; split; [|exact H1]. simpl. f_equal. exact H0. }
    right. apply normal_lam. refine (conj H _).
    intros C c H0 H1. apply IHt; clear IHt.
    exists (C, c). refine (conj H0 H1).
  + specialize (IHt1 _ (dec_left_up_strategy  t2 _ dec_s)).
    specialize (IHt2 _ (dec_right_up_strategy t1 _ dec_s)).
    destruct IHt1 as [[[C c] [H0 H1]]|IHt1].
    { left. eexists; split; [|exact H1]. simpl. f_equal. exact H0. }
    destruct IHt2 as [[[C c] [H0 H1]]|IHt2].
    { left. eexists; split; [|exact H1]. simpl. f_equal. exact H0. }
    right. apply normal_app. repeat split.
    - exact H.
    - intros C c H0 H1. apply IHt1; clear IHt1.
      exists (C, c). refine (conj H0 H1).
    - intros C c H0 H1. apply IHt2; clear IHt2.
      exists (C, c). refine (conj H0 H1).
Qed.

(** *** phased decidable determinitic substrategy *)

Lemma cbn_dec : dec_set cbn.
Proof.
  intros [C c]. induction C as [|[]]; cbn.
  + apply and_dec; [apply True_dec|].
    destruct c as [| |[]]; cbn; try (right; tauto).
    left; repeat split.
  + right. tauto.
  + destruct IHC as [IHC|IHC]; [left|right].
    - repeat split; apply IHC.
    - intros [[_ HC] Hc]. exact (IHC (conj HC Hc)).
  + right. tauto.
Qed.

Example not_phased_dec_substrategy : ¬ ∀ s1 s2,
  dec_set s1 → s1 ⊆ s2 → s1;; s2 == s2.
Proof.
  apply counterexample. exists cbn.
  apply counterexample. exists full_β.
  apply counterexample. exists cbn_dec.
  apply counterexample. eexists.
  { monotone_substrategy_tactic. apply subset_full. }
  apply counterexample. exists ([Rapp term_I; Lam "x"], (app term_I term_I)).
  intros [_ H].
  specialize (H (conj I (conj I I))).
  destruct H.
  + apply proj1, proj2, proj1 in H. exact H.
  + apply (proj1 H).
    eexists ([], _); split; [reflexivity|].
    repeat constructor.
Qed.

Theorem deterministic_extension : ∀ s1 s2,
  dec_set s1 → det_strategy s2 → s1 ⊆ s2 → s1;; s2 == s2.
Proof.
  intros s1 s2 dec_s1 det_s2 s1s2 d.
  assert (det_s1 := det_strategy_variance _ _ s1s2 det_s2).
  split; [apply (phased_substrategy _ _ s1s2)|].
  intros s2d.
  destruct (dec_strategy_dec_normal _ dec_s1 (recompose d))
    as [[d0 [H s1d0]]|s1d];
  [|refine (or_intror (conj s1d s2d))].
  assert (s2d0 := s1s2 _ s1d0).
  assert (H0 := det_s2 _ _ _  (conj H s2d0) (conj eq_refl s2d)); subst.
  destruct (dec_s1 d) as [s1d|s1d];
  [refine (or_introl s1d)|contradiction (s1d s1d0)].
Qed.

(** *** strongly specified deciders *)

Definition is_abs (e:term) : dec (abstraction e) :=
  match e with
  | lam _ _ => left I
  |       _ => right (False_ind _)
  end.

Definition is_abs_of {t':term → Prop} (d': ∀ e, dec (t' e))
  (x : string) (e' : term) : dec (abs_of t' (lam x e')) := d' e'.

Definition is_app_of {t1 t2:term → Prop}
  (d1: ∀ e, dec (t1 e)) (d2: ∀ e, dec (t2 e)) (e1 e2:term)
  : dec (app_of t1 t2 (app e1 e2)) := and_dec _ _ (d1 e1) (d2 e2).

(** *** rigid and hnf *)

Fixpoint is_rigid (t:term) : dec (rigid t).
Proof.
  destruct t.
  + left. constructor.
  + right. exact id.
  + destruct (is_rigid t1) as [H|H]; [left|right]; exact H.
Qed.

Fixpoint is_hnf (t:term) : dec (hnf t).
Proof.
  destruct (is_rigid t) as [H|H].
  { left. apply hnf_grammar. left. exact H. }
  destruct t; [|clear H|].
  + right. exact H.
  + destruct (is_hnf t) as [H|H]; [left|right]; exact H.
  + right. exact H.
Qed.

(** *** nf and neu *)

Fixpoint is_neu_weak (e:term) : bool :=
  match e with
  | var _ => true
  | lam _ _ => false
  | app e1 e2 => is_neu_weak e1 && is_nf_weak e2
  end
with is_nf_weak (e:term) : bool :=
  match e with
  | var _ => true
  | lam _ e' => is_nf_weak e'
  | app e1 e2 => is_neu_weak e1 && is_nf_weak e2
  end.

Lemma is_nf_neu_correct : ∀ e,
    ( nf e ↔ is_nf_weak e = true)
  ∧ (neu e ↔ is_neu_weak e = true).
Proof.
  intros e. induction e as [|? ? [IHnf _]|];
  simpl; do 2 split; auto; intros.
  1,2: apply IHnf, H.
  1: discriminate H.
  1,3: apply andb_true_iff.
  3,4: apply andb_true_iff in H.
  all: split; [apply IHe1|apply IHe2]; apply H.
Qed.

Definition is_nf (e:term) : dec (nf e).
Proof.
  destruct (is_nf_weak e) eqn:H; [left|right].
  + apply is_nf_neu_correct. assumption.
  + intros H0. apply is_nf_neu_correct in H0. congruence.
Qed.

Definition is_neu (e:term) : dec (neu e).
Proof.
  destruct (is_neu_weak e) eqn:H; [left|right].
  + apply is_nf_neu_correct. assumption.
  + intros H0. apply is_nf_neu_correct in H0. congruence.
Qed.

(** *** wnf and inert *)

Fixpoint is_inert_weak (e:term) : bool :=
  match e with
  | var _   => true
  | lam _ _ => false
  | app i e => is_inert_weak i && is_wnf_weak e
  end
with is_wnf_weak (e:term) : bool :=
  match e with
  | lam _ e'  => true
  | app e1 e2 => is_inert_weak e1 && is_wnf_weak e2
  | var _     => true
  end.

Lemma is_wnf_inert_correct : ∀ t,
    (  wnf t ↔   is_wnf_weak t = true)
  ∧ (inert t ↔ is_inert_weak t = true).
Proof.
  intros t. induction t as [| |s [IHsw IHsi] t [IHtw IHti]].
  all: simpl; split; try tauto.
  1: split; intros H; inversion H.
  all: rewrite andb_true_iff.
  all: split; intros [? ?]; split.
  all: apply IHsw + apply IHsi + apply IHtw + apply IHti; assumption.
Qed.

Definition is_wnf (t:term) : dec (wnf t).
Proof.
  destruct (is_wnf_weak t) eqn:H; [left|right].
  + apply is_wnf_inert_correct. assumption.
  + intros H0. apply is_wnf_inert_correct in H0. congruence.
Qed.

Lemma r_cbw_dec : dec_set r_cbw.
Proof.
  intros [C c]. induction C as [|[]]; cbn.
  + apply and_dec; [apply True_dec|].
    destruct c as [| |[]]; cbn; try (right; tauto).
    apply and_dec; [apply True_dec|].
    apply is_wnf.
  + right. tauto.
  + destruct IHC as [[HC Hc]|IHC].
    - destruct (is_wnf t); [left|right; tauto].
      repeat split; assumption.
    - right. intros [[_ HC] Hc]. exact (IHC (conj HC Hc)).
  + destruct IHC as [[HC Hc]|IHC]; [left|right].
    repeat split; assumption.
    intros [[_ HC] Hc]. exact (IHC (conj HC Hc)).
Qed.

(** *** mnf *)

Definition is_mnf_weak (e:term) : bool :=
  match e with
  | lam _ _ => true
  | _       => is_neu_weak e
  end.

Lemma is_mnf_correct : ∀ t, mnf t ↔ is_mnf_weak t = true.
Proof.
  intros t. induction t; try (simpl; tauto).
  + apply is_nf_neu_correct.
Qed.

Definition is_mnf (t:term) : dec (mnf t).
Proof.
  destruct (is_mnf_weak t) eqn:H; [left|right].
  + apply is_mnf_correct. assumption.
  + intros H0. apply is_mnf_correct in H0. congruence.
Qed.
