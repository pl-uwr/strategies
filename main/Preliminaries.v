(* adapted from:

  Yannick Forster, Gert Smolka:
  Call-by-Value Lambda Calculus as a Model of Computation in Coq.
  J. Autom. Reason. 63(2): 393-413 (2019) *)

(* ** Decidability *)

(* Search ({_} + {~ _}). *)
Definition dec (X : Prop) : Type := {X} + {~ X}.

Notation "'eq_dec' X" := (forall x y : X, dec (x=y)) (at level 70).

(* Register dec as a type class *)

Existing Class dec.

Definition decision (X : Prop) (D : dec X) : dec X := D.
Arguments decision X {D}.

Tactic Notation "decide" constr(p) :=
  destruct (decision p).
Tactic Notation "decide" constr(p) "as" simple_intropattern(i) := 
  destruct (decision p) as i.

(* Regiseva instance rules for dec *)

#[global]
Instance True_dec :  dec True :=
  left I.

#[global]
Instance False_dec :  dec False :=
  right (fun A => A).

#[global]
Instance impl_dec (X Y : Prop) :
  dec X -> dec Y -> dec (X -> Y).
Proof.
  unfold dec; tauto.
Defined.

#[global]
Instance and_dec (X Y : Prop) :
  dec X -> dec Y -> dec (X /\ Y).
Proof. 
  unfold dec; tauto.
Defined.

#[global]
Instance or_dec (X Y : Prop) :
  dec X -> dec Y -> dec (X \/ Y).
Proof.
  unfold dec; tauto.
Defined.

(* Coq standard modules make "not" and "iff" opaque for type class inference, can be seen with Print HintDb typeclass_instances. *)

#[global]
Instance not_dec (X : Prop) : 
  dec X -> dec (~ X).
Proof.
  intros [?|?]; [right|left]; tauto.
Defined.

#[global]
Instance iff_dec (X Y : Prop) :
  dec X -> dec Y -> dec (X <-> Y).
Proof.
  unfold iff. intros [|] [|]; (left; tauto) + (right; tauto).
Qed.

Lemma dec_DN X : 
  dec X -> ~~ X -> X.
Proof.
  unfold dec; tauto.
Qed.

Lemma dec_DM_and X Y :
  dec X -> dec Y -> ~ (X /\ Y) -> ~ X \/ ~ Y.
Proof. 
  unfold dec; tauto.
Qed.

Lemma dec_DM_impl X Y :
  dec X -> dec Y -> ~ (X -> Y) -> X /\ ~ Y.
Proof. 
  unfold dec; tauto.
Qed.

Lemma dec_prop_iff (X Y : Prop) : 
  (X <-> Y) -> dec X -> dec Y.
Proof.
  unfold dec; tauto.
Defined.

#[global]
Instance bool_eq_dec :
  eq_dec bool.
Proof.
  intros x y. hnf. decide equality.
Defined.

#[global]
Instance nat_eq_dec :
  eq_dec nat.
Proof.
  intros x y. hnf. decide equality.
Defined.

#[global]
Instance pair_eq_dec {A B : Type} :
  eq_dec A -> eq_dec B -> eq_dec (A * B).
Proof.
  intros A_dec B_dec [a1 b1] [a2 b2].
  eapply dec_prop_iff; [|exact (and_dec _ _ (A_dec a1 a2) (B_dec b1 b2))].
  split; [intros []; f_equal; assumption|].
  intros H. inversion H. split; reflexivity.
Qed.
