Require Import Utf8.
Require Import Coq.Program.Basics Coq.Classes.Morphisms.
Require Import Coq.Lists.List.
Require Import Coq.Strings.String.
Require Import Preliminaries Common Term Context Decomposition.
Import ListNotations.

Definition left_strategy (s : strategy) : strategy :=
  λ d, match d with (Rapp _ :: C, c) => s (C, c) | _ => False end.

Definition right_strategy (s : strategy) : strategy :=
  λ d, match d with (Lapp _ :: C, c) => s (C, c) | _ => False end.

Definition down_strategy (s : strategy) : strategy :=
  λ d, match d with (Lam _  :: C, c) => s (C, c) | _ => False end.

Definition sequence_strategy (r s: strategy) : strategy :=
  λ d, r d ∨ (normal_form r (recompose d) ∧ s d).

Definition left_abs_strategy (s : strategy) : strategy :=
  λ d, match d with (Rapp t2 :: C, c) => abstraction t2 ∧ s (C, c) | _ => False end.
Definition right_abs_strategy (s : strategy) : strategy :=
  λ d, match d with (Lapp t1 :: C, c) => abstraction t1 ∧ s (C, c) | _ => False end.

Notation "↙" := left_strategy.
Notation "↘" := right_strategy.
Notation "↓" := down_strategy.
Notation "'β'" := only_β_contraction.
Infix    ";;" := sequence_strategy (at level 60, right associativity).

Notation "↙λ" := left_abs_strategy.
Notation "↘λ" := right_abs_strategy.
Notation "'βλ'" := only_βλ_contraction.

Lemma sequence_strategy_empty_r : ∀ s, s;; ∅ == s.
Proof.
  intros s d. split.
  + intros [H|[_ []]]. exact H.
  + intros H. left. exact H.
Qed.

Lemma sequence_strategy_empty_l : ∀ s, ∅;; s == s.
Proof.
  intros s d. split.
  + intros [[]|[_ H]]. exact H.
  + intros H. right. split.
    - intros [_ [_ []]].
    - exact H.
Qed.

Lemma normal_form_sequence_strategy : ∀ r s,
  normal_form (r;; s) == normal_form r ∩ normal_form s.
Proof.
  intros r s t.
  split; [intros H|firstorder].
  assert(Hn : normal_form r t); [|split; [exact Hn|]];
  intros [d [? ?]]; subst; apply H;
  exists d; split; [|left| |right; split]; auto.
Qed.

Lemma sequence_strategy_assoc : ∀ q r s, q;; (r;; s) == (q;; r);; s.
Proof.
  intros q r s d. split.
  + intros [qd|[nqd [rd|[nrd sd]]]].
    - left. left. exact qd.
    - left. right. split; assumption.
    - right. split; [|assumption].
      apply normal_form_sequence_strategy. split; assumption.
  + intros [[qd|[nqd rd]]|[nqrd sd]].
    - left. exact qd.
    - right. split; [assumption|]. left. exact rd.
    - apply normal_form_sequence_strategy in nqrd.
      destruct nqrd as [nqd nrd].
      right. split; [assumption|].
      right. split; [assumption|].
      exact sd.
Qed.

Example l_cbv_not_simple_phased_strategy : ¬ l_cbv == ↙ l_cbv;; ↘ l_cbv;; β.
Proof.
  apply counterexample.
  exists ([Lapp (var "x")], (app term_I term_I)).
  intros [_ H]. apply H; clear H.
  right. split; [|left; cbn; auto].
  apply normal_app.
  repeat split; [|intros C c H; var_frames_tac H|];
  cbn; tauto.
Qed.

Definition   sdet_phased := ↙λ sdet;; βλ : strategy.
Definition  sdet_phased2 := βλ;; ↙λ sdet.
Definition    cbn_phased := ↙  cbn;; β.
Definition   cbn_phased2 := β;;  ↙  cbn.
Definition   head_phased := (β;; ↙ head      ) ∪ ↓ head.
Definition     no_phased := (β;; ↙ no;;  ↘ no) ∪ ↓ no.
Definition    no'_phased := ↙  no';; β;; ↘ no.
Definition    no_phased2 := no' ∪ ↓ no'.
Definition    ihs_phased := (↙ ihs;; β                ) ∪ ↓ ihs.
Definition    lis_phased := (↙ ihs;; β;; ↙ lis;; ↘ lis) ∪ ↓ lis.
Definition     hs_phased := β ∪ ↙hs ∪ ↓hs.
Definition full_β_phased := β ∪ ↙full_β ∪ ↘full_β ∪ ↓full_β.

Definition   cbwh_phased := ↙ cbwh;; ↘λ cbwh;; β.

Definition  l_cbw_phased := ↙ l_cbw;; ↘  l_cbw;; β.
Definition  r_cbw_phased := ↘ r_cbw;; ↙  r_cbw;; β.
Definition    low_phased := β;; ↙ low;; ↘ low.
Definition   low_phased2 := ↙ low;; β;; ↘ low.
Definition  l_cbv_phased := ↙ l_cbv;; ↘λ l_cbv;; βλ.
Definition  r_cbv_phased := ↘ r_cbv;; ↙λ r_cbv;; βλ.
Definition    cbv_phased := βλ ∪ ↙cbv ∪ ↘cbv.
Definition   weak_phased := β ∪ ↙weak ∪ ↘weak.
Definition    cbw_phased := (↙cbw ∪ ↘cbw);; β.
Definition   scbw_phased := (cbw;; (↙scbw ∪ ↘scbw)) ∪ ↓scbw.

Definition ll_cbw_phased := (l_cbw;; ↙ ll_cbw;; ↘ ll_cbw) ∪ ↓ ll_cbw.
Definition rl_cbw_phased := (l_cbw;; ↘ rl_cbw;; ↙ rl_cbw) ∪ ↓ rl_cbw.
Definition lr_cbw_phased := (r_cbw;; ↙ lr_cbw;; ↘ lr_cbw) ∪ ↓ lr_cbw.
Definition rr_cbw_phased := (r_cbw;; ↘ rr_cbw;; ↙ rr_cbw) ∪ ↓ rr_cbw.
Definition lr_cbw_special := (↘ r_cbw;; β;; ↙ lr_cbw;; ↘ lr_cbw) ∪ ↓ lr_cbw.

Definition  l_cbm_phased := rl_cbm ∪ ↓ l_cbm.
Definition rl_cbm_phased := ↙ rl_cbm;; ↘ rl_cbm;; β;; ↘ l_cbm.
Definition  r_cbm_phased := rr_cbm ∪ ↓ r_cbm.
Definition rr_cbm_phased := ↘ rr_cbm;; ↙ rr_cbm;; β;; ↘ r_cbm.
Definition    cbm_phased := rigid_cbm ∪ ↓ cbm.
Definition rigid_cbm_phased := (↙ rigid_cbm ∪ ↘ rigid_cbm);; β;; ↘ cbm.

Definition     li_phased := (↙   li;; ↘ li;;   β) ∪ ↓ li.
Definition     ri_phased := (↘   ri;; ↙ ri;;   β) ∪ ↓ ri.
Definition    inn_phased :=((↙  inn ∪ ↘inn);;  β) ∪ ↓ inn.

Lemma normal_left_strategy_var : ∀ s x, normal_form (↙ s) (var x).
Proof. intros s x. apply normal_var. tauto. Qed.

Lemma normal_right_strategy_var : ∀ s x, normal_form (↘ s) (var x).
Proof. intros s x. apply normal_var. tauto. Qed.

Lemma normal_left_abs_strategy_var : ∀ s x, normal_form (↙λ s) (var x).
Proof. intros s x. apply normal_var. tauto. Qed.

Lemma normal_right_abs_strategy_var : ∀ s x, normal_form (↘λ s) (var x).
Proof. intros s x. apply normal_var. tauto. Qed.

Lemma normal_down_strategy_var : ∀ s x, normal_form (↓ s) (var x).
Proof. intros s x. apply normal_var. tauto. Qed.

Lemma normal_only_β_contraction_var : ∀ x, normal_form β (var x).
Proof. intros x. apply normal_var. firstorder. Qed.

Lemma normal_only_βλ_contraction_var : ∀ x, normal_form βλ (var x).
Proof. intros x. apply normal_var. firstorder. Qed.

Lemma normal_left_strategy_lam : ∀ s x t, normal_form (↙ s) (lam x t).
Proof. intros s x t. apply normal_lam. tauto. Qed.

Lemma normal_right_strategy_lam : ∀ s x t, normal_form (↘ s) (lam x t).
Proof. intros s x t. apply normal_lam. tauto. Qed.

Lemma normal_down_strategy_lam : ∀ s x t, normal_form s t ↔ normal_form (↓ s) (lam x t).
Proof with firstorder.
  intros s x t. split.
  + intros H. apply normal_lam...
  + intros H [[C c] []]. subst. apply H. eexists (Lam x :: C, c)...
Qed.

Lemma normal_left_abs_strategy_lam  : ∀ s x t, normal_form (↙λ s) (lam x t).
Proof. intros s x t. apply normal_lam. tauto. Qed.
Lemma normal_right_abs_strategy_lam : ∀ s x t, normal_form (↘λ s) (lam x t).
Proof. intros s x t. apply normal_lam. tauto. Qed.

Lemma normal_only_β_contraction_lam : ∀ x t, normal_form β (lam x t).
Proof. intros x t. apply normal_lam. firstorder. Qed.

Lemma normal_only_βλ_contraction_lam : ∀ x t, normal_form βλ (lam x t).
Proof. intros x t. apply normal_lam. firstorder. Qed.

Lemma normal_left_strategy_app : ∀ s t1 t2,
  normal_form s t1 ↔ normal_form (↙ s) (app t1 t2).
Proof with firstorder.
  intros s t1 t2. split.
  + intros H. apply normal_app...
  + intros H [[C c] []]. subst. apply H. eexists (Rapp t2 :: C, c)...
Qed.

Lemma normal_right_strategy_app : ∀ s t1 t2,
  normal_form s t2 ↔ normal_form (↘ s) (app t1 t2).
Proof with firstorder.
  intros s t1 t2. split.
  + intros H. apply normal_app...
  + intros H [[C c] []]. subst. apply H. eexists (Lapp t1 :: C, c)...
Qed.

Lemma normal_left_abs_strategy_app : ∀ s t1 t2,
  normal_form s t1 ∨ (¬ abstraction t2) ↔ normal_form (↙λ s) (app t1 t2).
Proof with firstorder.
  intros s t1 t2. split.
  + intros H. apply normal_app...
  + intros H. destruct t2; try (right; tauto). left.
    intros [[C c] []]. apply H. eexists (Rapp (lam x t2) :: C, c)...
    simpl. f_equal. exact H0.
Qed.

Lemma normal_right_abs_strategy_app : ∀ s t1 t2,
  (¬ abstraction t1) ∨ normal_form s t2 ↔ normal_form (↘λ s) (app t1 t2).
Proof with firstorder.
  intros s t1 t2. split.
  + intros H. apply normal_app...
  + intros H. destruct t1; try (left; tauto). right.
    intros [[C c] []]. apply H. eexists (Lapp (lam x t1) :: C, c)...
    simpl. f_equal. exact H0.
Qed.

Lemma normal_down_strategy_app : ∀ s t1 t2, normal_form (↓ s) (app t1 t2).
Proof. intros s t1 t2. apply normal_app. firstorder. Qed.

Lemma normal_union_down_strategy_app : ∀ s1 s2 t1 t2,
  normal_form (s1 ∪ ↓ s2) (app t1 t2) ↔ normal_form s1 (app t1 t2).
Proof.
  intros s1 s2 t1 t2. split;
  intros H [d [H0 H1]]; apply H; clear H; exists d; (split; [assumption|]).
  + left. exact H1.
  + destruct H1; [assumption|]. exfalso.
    destruct d. app_frames_tac H0; exact H.
Qed.

Lemma normal_only_β_contraction_app : ∀ t1 t2,
  ¬ abstraction t1 ↔ normal_form β (app t1 t2).
Proof with firstorder.
  intros t1 t2. split.
  + intros H. apply normal_app...
  + intros H ?. apply H. eexists ([], _)...
Qed.

Lemma normal_only_βλ_contraction_app : ∀ t1 t2,
  ¬ abstraction t1 ∨ ¬ abstraction t2 ↔ normal_form βλ (app t1 t2).
Proof with firstorder.
  intros t1 t2. split.
  + intros H. apply normal_app...
  + intros H.
    destruct t1; try (left; tauto).
    right. intros abs_t2.
    apply H. eexists ([], _)...
Qed.

Lemma normal_on_term_aux : ∀ t s1 s2,
  strategies_equal_on_term t s1 s2 → normal_form s1 t → normal_form s2 t.
Proof.
  intros t s1 s2 H H0 [d' [H1 H2]].
  exact (H0 (ex_intro _ d' (conj H1 ((proj2 (H _ H1)) H2)))).
Qed.

Lemma normal_on_term : ∀ t s1 s2,
  strategies_equal_on_term t s1 s2 → normal_form s1 t ↔ normal_form s2 t.
Proof.
  intros t s1 s2 H. split; [|symmetry in H];
  apply normal_on_term_aux; assumption.
Qed.

Lemma normal_phased_strategy_nil : is_full (normal_form ∅).
Proof. intros t [d [_ H]]. exact H. Qed.

Lemma normal_union_strategy : ∀ s1 s2,
  normal_form (s1 ∪ s2) == (normal_form s1 ∩ normal_form s2).
Proof.
  intros s1 s2 t. split.
  + intros H. split;
    intros [d [H0 H1]]; apply H; clear H; exists d; (split; [assumption|]);
    [left|right]; assumption.
  + intros [H1 H2].
    intros [d [H [H0|H0]]]; [apply H1|apply H2]; exists d; split; assumption.
Qed.

Ltac normal_form_sequence_strategy_tac :=
  apply normal_form_sequence_strategy; split;
  [|try normal_form_sequence_strategy_tac].


#[global]
Instance left_strategy_variance : Proper (subset ++> subset) left_strategy.
Proof. intros s1 s2 s12 [[|[]] c]; try (exact id). apply s12. Qed.

#[global]
Instance left_strategy_proper : Proper (set_eq ==> set_eq) left_strategy.
Proof.
  intros s1 s2 s12. apply two_inclusions.
  split; apply left_strategy_variance, two_inclusions; [symmetry|]; apply s12.
Qed.

#[global]
Instance right_strategy_variance : Proper (subset ++> subset) right_strategy.
Proof. intros s1 s2 s12 [[|[]] c]; try (exact id). apply s12. Qed.

#[global]
Instance right_strategy_proper : Proper (set_eq ==> set_eq) right_strategy.
Proof.
  intros s1 s2 s12. apply two_inclusions.
  split; apply right_strategy_variance, two_inclusions; [symmetry|]; apply s12.
Qed.

#[global]
Instance down_strategy_variance : Proper (subset ++> subset) down_strategy.
Proof. intros s1 s2 s12 [[|[]] c]; try (exact id). apply s12. Qed.

#[global]
Instance down_strategy_proper : Proper (set_eq ==> set_eq) down_strategy.
Proof.
  intros s1 s2 s12. apply two_inclusions.
  split; apply down_strategy_variance, two_inclusions; [symmetry|]; apply s12.
Qed.

Lemma left_weak_strategy_β_contraction_commutative : ∀ w,
  w ⊆ weak → ↙ w;; β == β;; ↙ w.
Proof.
  intros w H H0.
  strategies_equal_on_all_terms_tac; split; try (cbv; tauto).
  + intros [[_ [H1 _]]|[_ H1]].
    - right. split.
      { apply normal_left_strategy_app, (normal_form_variance _ _ H),
              abstraction_is_weak_normal_form, H1. }
      repeat split. exact H1.
    - left. exact H1.
  + intros [H1|[_ [[] _]]].
    right; split; [|exact H1].
    apply H in H1.
    intros [[C0 c0] [H2 [H3 H4]]].
    destruct C0; [clear H3|exact H3].
    simpl in H2. subst.
    destruct H4 as [H4 _].
    apply abstraction_is_weak_normal_form in H4.
    apply H4. eexists; split; [|exact H1]. reflexivity.
Qed.

Lemma left_abs_weak_strategy_βλ_contraction_commutative : ∀ w,
  w ⊆ weak → ↙λ w;; βλ == βλ;; ↙λ w.
Proof.
  intros w H H0.
  strategies_equal_on_all_terms_tac; split; try (cbv; tauto).
  + intros [[_ [H1 H2]]|[_ H1]].
    - right. split.
      { apply normal_left_abs_strategy_app, or_introl, (normal_form_variance _ _ H),
              abstraction_is_weak_normal_form, H1. }
      repeat split; assumption.
    - left. exact H1.
  + intros [H1|[_ [[] _]]].
    right; split; [|exact H1].
    apply proj2, H in H1.
    intros [[C0 c0] [H2 [H3 H4]]].
    destruct C0; [clear H3|exact H3].
    simpl in H2. subst.
    destruct H4 as [H4 _].
    apply abstraction_is_weak_normal_form in H4.
    apply H4. eexists; split; [|exact H1]. reflexivity.
Qed.

#[global]
Instance sequence_strategy_proper_aux : Proper (set_eq ==> set_eq ==> subset) sequence_strategy.
Proof.
  intros r r0 rr0 s s0 ss0 d [H|H]; [left; apply rr0, H|right].
  split; [|apply ss0, H].
  apply (normal_form_proper _ _ rr0), H.
Qed.

#[global]
Instance sequence_strategy_proper : Proper (set_eq ==> set_eq ==> set_eq) sequence_strategy.
Proof.
  intros r r0 rr0 s s0 ss0.
  apply two_inclusions.
  split; apply sequence_strategy_proper_aux;
  assumption + symmetry; assumption.
Qed.

Lemma phased_substrategy : ∀ s1 s2,
  s1 ⊆ s2 → s1;; s2 ⊆ s2.
Proof.
  intros s1 s2 s1s2 d [s1d|[_ s2d]]; [exact (s1s2 _ s1d)|exact s2d].
Qed.

(* Lemma deterministic_extension in Decider.v *)

Lemma phased_left_strategy : ∀ s s',
  ↙ s;; ↙ s' == ↙ (s;; s').
Proof.
  intros s s'.
  strategies_equal_on_all_terms_tac; split; try (cbv; tauto);
  (intros [H0|[H0 H1]]; [left; exact H0|right];
    split; [|exact H1]).
  + eapply normal_left_strategy_app, H0.
  +  apply normal_left_strategy_app, H0.
Qed.

Lemma phased_right_strategy : ∀ s s',
  ↘ s;; ↘ s' == ↘ (s;; s').
Proof.
  intros s s'.
  strategies_equal_on_all_terms_tac; split; try (cbv; tauto);
  (intros [H0|[H0 H1]]; [left; exact H0|right];
    split; [|exact H1]).
  + eapply normal_right_strategy_app, H0.
  +  apply normal_right_strategy_app, H0.
Qed.

Lemma phased_down_strategy : ∀ s s',
  ↓ s;; ↓ s' == ↓ (s;; s').
Proof.
  intros s s'.
  strategies_equal_on_all_terms_tac; split; try (cbv; tauto);
  (intros [H0|[H0 H1]]; [left; exact H0|right];
    split; [|exact H1]).
  + eapply normal_down_strategy_lam, H0.
  + eapply normal_down_strategy_lam in H0. exact H0.
Qed.

Lemma phased_left_abs_strategy : ∀ s s',
  ↙λ s;; ↙λ s' == ↙λ (s;; s').
Proof.
  intros s s'.
  strategies_equal_on_all_terms_tac; split; try (cbv; tauto).
  + intros [[H0 H2]|[H0 [H1 H2]]].
    - refine (conj H0 _). left. exact H2.
    - refine (conj H1 _). right. split.
      * eapply normal_left_abs_strategy_app in H0.
        destruct H0 as [H0|H0]; [exact H0|contradiction (H0 H1)].
      * assumption.
  + intros [H0 [H1|[H1 H2]]].
    - left. exact (conj H0 H1).
    - right. split.
      * apply normal_left_abs_strategy_app. left. assumption.
      * split; assumption.
Qed.

Lemma phased_right_abs_strategy : ∀ s s',
  ↘λ s;; ↘λ s' == ↘λ (s;; s').
Proof.
  intros s s'.
  strategies_equal_on_all_terms_tac; split; try (cbv; tauto).
  + intros [[H0 H2]|[H0 [H1 H2]]].
    - refine (conj H0 _). left. exact H2.
    - refine (conj H1 _). right. split.
      * eapply normal_right_abs_strategy_app in H0.
        destruct H0 as [H0|H0]; [contradiction (H0 H1)|exact H0].
      * assumption.
  + intros [H0 [H1|[H1 H2]]].
    - left. exact (conj H0 H1).
    - right. split.
      * apply normal_right_abs_strategy_app. right. assumption.
      * split; assumption.
Qed.

Theorem phase_distributive_over_sequencing : ∀ Ж,
  In Ж [↙;↘;↓;↙λ;↘λ] → ∀ s s', (Ж s;; Ж s') == Ж (s;; s').
Proof.
  intros Ж [H|[H|[H|[H|[H|[]]]]]]; subst.
  + exact phased_left_strategy.
  + exact phased_right_strategy.
  + exact phased_down_strategy.
  + exact phased_left_abs_strategy.
  + exact phased_right_abs_strategy.
Qed.

Definition phased_lemma (s s' : strategy) (f : term → Prop) : Prop :=
  ∀ t, strategies_equal_on_term t s s' ∧ (f t ↔ normal_form s' t).

Lemma phased_lemma_first_corollary : ∀ s s' f,
  phased_lemma s s' f → s == s'.
Proof. intros s s' f H t. exact (proj1 (H _) _ eq_refl). Qed.

Lemma phased_lemma_second_corollary : ∀ s s' f,
  phased_lemma s s' f → normal_form s == f.
Proof.
  intros. etransitivity; intros t; [apply normal_on_term|symmetry]; apply H.
Qed.

Lemma normal_left_strategy_app_aux : ∀ s s' f t1 t2,
  (f t1 ↔ normal_form s' t1) →
  strategies_equal_on_term t1 s s' →
  (f t1 ↔ normal_form (↙ s) (app t1 t2)).
Proof.
  intros s s' f t1 t2 IHnt1 IHt1.
  apply normal_on_term in IHt1.
  split; intros f_t1.
  +  apply normal_left_strategy_app, IHt1, IHnt1, f_t1.
  + eapply IHnt1, IHt1, normal_left_strategy_app, f_t1.
Qed.

Lemma normal_right_strategy_app_aux : ∀ s s' f t1 t2,
  (f t2 ↔ normal_form s' t2) →
  strategies_equal_on_term t2 s s' →
  (f t2 ↔ normal_form (↘ s) (app t1 t2)).
Proof.
  intros s s' f t1 t2 IHnt2 IHt2.
  apply normal_on_term in IHt2.
  split; intros f_t2.
  +  apply normal_right_strategy_app, IHt2, IHnt2, f_t2.
  + eapply IHnt2, IHt2, normal_right_strategy_app, f_t2.
Qed.

Ltac assert_normals_var S x :=
  assert (Hl := normal_left_strategy_var S x);
  assert (Hr := normal_right_strategy_var S x);
  assert (Hd := normal_down_strategy_var S x);
  assert (Hc := normal_only_β_contraction_var x).

Ltac assert_normals_lam S x t :=
  assert (Hl := normal_left_strategy_lam S x t);
  assert (Hr := normal_right_strategy_lam S x t);
  assert (Hc := normal_only_β_contraction_lam x t).

Ltac phased_lemma_by_induction S :=
  intros t; induction t as [x|x t [IHt IHnt]|t1 [IHt1 IHnt1] t2 [IHt2 IHnt2]];
  [ assert_normals_var S x
  | assert_normals_lam S x t
  | apply (normal_left_strategy_app_aux  _ _ _ t1 t2 IHnt1) in IHt1;
    apply (normal_right_strategy_app_aux _ _ _ t1 t2 IHnt2) in IHt2;
    clear IHnt1; clear IHnt2].
