Require Import Utf8.
Require Import Coq.Program.Basics Coq.Classes.Morphisms.
Require Import Coq.Lists.List.
Require Import Coq.Strings.String.
Require Import Preliminaries Common Term Context Decomposition Phased Decider.
Import ListNotations.

(** *** decompose correctness *)

Definition decompose_decomposes (f: term → option decomposition) : Prop :=
  ∀ t d, f t = Some d → t = recompose d.

Definition decompose_sound (f: term → option decomposition) (s : strategy) : Prop :=
  ∀ t d, f t = Some d → s d.

Definition decompose_complete (f: term → option decomposition) (s : strategy) : Prop :=
  ∀ d, s d → f (recompose d) = Some d.

Definition decompose_correct (f: term → option decomposition) (s : strategy) : Prop :=
  decompose_decomposes f ∧ decompose_sound f s ∧ decompose_complete f s.

Definition decompose_correct_on (f: term → option decomposition) (s : strategy) (t : term) : Prop :=
  ∀ d, (f t = Some d ↔ decomposes_into_in t d s).

(** *** decompose correct properties *)

Lemma decompose_correct_on_all : ∀ f s,
  (∀ t, decompose_correct_on f s t) → decompose_correct f s.
Proof.
  intros f s H. repeat split.
  + intros t d. apply (H t d).
  + intros t d. apply (H t d).
  + intros d H0. exact (proj2 (H _ d) (conj eq_refl H0)).
Qed.

Lemma decompose_sound_normal_form : ∀ t s f,
  decompose_decomposes f →
  decompose_sound f s →
  normal_form s t →
  f t = None.
Proof.
  intros t s f Hd Hs H.
  destruct (f t) eqn:H0; [|reflexivity].
  specialize (Hd _ _ H0).
  specialize (Hs _ _ H0).
  clear H0. subst.
  contradiction (H (ex_intro _ _ (conj eq_refl Hs))).
Qed.

Lemma decompose_complete_None : ∀ t s f,
  decompose_complete f s →
  f t = None →
  normal_form s t.
Proof.
  intros t s f Hc H [d [H0 H1]].
  specialize (Hc _ H1).
  subst. rewrite H in Hc. inversion Hc.
Qed.

Lemma decompose_correct_on_normal_form : ∀ t s f,
  decompose_correct_on f s t →
  normal_form s t →
  f t = None.
Proof.
  intros t s f Hc H.
  destruct (f t) eqn:H0; [|reflexivity].
  apply Hc in H0.
  unfold decomposes_into_in in H0.
  contradiction (H (ex_intro _ _ H0)).
Qed.

Lemma decompose_correct_on_None : ∀ t s f,
  decompose_correct_on f s t →
  f t = None →
  normal_form s t.
Proof.
  intros t s f Hc H [d H0].
  apply Hc in H0.
  rewrite H in H0. inversion H0.
Qed.

(** *** decompose strategy *)

Definition decompose_strategy (f: term → option decomposition) : strategy :=
  λ d, f (recompose d) = Some d.

#[global]
Instance decompose_strategy_proper : Proper (ext_eq ==> set_eq) decompose_strategy.
Proof. intros f1 f2 f12 d. unfold decompose_strategy. rewrite f12. reflexivity. Qed.

Lemma decompose_strategy_complete : ∀ f,
  decompose_complete f (decompose_strategy f).
Proof. intros f d H. exact H. Qed.

Lemma decompose_strategy_sound : ∀ f,
  decompose_decomposes f → decompose_sound f (decompose_strategy f).
Proof.
  intros f H0 t d H.
  rewrite (H0 t d H) in H.
  exact H.
Qed.

Lemma decompose_strategy_set_eq : ∀ f s,
  s == decompose_strategy f → decompose_complete f s.
Proof. intros f s H d H0. apply H in H0. exact H0. Qed.

Lemma decompose_strategy_correct : ∀ f s,
  decompose_correct f s → decompose_strategy f == s.
Proof.
  intros f s [_ [Hs Hc]] d.
  split.
  + intros H. apply Hs in H. exact H.
  + intros H. apply Hc in H. exact H.
Qed.

Lemma normal_form_decompose_strategy : ∀ f s t,
  decompose_correct_on f s t →
  normal_form (decompose_strategy f) t →
  f t = None.
Proof.
  intros f s t H H0.
  destruct (f t) as [d|] eqn:H1; [|reflexivity].
  exfalso. apply H0. clear H0.
  destruct (proj1 (H _) H1) as [H2 _].
  exists d. split.
  + exact H2.
  + unfold decompose_strategy.
    etransitivity; [|exact H1].
    f_equal. symmetry. exact H2.
Qed.

(** *** local decomposers *)

Definition dec_cons (f:frame) : decomposition → decomposition :=
  fst_map (cons f).

Definition down_decompose (f: term → option decomposition)
  (t : term) : option decomposition :=
  match t with
  | lam  x  t => f  t |> option_map (dec_cons (Lam   x))
  | _         => None
  end.

Definition right_decompose (f: term → option decomposition)
  (t : term) : option decomposition :=
  match t with
  | app t1 t2 => f t2 |> option_map (dec_cons (Lapp t1))
  | _         => None
  end.

Definition left_decompose (f: term → option decomposition)
  (t : term) : option decomposition :=
  match t with
  | app t1 t2 => f t1 |> option_map (dec_cons (Rapp t2))
  | _         => None
  end.

Definition contrex_decompose (t : term) : option decomposition :=
  match t with
  | app t1 t2 => if is_abs t1 then Some ([], app t1 t2) else None
  | _         => None
  end.

Definition sequence_decompose (f1 f2: term → option decomposition)
  (t : term) : option decomposition :=
  option_append (f1 t) (f2 t).

(** *** local decomposers properties *)

Lemma only_β_contraction_correct :
  decompose_correct contrex_decompose β.
Proof.
  repeat split.
  + intros [| |[]] d; simpl; intros H;
    try (inversion H; fail).
    apply Some_inj in H.
    subst.
    reflexivity.
  + intros [| |[]] d; simpl; intros H;
    try (inversion H; fail).
    apply Some_inj in H.
    subst.
    repeat split.
  + intros [C c] [HC Hc].
    apply Hole_inv in HC. subst.
    invert_by app_of_inv Hc.
    invert_by abstraction_inv H0.
    reflexivity.
Qed.

Lemma contrex_decompose_app_Some_inv : ∀ t1 t2 C c,
  contrex_decompose (app t1 t2) = Some (C, c) →
  abstraction t1 ∧ C = [] ∧ c = app t1 t2.
Proof.
  intros [] t2 C c H; simpl in H; try (inversion H; fail).
  apply Some_inj, pair_equal_spec in H. destruct H. simpl. auto.
Qed.

Lemma sequence_strategy_correct_on : ∀ f1 f2 s1 s2 t,
  decompose_correct_on f1 s1 t →
  decompose_correct_on f2 s2 t →
  decompose_correct_on (sequence_decompose f1 f2) (s1;; s2) t.
Proof.
  intros f1 f2 s1 s2 t H1 H2 d.
  unfold decompose_strategy, sequence_decompose.
  split.
  + intros H0.
    destruct (f1 _) eqn:H3.
    - apply Some_inj in H0. subst.
      split; [|left]; apply H1, H3.
    - destruct (f2 _) eqn:H4; [|inversion H0].
      apply Some_inj in H0. subst.
      split.
      * apply H2, H4.
      * right. split.
        { apply (decompose_correct_on_None _ _ _ H1) in H3.
          apply H2 in H4. destruct H4. subst. exact H3. }
        apply H2, H4.
  + intros [Hd [H0|[H0 H3]]].
    - rewrite (proj2 (H1 _ ) (conj Hd H0)).
      reflexivity.
    - subst.
      rewrite (decompose_correct_on_normal_form _ _ _ H1 H0).
      rewrite option_append_None_l.
      apply H2. exact (conj eq_refl H3).
Qed.

(** *** cbn *)

Fixpoint cbn_decompose (t : term): option decomposition :=
  sequence_decompose
    (left_decompose cbn_decompose)
    contrex_decompose
  t.

Lemma cbn_decompose_unfold : cbn_decompose =ext
  sequence_decompose
    (left_decompose cbn_decompose)
    contrex_decompose.
Proof. intros []; reflexivity. Qed.

Definition cbn_decomposition : strategy := decompose_strategy cbn_decompose.

Definition  cbn_decomposition_phased : strategy :=
  ↙  cbn_decomposition;; β.

Lemma cbn_decompose_lemma : decompose_correct cbn_decompose cbn_decomposition_phased.
Proof.
  apply decompose_correct_on_all.
  induction t; intros [C c]; split.
  + intros H. inversion H.
  + intros [H H0].
    var_frames_tac H.
    destruct H0 as [[]|[_ [_ []]]].
  + intros H. inversion H.
  + intros [H H0].
    lam_frames_tac H.
    - destruct H0 as [[]|[_ [_ []]]].
    - destruct H0 as [[]|[_ [[] _]]].
  + intros H.
    simpl in H. unfold sequence_decompose in H.
    destruct (left_decompose _) as [d|] eqn:H0.
    { apply Some_inj in H. subst.
      simpl in H0.
      apply option_map_Some in H0.
      destruct H0 as [[C' c'] [H H0]].
      apply IHt1 in H.
      apply pair_equal_spec in H0.
      destruct H, H0.
      subst.
      split.
      + reflexivity.
      + left. apply IHt1. repeat split. exact H1. }
    simpl in H.
    destruct t1; simpl in H; try (inversion H; fail).
    apply Some_inj, pair_equal_spec in H.
    destruct H. subst.
    split.
    - reflexivity.
    - right. repeat split.
      apply normal_left_strategy_app.
      intros [d [H H1]].
      unfold cbn_decomposition, decompose_strategy in H1.
      rewrite <- H in H1.
      inversion H1.
  + intros [H H0].
    app_frames_tac H.
    - destruct H0 as [[]|[_ [_ [H0 _]]]].
      invert_by abstraction_inv H0.
      reflexivity.
    - destruct H0 as [|[_ [[] _]]].
      assert (cbn_decompose (plug x c) = Some (x, c)).
      { apply IHt1.
        split; [reflexivity|].
        apply IHt1, H. }
      simpl. unfold sequence_decompose. simpl.
      rewrite H0.
      reflexivity.
    - destruct H0 as [[]|[_ [[] _]]].
Qed.

Corollary cbn_decomposition_phased_form : cbn_decomposition ==  cbn_decomposition_phased.
Proof. apply decompose_strategy_correct, cbn_decompose_lemma. Qed.

Theorem normal_cbn_decomposition_whnf : normal_form cbn_decomposition == whnf.
Proof.
  intros t. induction t; split.
  + intros _. right. constructor.
  + intros _.
    apply (normal_form_proper _ _ cbn_decomposition_phased_form).
    eapply decompose_complete_None.
    - apply (proj2 (proj2 cbn_decompose_lemma)).
    - reflexivity.
  + intros _. left. constructor.
  + intros _.
    apply (normal_form_proper _ _ cbn_decomposition_phased_form).
    eapply decompose_complete_None.
    - apply (proj2 (proj2 cbn_decompose_lemma)).
    - reflexivity.
  + intros H. right. simpl.
    apply (normal_form_proper _ _ cbn_decomposition_phased_form) in H.
    apply normal_form_sequence_strategy in H.
    destruct H as [H H0].
    apply normal_left_strategy_app, IHt1 in H.
    apply normal_only_β_contraction_app in H0.
    destruct H as [H|H].
    - contradiction (H0 H).
    - exact H.
  + intros [[]|H].
    apply (normal_form_proper _ _ cbn_decomposition_phased_form).
    normal_form_sequence_strategy_tac.
    - apply normal_left_strategy_app, IHt1, or_intror, H.
    - apply normal_only_β_contraction_app.
      intros H0.
      exact (rigid_abstraction_disjoint t1 H H0).
Qed.

Definition cbn_eqn (s : strategy) : Prop := s == ↙ s;; β.

Lemma cbn_unique : ∀ s, cbn_eqn s → s == cbn_decomposition.
Proof.
  intros s cbn_s.
  apply strategies_equal_on_all_terms. intros t. induction t;
  intros [C c] H.
  + var_frames_tac H. split; [|intros H; inversion H].
    intros H. apply cbn_s in H.
    cbv in H; tauto.
  + lam_frames_tac H; (split; [|intros H; inversion H]);
    intros H; apply cbn_s in H; cbv in H; tauto.
  + app_frames_tac H; [|specialize (IHt1 (_,_) eq_refl)|]; split;
    intros H;
      (apply cbn_s; apply cbn_decomposition_phased_form in H)
    + (apply cbn_s in H; apply cbn_decomposition_phased_form).
    - destruct H as [[]|[_ [_ [H _]]]].
      right. split.
      { apply normal_left_strategy_app, normal_cbn_decomposition_whnf, or_introl, H. }
      repeat split. exact H.
    - destruct H as [[]|[H1 H]].
      apply normal_left_strategy_app in H1.
      right. split.
      { apply normal_left_strategy_app, (normal_on_term _ _ _ IHt1), H1. }
      exact H.
    - destruct H as [H|[_ [[] _]]].
      left. apply IHt1, H.
    - destruct H as [H|[_ [[] _]]].
      left. apply IHt1, H.
    - destruct H as [[]|[_ [[] _]]].
    - destruct H as [[]|[_ [[] _]]].
Qed.

Lemma cbn_phased_lemma : phased_lemma cbn cbn_phased whnf.
Proof with cbv; tauto.
  phased_lemma_by_induction cbn.
  + split.
    - intros [C c] H. var_frames_tac H...
    - split; intros _; [|right; constructor].
      apply normal_var...
  + split.
    - intros [C c] H. lam_frames_tac H...
    - split; intros _; [|left; constructor].
      apply normal_lam.
      split; [|intros C c H; subst]...
  + clear IHt2.
    split; [|split; intros H].
    - intros [C c] H.
      app_frames_tac H.
      * split; [|intros [[]|[_ [_ [H0 _]]]]; repeat split; assumption].
        intros [_ [H0 _]].
        right. split; [|repeat split; apply H0].
        apply IHt1.
        left. exact H0.
      * split; [intros [[_ H] H0]; left; exact (conj H H0)|].
        intros [[H H0]|[H [[] _]]].
        repeat split; assumption.
      * idtac...
    - destruct H as [[]|rigid_t1].
      unfold cbn_phased, sequence_strategy.
      normal_form_sequence_strategy_tac.
      * apply IHt1, or_intror, rigid_t1.
      * apply normal_only_β_contraction_app.
        exact (λ abs_t1, rigid_abstraction_disjoint t1 rigid_t1 abs_t1).
    - right.
      apply normal_form_sequence_strategy in H. destruct H as [H H0].
      apply IHt1 in H.
      apply normal_only_β_contraction_app in H0.
      destruct H as [H|H]; [contradiction (H0 H)|exact H].
Qed.

Corollary cbn_phased_form : cbn == cbn_phased.
Proof. exact (phased_lemma_first_corollary _ _ _ cbn_phased_lemma). Qed.

Corollary cbn_phased_form2 : cbn == cbn_phased2.
Proof.
  etransitivity; [apply cbn_phased_form|].
  apply left_weak_strategy_β_contraction_commutative.
  apply cartesian_product_monotone; [apply CBN_is_Weak|reflexivity].
Qed.

Corollary normal_cbn_whnf : normal_form cbn == whnf.
Proof. exact (phased_lemma_second_corollary _ _ _ cbn_phased_lemma). Qed.

Theorem cbn_decompose_form : cbn == cbn_decomposition.
Proof. apply cbn_unique, cbn_phased_form. Qed.

Definition cbn_ind : strategy := ⋂ cbn_eqn.

Theorem cbn_ind_form : cbn == cbn_ind.
Proof.
  etransitivity; [apply cbn_decompose_form|].
  intros d. split; intros H.
  + exact (λ _ H0, proj2 (cbn_unique _ H0 _) H).
  + exact (H _ cbn_decomposition_phased_form).
Qed.

Definition cbn_coind : strategy := ⋃ cbn_eqn.

Theorem cbn_coind_form : cbn == cbn_coind.
Proof.
  etransitivity; [apply cbn_decompose_form|].
  intros d. split.
  + intros H. exists cbn. split; [exact cbn_phased_form|apply cbn_decompose_form, H].
  + intros [cbn0 [H0 H]]. exact (proj1 (cbn_unique _ H0 _) H).
Qed.

(** *** refocusing *)

Definition refocus (decompose: term → option decomposition) :
                      decomposition → option decomposition :=
  compose decompose recompose.

Lemma refocus_monadic_idempotent : ∀ f,
  decompose_decomposes f →
  option_arrow (refocus f) (refocus f) =ext refocus f.
Proof.
  intros f H d.
  unfold option_arrow, option_bind, refocus, compose.
  destruct (f _) eqn:H0; [|reflexivity].
  rewrite <- (H _ _ H0).
  exact H0.
Qed.

Example cbn_refocus : decomposition → option decomposition := refocus cbn_decompose.

(** *** rcbm *)

Fixpoint rcbm_decompose (t : term) : option decomposition :=
  sequence_decompose
    (sequence_decompose
      (right_decompose rrcbm_decompose)
    (sequence_decompose
      (left_decompose rrcbm_decompose)
    (sequence_decompose
      contrex_decompose
      (right_decompose rcbm_decompose))))
    (down_decompose rcbm_decompose)
  t
with rrcbm_decompose (t : term) : option decomposition :=
  sequence_decompose
    (right_decompose rrcbm_decompose)
  (sequence_decompose
    (left_decompose rrcbm_decompose)
  (sequence_decompose
    contrex_decompose
    (right_decompose rcbm_decompose)))
  t.

Lemma rcbm_decompose_unfold : rcbm_decompose =ext
  sequence_decompose
    rrcbm_decompose
    (down_decompose rcbm_decompose).
Proof. intros []; reflexivity. Qed.

Lemma rrcbm_decompose_unfold : rrcbm_decompose =ext
  sequence_decompose
    (right_decompose rrcbm_decompose)
  (sequence_decompose
    (left_decompose rrcbm_decompose)
  (sequence_decompose
    contrex_decompose
    (right_decompose rcbm_decompose))).
Proof. intros []; reflexivity. Qed.

Definition  rcbm_decomposition : strategy := decompose_strategy  rcbm_decompose.
Definition rrcbm_decomposition : strategy := decompose_strategy rrcbm_decompose.

Definition  rcbm_decomposition_phased : strategy :=
  rrcbm_decomposition ∪ ↓ rcbm_decomposition.
Definition rrcbm_decomposition_phased : strategy :=
  ↘ rrcbm_decomposition;; ↙ rrcbm_decomposition;; β;; ↘ rcbm_decomposition.

Lemma rrcbm_decompose_lam : ∀ x t, rrcbm_decompose (lam x t) = None.
Proof. auto. Qed.

Lemma rcbm_rrcbm_decompose_lemma :
    decompose_correct  rcbm_decompose  rcbm_decomposition_phased
  ∧ decompose_correct rrcbm_decompose rrcbm_decomposition_phased.
Proof.
  apply (conj_bifunctor _ _ _ _ (decompose_correct_on_all _ _) (decompose_correct_on_all _ _)),
         conj_forall.
  intros t. induction t as [|? ? [IHt IHtr]|t1 [IHt1 IHt1r] t2 [IHt2 IHt2r]];
  split; intros [C c]; split.
  + intros H. inversion H.
  + intros [H H0]. var_frames_tac H. cbv in H0. tauto.
  + intros H. inversion H.
  + intros [H H0]. var_frames_tac H. cbv in H0. tauto.
  + intros H.
    rewrite rcbm_decompose_unfold in H.
    unfold sequence_decompose in H.
    rewrite rrcbm_decompose_lam in H.
    apply option_map_Some in H.
    destruct H as [[C' c'] [H  H0]].
    apply pair_equal_spec in H0.
    destruct H0. subst.
    destruct (proj1 (IHt _) H). subst.
    split; [reflexivity|].
    right. exact H.
  + intros [H H0].
    lam_frames_tac H.
    - destruct H0 as [H|[]].
      unfold rrcbm_decomposition, decompose_strategy in H.
      rewrite rrcbm_decompose_unfold in H.
      unfold sequence_decompose in H.
      repeat (try apply option_append_Some in H; destruct H as [H|H]);
      simpl in H; inversion H.
    - destruct H0 as [H0|H0].
      { unfold rrcbm_decomposition, decompose_strategy in H0.
        replace (recompose _) with (lam x (plug x0 c)) in H0 by auto.
        rewrite rrcbm_decompose_lam in H0.
        inversion H0. }
      cbn.
      simpl in H0.
      unfold rcbm_decomposition, decompose_strategy in H0.
      simpl in H0.
      rewrite H0.
      reflexivity.
  + intros H. inversion H.
  + intros [H H0].
    lam_frames_tac H.
    - destruct H0 as [[]|[_ [[]|[_ [[_ []]|[_ []]]]]]].
    - destruct H0 as [[]|[_ [[]|[_ [[[] _]|[_ []]]]]]].
  + intros H.
    simpl in H.
    unfold sequence_decompose in H.
    replace (down_decompose _ _) with (@None decomposition) in H by reflexivity.
    rewrite option_append_None_r in H.
    apply (decomposes_into_in_variance _ _ rrcbm_decomposition);
    [exact (λ _ H, or_introl H)|].
    apply option_append_Some2 in H; destruct H as [H|[H0 H]].
    { apply option_map_Some in H.
      destruct H as [[C' c'] [H  H5]].
      apply pair_equal_spec in H5; destruct H5; subst.
      assert (H5 := H). apply IHt2r in H5.
      destruct H5 as [? H5]. subst. simpl in H.
      split; [reflexivity|].
      unfold rrcbm_decomposition, decompose_strategy. simpl. unfold sequence_decompose.
      simpl. rewrite H. reflexivity. }
    apply option_map_None in H0.
    apply option_append_Some2 in H; destruct H as [H|[H1 H]].
    { apply option_map_Some in H.
      destruct H as [[C' c'] [H  H5]].
      apply pair_equal_spec in H5; destruct H5; subst.
      assert (H5 := H). apply IHt1r in H5.
      destruct H5 as [? H5]. subst. simpl in H.
      split; [reflexivity|].
      unfold rrcbm_decomposition, decompose_strategy. simpl. unfold sequence_decompose.
      simpl. rewrite H0, H. reflexivity. }
    apply option_map_None in H1.
    apply option_append_Some2 in H; destruct H as [H|[H2 H]].
    { apply contrex_decompose_app_Some_inv in H.
      destruct H as [H []]. subst.
      invert_by abstraction_inv H.
      split; [reflexivity|].
      unfold rrcbm_decomposition, decompose_strategy. simpl. unfold sequence_decompose.
      simpl. rewrite H0. reflexivity. }
    { apply option_map_Some in H.
      destruct H as [[C' c'] [H  H5]].
      apply pair_equal_spec in H5; destruct H5; subst.
      assert (H5 := H). apply IHt2 in H5.
      destruct H5 as [? H5]. subst. simpl in H.
      split; [reflexivity|].
      unfold rrcbm_decomposition, decompose_strategy. simpl. unfold sequence_decompose.
      simpl. simpl in H0, H2. rewrite H0, H1, H2, H. reflexivity. }
  + intros [H H0].
    app_frames_tac H; destruct H0 as [H0|[]];
    unfold rrcbm_decomposition, decompose_strategy in H0.
    - replace (recompose _) with (app t1 t2) in H0 by reflexivity.
      rewrite rcbm_decompose_unfold.
      unfold sequence_decompose.
      rewrite H0. reflexivity.
    - replace (recompose _) with (app (plug x c) t2) in H0 by reflexivity.
      rewrite rcbm_decompose_unfold.
      unfold sequence_decompose.
      rewrite H0. reflexivity.
    - replace (recompose _) with (app t1 (plug x c)) in H0 by reflexivity.
      rewrite rcbm_decompose_unfold.
      unfold sequence_decompose.
      rewrite H0. reflexivity.
  + intros H.
    simpl in H.
    apply option_append_Some2 in H; destruct H as [H|[H0 H]].
    { apply option_map_Some in H.
      destruct H as [[C' c'] [H  H5]].
      apply pair_equal_spec in H5; destruct H5; subst.
      assert (H5 := H). apply IHt2r in H5.
      destruct H5 as [? H5]. subst. simpl in H.
      split; [reflexivity|].
      unfold rrcbm_decomposition, decompose_strategy. simpl. unfold sequence_decompose.
      left. simpl. apply H. }
    apply option_map_None, (decompose_correct_on_None _ _ _ IHt2r) in H0.
    apply option_append_Some2 in H; destruct H as [H|[H1 H]].
    { apply option_map_Some in H.
      destruct H as [[C' c'] [H  H5]].
      apply pair_equal_spec in H5; destruct H5; subst.
      assert (H5 := H). apply IHt1r in H5.
      destruct H5 as [? H5]. subst. simpl in H.
      split; [reflexivity|].
      right. split.
      { apply normal_right_strategy_app.
        intros [d [? H6]]. subst.
        apply IHt2r in H6.
        exact (H0 (ex_intro _ d H6)). }
      left. simpl. apply H. }
    apply option_map_None, (decompose_correct_on_None _ _ _ IHt1r) in H1.
    apply option_append_Some2 in H; destruct H as [H|[H2 H]].
    { apply contrex_decompose_app_Some_inv in H.
      destruct H as [H []]. subst.
      invert_by abstraction_inv H.
      split; [reflexivity|].
      right. split.
      { apply normal_right_strategy_app.
        intros [d [? H6]]. subst.
        apply IHt2r in H6.
        exact (H0 (ex_intro _ d H6)). }
      right. split.
      { apply normal_left_strategy_app.
        intros [d [H5 H6]]. rewrite H5 in IHt1r.
        apply IHt1r in H6.
        apply H1. exists d. rewrite H5. exact H6. }
      left. repeat constructor. }
    { apply option_map_Some in H.
      destruct H as [[C' c'] [H  H5]].
      apply pair_equal_spec in H5; destruct H5; subst.
      assert (H5 := H). apply IHt2 in H5.
      destruct H5 as [? H5]. subst. simpl in H.
      split; [reflexivity|].
      right. split.
      { simpl. apply normal_right_strategy_app.
        intros [d [H7 H6]]. subst. simpl in IHt2r. rewrite H7 in IHt2r.
        apply IHt2r in H6.
        simpl in H0. rewrite H7 in H0.
        exact (H0 (ex_intro _ d H6)). }
      right. split.
      { apply normal_left_strategy_app.
        intros [d [H7 H6]]. rewrite H7 in IHt1r.
        apply IHt1r in H6.
        apply H1. exists d. rewrite H7. exact H6. }
      right. split.
      { eapply decompose_complete_None in H2;
        [exact H2|apply only_β_contraction_correct]. }
      { exact H. } }
  + intros [H H0].
    app_frames_tac H.
    - destruct H0 as [[]|[H1 [[]|[H2 [[_ [H0 _]]|[_ []]]]]]].
      * apply normal_right_strategy_app, (normal_form_decompose_strategy _ _ _ IHt2r) in H1.
        apply  normal_left_strategy_app, (normal_form_decompose_strategy _ _ _ IHt1r) in H2.
        simpl. unfold sequence_decompose. simpl.
        rewrite H1, H2.
        destruct (is_abs t1) as [_|H0']; [|contradiction (H0' H0)].
        reflexivity.
    - destruct H0 as [[]|[H1 [H0|[_ [[[]]|[_ []]]]]]].
      * apply normal_right_strategy_app, (normal_form_decompose_strategy _ _ _ IHt2r) in H1.
        simpl in H0. unfold rrcbm_decomposition, decompose_strategy in H0. simpl in H0.
        simpl. unfold sequence_decompose. simpl.
        rewrite H1, H0. reflexivity.
    - destruct H0 as [H0|[H1 [[]|[H2 [[[]]|[H3 H0]]]]]].
      * simpl. unfold sequence_decompose. simpl.
        simpl in H0. unfold rrcbm_decomposition, decompose_strategy in H0. simpl in H0.
        rewrite H0. reflexivity.
      * simpl. unfold sequence_decompose. simpl.
        simpl in H0. unfold rcbm_decomposition, decompose_strategy in H0. simpl in H0.
        apply normal_right_strategy_app, (normal_form_decompose_strategy _ _ _ IHt2r) in H1.
        apply  normal_left_strategy_app, (normal_form_decompose_strategy _ _ _ IHt1r) in H2.
        apply (decompose_sound_normal_form _ _ _
          (proj1 only_β_contraction_correct) (proj1 (proj2 only_β_contraction_correct))) in H3.
        simpl in H3.
        rewrite H1, H2, H3, H0. reflexivity.
Qed.

Corollary  rcbm_decomposition_phased_form :  rcbm_decomposition ==  rcbm_decomposition_phased.
Proof. apply decompose_strategy_correct, rcbm_rrcbm_decompose_lemma. Qed.
Corollary rrcbm_decomposition_phased_form : rrcbm_decomposition == rrcbm_decomposition_phased.
Proof. apply decompose_strategy_correct, rcbm_rrcbm_decompose_lemma. Qed.

Theorem normal_rcbm_rrcbm_decomposition :
    normal_form  rcbm_decomposition ==  nf
  ∧ normal_form rrcbm_decomposition == mnf.
Proof.
  apply conj_forall.
  intros t.
  induction t as [|x t [IHt IHtr]|t1 [IHt1 IHt1r] t2 [IHt2 IHt2r]]; split; split.
  + intros _. constructor.
  + intros _. apply normal_var. intros H. inversion H.
  + intros _. constructor.
  + intros _. apply normal_var. intros H. inversion H.
  + intros H.
    apply IHt. clear IHt.
    intros [[C c] [H0 H1]].
    apply normal_lam, proj2 in H.
    apply (H C c H0).
    unfold rcbm_decomposition, decompose_strategy in H1. simpl in H1.
    unfold rcbm_decomposition, decompose_strategy. simpl.
    unfold sequence_decompose. simpl. rewrite H1. reflexivity.
  + intros H.
    apply IHt in H.
    apply (normal_form_proper _ _ rcbm_decomposition_phased_form).
    intros [[C c] [H0 [H1|H1]]].
    { apply rrcbm_decomposition_phased_form in H1.
      lam_frames_tac H0.
      + destruct H1 as [[]|[_ [[]|[_ [[_ []]|[_ []]]]]]].
      + destruct H1 as [[]|[_ [[]|[_ [[[] _]|[_ []]]]]]]. }
    lam_frames_tac H0; [contradiction H1|].
    apply H.
    exists (x0, c).
    split; [reflexivity|exact H1].
  + intros _. constructor.
  + intros _.
    apply (normal_form_proper _ _ rrcbm_decomposition_phased_form).
    repeat (try (apply normal_form_sequence_strategy; split)).
    - apply normal_right_strategy_lam.
    - apply normal_left_strategy_lam.
    - apply normal_only_β_contraction_lam.
    - apply normal_right_strategy_lam.
  + intros H.
    apply (normal_form_proper _ _ rcbm_decomposition_phased_form),
          normal_union_strategy, proj1,
          (normal_form_proper _ _ rrcbm_decomposition_phased_form),
          normal_form_sequence_strategy, proj2,
          normal_form_sequence_strategy in H.
    destruct H as [mnf_t1 H].
    apply normal_form_sequence_strategy in H.
    destruct H as [nabs_t1 nf_t2].
    apply  normal_left_strategy_app, IHt1r in mnf_t1.
    apply normal_only_β_contraction_app in nabs_t1.
    apply normal_right_strategy_app, IHt2 in nf_t2.
    apply mnf_grammar in mnf_t1.
    destruct mnf_t1 as [abs_t1|neu_t1].
    { contradiction (nabs_t1 abs_t1). }
    exact (conj neu_t1 nf_t2).
  + intros [neu_t1 nf_t2].
    apply (normal_form_proper _ _ rcbm_decomposition_phased_form),
          normal_union_strategy.
    split; [|apply normal_down_strategy_app].
    apply (normal_form_proper _ _ rrcbm_decomposition_phased_form).
    repeat (try (apply normal_form_sequence_strategy; split)).
    - apply normal_right_strategy_app, IHt2r, nf_is_mnf, nf_t2.
    - apply  normal_left_strategy_app, IHt1r, mnf_grammar, or_intror, neu_t1.
    - apply normal_only_β_contraction_app.
      apply neu_is_inert, inert_is_rigid in neu_t1.
      exact (rigid_abstraction_disjoint _ neu_t1).
    - apply normal_right_strategy_app, IHt2, nf_t2.
  + intros H.
    apply (normal_form_proper _ _ rrcbm_decomposition_phased_form),
          normal_form_sequence_strategy, proj2,
          normal_form_sequence_strategy in H.
    destruct H as [mnf_t1 H].
    apply normal_form_sequence_strategy in H.
    destruct H as [nabs_t1 nf_t2].
    apply  normal_left_strategy_app, IHt1r in mnf_t1.
    apply normal_only_β_contraction_app in nabs_t1.
    apply normal_right_strategy_app, IHt2 in nf_t2.
    apply mnf_grammar in mnf_t1.
    destruct mnf_t1 as [abs_t1|neu_t1].
    { contradiction (nabs_t1 abs_t1). }
    exact (conj neu_t1 nf_t2).
  + intros [neu_t1 nf_t2].
    apply (normal_form_proper _ _ rrcbm_decomposition_phased_form).
    repeat (try (apply normal_form_sequence_strategy; split)).
    - apply normal_right_strategy_app, IHt2r, nf_is_mnf, nf_t2.
    - apply  normal_left_strategy_app, IHt1r, mnf_grammar, or_intror, neu_t1.
    - apply normal_only_β_contraction_app.
      apply neu_is_inert, inert_is_rigid in neu_t1.
      exact (rigid_abstraction_disjoint _ neu_t1).
    - apply normal_right_strategy_app, IHt2, nf_t2.
Qed.

Definition r_cbm_rr_cbm_eqn (s rs: strategy) : Prop :=
     s == rs ∪ ↓ s
  ∧ rs == ↘ rs;; ↙ rs;; β;; ↘ s.

Lemma r_cbm_rr_cbm_unique : ∀ s rs,
  r_cbm_rr_cbm_eqn s rs →
  s == rcbm_decomposition ∧ rs == rrcbm_decomposition.
Proof.
  intros s rs [sH rsH].
  eapply conj_bifunctor; try apply strategies_equal_on_all_terms.
  apply conj_forall. intros t.
  induction t as [|x t [IHt IHtr]|t1 [IHt1 IHt1r] t2 [IHt2 IHt2r]]; split;
  intros [C c] H.
  + var_frames_tac H. split; intros H; [|inversion H].
    apply  sH in H. destruct H as [|[]].
    apply rsH in H. destruct H as [[]|[_ [[]|[_ [[_ []]|[_ []]]]]]].
  + var_frames_tac H. split; intros H; [|inversion H].
    apply rsH in H. destruct H as [[]|[_ [[]|[_ [[_ []]|[_ []]]]]]].
  + lam_frames_tac H; split; intros H.
    - apply  sH in H. destruct H as [|[]].
      apply rsH in H. destruct H as [[]|[_ [[]|[_ [[_ []]|[_ []]]]]]].
    - apply rcbm_rrcbm_decompose_lemma in H.
      destruct H as [|[]].
      apply rcbm_rrcbm_decompose_lemma in H.
      destruct H as [[]|[_ [[]|[_ [[_ []]|[_ []]]]]]].
    - apply  sH in H. destruct H as [|].
      * apply rsH in H. destruct H as [[]|[_ [[]|[_ [[[] _]|[_ []]]]]]].
      * apply rcbm_rrcbm_decompose_lemma, or_intror, (IHt (x0, c) eq_refl), H.
    - apply rcbm_rrcbm_decompose_lemma in H.
      destruct H as [|].
      * apply rcbm_rrcbm_decompose_lemma in H.
        destruct H as [[]|[_ [[]|[_ [[[] _]|[_ []]]]]]].
      * apply sH, or_intror, (IHt (x0, c) eq_refl), H.
  + lam_frames_tac H; split; intros H.
    - apply rsH in H. destruct H as [[]|[_ [[]|[_ [[_ []]|[_ []]]]]]].
    - apply rcbm_rrcbm_decompose_lemma in H.
      destruct H as [[]|[_ [[]|[_ [[_ []]|[_ []]]]]]].
    - apply rsH in H. destruct H as [[]|[_ [[]|[_ [[[] _]|[_ []]]]]]].
    - apply rcbm_rrcbm_decompose_lemma in H.
      destruct H as [[]|[_ [[]|[_ [[[] _]|[_ []]]]]]].
  + app_frames_tac H; split; intros H;
      (apply sH; apply rcbm_rrcbm_decompose_lemma in H)
    + (apply sH in H; apply rcbm_rrcbm_decompose_lemma).
    - destruct H as [|[]]. left.
      * apply rsH in H. apply rcbm_rrcbm_decompose_lemma.
        destruct H as [[]|[H1 [[]|[H2 [H|[_ []]]]]]].
        apply normal_right_strategy_app, (normal_on_term _ _ _ IHt2r) in H1.
        apply  normal_left_strategy_app, (normal_on_term _ _ _ IHt1r) in H2.
        right. split.
        { apply normal_right_strategy_app, H1. }
        right. split.
        { apply  normal_left_strategy_app, H2. }
        left. exact H.
    - destruct H as [|[]]. left.
      * apply rsH. apply rcbm_rrcbm_decompose_lemma in H.
        destruct H as [[]|[H1 [[]|[H2 [H|[_ []]]]]]].
        apply normal_right_strategy_app, (normal_on_term _ _ _ IHt2r) in H1.
        apply  normal_left_strategy_app, (normal_on_term _ _ _ IHt1r) in H2.
        right. split.
        { apply normal_right_strategy_app, H1. }
        right. split.
        { apply  normal_left_strategy_app, H2. }
        left. exact H.
    - destruct H as [|[]]. left.
      * apply rsH in H. apply rcbm_rrcbm_decompose_lemma.
        destruct H as [[]|[H1 [H|[_ [[[] _]|[_ []]]]]]].
        apply normal_right_strategy_app, (normal_on_term _ _ _ IHt2r) in H1.
        right. split.
        { apply normal_right_strategy_app, H1. }
        left. apply IHt1r; [reflexivity|]. exact H.
    - destruct H as [|[]]. left.
      * apply rsH. apply rcbm_rrcbm_decompose_lemma in H.
        destruct H as [[]|[H1 [H|[_ [[[] _]|[_ []]]]]]].
        apply normal_right_strategy_app, (normal_on_term _ _ _ IHt2r) in H1.
        right. split.
        { apply normal_right_strategy_app, H1. }
        left. apply IHt1r; [reflexivity|]. exact H.
    - destruct H as [|[]]. left.
      apply rsH in H. apply rcbm_rrcbm_decompose_lemma.
      destruct H as [H|[H1 [[]|[H2 [[[] _]|[H3 H]]]]]].
      * left. apply IHt2r; [reflexivity|]. exact H.
      * apply normal_right_strategy_app, (normal_on_term _ _ _ IHt2r) in H1.
        apply  normal_left_strategy_app, (normal_on_term _ _ _ IHt1r) in H2.
        right. split.
        { apply normal_right_strategy_app, H1. }
        right. split.
        { apply  normal_left_strategy_app, H2. }
        right. split.
        { exact H3. }
        apply IHt2; [reflexivity|]. exact H.
    - destruct H as [|[]]. left.
      apply rsH. apply rcbm_rrcbm_decompose_lemma in H.
      destruct H as [H|[H1 [[]|[H2 [[[] _]|[H3 H]]]]]].
      * left. apply IHt2r; [reflexivity|]. exact H.
      * apply normal_right_strategy_app, (normal_on_term _ _ _ IHt2r) in H1.
        apply  normal_left_strategy_app, (normal_on_term _ _ _ IHt1r) in H2.
        right. split.
        { apply normal_right_strategy_app, H1. }
        right. split.
        { apply  normal_left_strategy_app, H2. }
        right. split.
        { exact H3. }
        apply IHt2; [reflexivity|]. exact H.
  + app_frames_tac H; split; intros H;
      (apply rsH; apply rcbm_rrcbm_decompose_lemma in H)
    + (apply rsH in H; apply rcbm_rrcbm_decompose_lemma).
    - destruct H as [[]|[H1 [[]|[H2 [H|[_ []]]]]]].
      apply normal_right_strategy_app, (normal_on_term _ _ _ IHt2r) in H1.
      apply  normal_left_strategy_app, (normal_on_term _ _ _ IHt1r) in H2.
      right. split.
      { apply normal_right_strategy_app, H1. }
      right. split.
      { apply  normal_left_strategy_app, H2. }
      left. exact H.
    - destruct H as [[]|[H1 [[]|[H2 [H|[_ []]]]]]].
      apply normal_right_strategy_app, (normal_on_term _ _ _ IHt2r) in H1.
      apply  normal_left_strategy_app, (normal_on_term _ _ _ IHt1r) in H2.
      right. split.
      { apply normal_right_strategy_app, H1. }
      right. split.
      { apply  normal_left_strategy_app, H2. }
      left. exact H.
    - destruct H as [[]|[H1 [H|[_ [[[] _]|[_ []]]]]]].
      apply normal_right_strategy_app, (normal_on_term _ _ _ IHt2r) in H1.
      right. split.
      { apply normal_right_strategy_app, H1. }
      left. apply IHt1r; [reflexivity|]. exact H.
    - destruct H as [[]|[H1 [H|[_ [[[] _]|[_ []]]]]]].
      apply normal_right_strategy_app, (normal_on_term _ _ _ IHt2r) in H1.
      right. split.
      { apply normal_right_strategy_app, H1. }
      left. apply IHt1r; [reflexivity|]. exact H.
    - destruct H as [H|[H1 [[]|[H2 [[[] _]|[H3 H]]]]]].
      * left. apply IHt2r; [reflexivity|]. exact H.
      * apply normal_right_strategy_app, (normal_on_term _ _ _ IHt2r) in H1.
        apply  normal_left_strategy_app, (normal_on_term _ _ _ IHt1r) in H2.
        right. split.
        { apply normal_right_strategy_app, H1. }
        right. split.
        { apply  normal_left_strategy_app, H2. }
        right. split.
        { exact H3. }
        apply IHt2; [reflexivity|]. exact H.
    - destruct H as [H|[H1 [[]|[H2 [[[] _]|[H3 H]]]]]].
      * left. apply IHt2r; [reflexivity|]. exact H.
      * apply normal_right_strategy_app, (normal_on_term _ _ _ IHt2r) in H1.
        apply  normal_left_strategy_app, (normal_on_term _ _ _ IHt1r) in H2.
        right. split.
        { apply normal_right_strategy_app, H1. }
        right. split.
        { apply  normal_left_strategy_app, H2. }
        right. split.
        { exact H3. }
        apply IHt2; [reflexivity|]. exact H.
Qed.

Lemma r_cbm_rr_cbm_decomposition_form :
  r_cbm == rcbm_decomposition ∧ rr_cbm == rrcbm_decomposition.
Proof.
  eapply conj_bifunctor; try apply strategies_equal_on_all_terms.
  apply conj_forall.
  intros t. induction t as [|x t [IHt IHtr]|t1 [IHt1 IHt1r] t2 [IHt2 IHt2r]]; split;
  intros [C c] H; split; intros H0.
  + var_frames_tac H. destruct H0 as [_ []].
  + var_frames_tac H. inversion H0.
  + var_frames_tac H. destruct H0 as [_ []].
  + var_frames_tac H. inversion H0.
  + lam_frames_tac H.
    - destruct H0 as [_ []].
    - apply rcbm_rrcbm_decompose_lemma, or_intror, IHt; [reflexivity|].
      exact H0.
  + lam_frames_tac H.
    - apply rcbm_rrcbm_decompose_lemma in H0.
      destruct H0 as [H0|[]].
      apply rcbm_rrcbm_decompose_lemma in H0.
      destruct H0 as [[]|[_ [[]|[_ [[_ []]|[_ []]]]]]].
    - apply rcbm_rrcbm_decompose_lemma in H0.
      destruct H0 as [H0|H0].
      * apply rcbm_rrcbm_decompose_lemma in H0.
        destruct H0 as [[]|[_ [[]|[_ [[[] _]|[_ []]]]]]].
      * apply IHt in H0; [|reflexivity]. apply H0.
  + lam_frames_tac H.
    - destruct H0 as [_ []].
    - destruct H0 as [[] _].
  + lam_frames_tac H.
    - apply rcbm_rrcbm_decompose_lemma in H0.
      destruct H0 as [[]|[_ [[]|[_ [[_ []]|[_ []]]]]]].
    - apply rcbm_rrcbm_decompose_lemma in H0.
      destruct H0 as [[]|[_ [[]|[_ [[[] _]|[_ []]]]]]].
  + app_frames_tac H;
    apply rcbm_rrcbm_decompose_lemma, or_introl, rcbm_rrcbm_decompose_lemma.
    - destruct H0 as [_ [abs_t1 mnf_t2]].
      right. split.
      { apply normal_right_strategy_app, normal_rcbm_rrcbm_decomposition, mnf_t2. }
      right. split.
      { apply normal_left_strategy_app, normal_rcbm_rrcbm_decomposition.
        apply mnf_grammar, or_introl, abs_t1. }
      left. repeat constructor. exact abs_t1.
    - destruct H0 as [[HC mnf_t2] Hc].
      right. split.
      { apply normal_right_strategy_app, normal_rcbm_rrcbm_decomposition, mnf_t2. }
      left. apply IHt1r; [reflexivity|].
      exact (conj HC Hc).
    - destruct H0 as [[HC|[neu_t1 HC]] Hc].
      * left. apply IHt2r; [reflexivity|]. exact (conj HC Hc).
      * destruct (is_mnf (plug x c)) as [mnf_t2|nmnf_t2].
        { right. split.
          { apply normal_right_strategy_app, normal_rcbm_rrcbm_decomposition, mnf_t2. }
          right. split.
          { apply normal_left_strategy_app, normal_rcbm_rrcbm_decomposition,
                  mnf_grammar, or_intror, neu_t1. }
          right. split.
          { apply normal_only_β_contraction_app.
            apply neu_is_inert, inert_is_rigid, rigid_abstraction_disjoint in neu_t1.
            exact neu_t1. }
          apply IHt2; [reflexivity|].
          exact (conj HC Hc). }
        left. apply IHt2r; [reflexivity|].
        exact (conj (R_CBM_on_not_mnf _ _ nmnf_t2 HC) Hc).
  + app_frames_tac H;
    apply rcbm_rrcbm_decompose_lemma in H0; destruct H0 as [H0|[]];
    apply rcbm_rrcbm_decompose_lemma in H0.
    - destruct H0 as [[]|[mnf_t2 [[]|[_ [[_ [abs_t1 _]]|[_ []]]]]]].
      apply normal_right_strategy_app, normal_rcbm_rrcbm_decomposition in mnf_t2.
      exact (conj I (conj abs_t1 mnf_t2)).
    - destruct H0 as [[]|[mnf_t2 [H0|[_ [[[] _]|[_ []]]]]]].
      apply normal_right_strategy_app, normal_rcbm_rrcbm_decomposition in mnf_t2.
      simpl in H0. apply IHt1r in H0; [|reflexivity].
      destruct H0 as [HC Hc].
      simpl. auto.
    - destruct H0 as [H0|[mnf_t2 [[]|[mnf_t1 [[[] _]|[nabs_t1 H0]]]]]].
      * apply IHt2r in H0; [|reflexivity].
        destruct H0 as [HC Hc].
        exact (conj (or_introl HC) Hc).
      * simpl in mnf_t2.
        apply normal_right_strategy_app, normal_rcbm_rrcbm_decomposition in mnf_t2.
        apply normal_left_strategy_app, normal_rcbm_rrcbm_decomposition, mnf_grammar in mnf_t1.
        apply normal_only_β_contraction_app in nabs_t1.
        destruct mnf_t1 as [abs_t1|neu_t1]; [contradiction (nabs_t1 abs_t1)|].
        apply IHt2 in H0; [|reflexivity].
        destruct H0 as [HC Hc].
        exact (conj (or_intror (conj neu_t1 HC)) Hc).
  + app_frames_tac H;
    apply rcbm_rrcbm_decompose_lemma.
    - destruct H0 as [_ [abs_t1 mnf_t2]].
      right. split.
      { apply normal_right_strategy_app, normal_rcbm_rrcbm_decomposition, mnf_t2. }
      right. split.
      { apply normal_left_strategy_app, normal_rcbm_rrcbm_decomposition.
        apply mnf_grammar, or_introl, abs_t1. }
      left. repeat constructor. exact abs_t1.
    - destruct H0 as [[HC mnf_t2] Hc].
      right. split.
      { apply normal_right_strategy_app, normal_rcbm_rrcbm_decomposition, mnf_t2. }
      left. apply IHt1r; [reflexivity|].
      exact (conj HC Hc).
    - destruct H0 as [[HC|[neu_t1 HC]] Hc].
      * left. apply IHt2r; [reflexivity|]. exact (conj HC Hc).
      * destruct (is_mnf (plug x c)) as [mnf_t2|nmnf_t2].
        { right. split.
          { apply normal_right_strategy_app, normal_rcbm_rrcbm_decomposition, mnf_t2. }
          right. split.
          { apply normal_left_strategy_app, normal_rcbm_rrcbm_decomposition,
                  mnf_grammar, or_intror, neu_t1. }
          right. split.
          { apply normal_only_β_contraction_app.
            apply neu_is_inert, inert_is_rigid, rigid_abstraction_disjoint in neu_t1.
            exact neu_t1. }
          apply IHt2; [reflexivity|].
          exact (conj HC Hc). }
        left. apply IHt2r; [reflexivity|].
        exact (conj (R_CBM_on_not_mnf _ _ nmnf_t2 HC) Hc).
  + app_frames_tac H;
    apply rcbm_rrcbm_decompose_lemma in H0.
    - destruct H0 as [[]|[mnf_t2 [[]|[_ [[_ [abs_t1 _]]|[_ []]]]]]].
      apply normal_right_strategy_app, normal_rcbm_rrcbm_decomposition in mnf_t2.
      exact (conj I (conj abs_t1 mnf_t2)).
    - destruct H0 as [[]|[mnf_t2 [H0|[_ [[[] _]|[_ []]]]]]].
      apply normal_right_strategy_app, normal_rcbm_rrcbm_decomposition in mnf_t2.
      simpl in H0. apply IHt1r in H0; [|reflexivity].
      destruct H0 as [HC Hc].
      simpl. auto.
    - destruct H0 as [H0|[mnf_t2 [[]|[mnf_t1 [[[] _]|[nabs_t1 H0]]]]]].
      * apply IHt2r in H0; [|reflexivity].
        destruct H0 as [HC Hc].
        exact (conj (or_introl HC) Hc).
      * simpl in mnf_t2.
        apply normal_right_strategy_app, normal_rcbm_rrcbm_decomposition in mnf_t2.
        apply normal_left_strategy_app, normal_rcbm_rrcbm_decomposition, mnf_grammar in mnf_t1.
        apply normal_only_β_contraction_app in nabs_t1.
        destruct mnf_t1 as [abs_t1|neu_t1]; [contradiction (nabs_t1 abs_t1)|].
        apply IHt2 in H0; [|reflexivity].
        destruct H0 as [HC Hc].
        exact (conj (or_intror (conj neu_t1 HC)) Hc).
Qed.

Corollary normal_r_cbm_nf : normal_form r_cbm == nf.
Proof.
  etransitivity.
  + exact (normal_form_proper _ _ (proj1 r_cbm_rr_cbm_decomposition_form)).
  + apply normal_rcbm_rrcbm_decomposition.
Qed.

Corollary r_cbm_phased_form : r_cbm == r_cbm_phased.
Proof.
  etransitivity; [apply r_cbm_rr_cbm_decomposition_form|].
  etransitivity; [apply rcbm_decomposition_phased_form|].
  apply union_proper; symmetry.
  + apply r_cbm_rr_cbm_decomposition_form.
  + apply down_strategy_proper.
    apply r_cbm_rr_cbm_decomposition_form.
Qed.

Corollary normal_rr_cbm_mnf : normal_form rr_cbm == mnf.
Proof.
  etransitivity.
  + exact (normal_form_proper _ _ (proj2 r_cbm_rr_cbm_decomposition_form)).
  + apply normal_rcbm_rrcbm_decomposition.
Qed.

Corollary rr_cbm_phased_form : rr_cbm == rr_cbm_phased.
Proof.
  etransitivity; [apply r_cbm_rr_cbm_decomposition_form|].
  etransitivity; [apply rrcbm_decomposition_phased_form|].
  symmetry.
  apply sequence_strategy_proper.
  { apply right_strategy_proper, r_cbm_rr_cbm_decomposition_form. }
  apply sequence_strategy_proper.
  { apply left_strategy_proper, r_cbm_rr_cbm_decomposition_form. }
  apply sequence_strategy_proper.
  { reflexivity. }
  { apply right_strategy_proper, r_cbm_rr_cbm_decomposition_form. }
Qed.

(** *** lcbm *)

Fixpoint lcbm_decompose (t : term) : option decomposition :=
  sequence_decompose
    (sequence_decompose
      (left_decompose rlcbm_decompose)
    (sequence_decompose
      (right_decompose rlcbm_decompose)
    (sequence_decompose
      contrex_decompose
      (right_decompose lcbm_decompose))))
    (down_decompose lcbm_decompose)
  t
with rlcbm_decompose (t : term) : option decomposition :=
  sequence_decompose
    (left_decompose rlcbm_decompose)
  (sequence_decompose
    (right_decompose rlcbm_decompose)
  (sequence_decompose
    contrex_decompose
    (right_decompose lcbm_decompose)))
  t.

Lemma lcbm_decompose_unfold : lcbm_decompose =ext
  sequence_decompose
    rlcbm_decompose
    (down_decompose lcbm_decompose).
Proof. intros []; reflexivity. Qed.

Lemma rlcbm_decompose_unfold : rlcbm_decompose =ext
  sequence_decompose
    (left_decompose rlcbm_decompose)
  (sequence_decompose
    (right_decompose rlcbm_decompose)
  (sequence_decompose
    contrex_decompose
    (right_decompose lcbm_decompose))).
Proof. intros []; reflexivity. Qed.

Definition  lcbm_decomposition : strategy := decompose_strategy  lcbm_decompose.
Definition rlcbm_decomposition : strategy := decompose_strategy rlcbm_decompose.

Definition  lcbm_decomposition_phased : strategy :=
  rlcbm_decomposition ∪ ↓ lcbm_decomposition.
Definition rlcbm_decomposition_phased : strategy :=
  ↙ rlcbm_decomposition;; ↘ rlcbm_decomposition;; β;; ↘ lcbm_decomposition.

Lemma rlcbm_decompose_lam : ∀ x t, rlcbm_decompose (lam x t) = None.
Proof. auto. Qed.

Lemma lcbm_rlcbm_decompose_lemma :
    decompose_correct  lcbm_decompose  lcbm_decomposition_phased
  ∧ decompose_correct rlcbm_decompose rlcbm_decomposition_phased.
Proof.
  apply (conj_bifunctor _ _ _ _ (decompose_correct_on_all _ _) (decompose_correct_on_all _ _)),
         conj_forall.
  intros t. induction t as [|? ? [IHt IHtr]|t1 [IHt1 IHt1r] t2 [IHt2 IHt2r]];
  split; intros [C c]; split.
  + intros H. inversion H.
  + intros [H H0]. var_frames_tac H. cbv in H0. tauto.
  + intros H. inversion H.
  + intros [H H0]. var_frames_tac H. cbv in H0. tauto.
  + intros H.
    rewrite lcbm_decompose_unfold in H.
    unfold sequence_decompose in H.
    rewrite rlcbm_decompose_lam in H.
    apply option_map_Some in H.
    destruct H as [[C' c'] [H  H0]].
    apply pair_equal_spec in H0.
    destruct H0. subst.
    destruct (proj1 (IHt _) H). subst.
    split; [reflexivity|].
    right. exact H.
  + intros [H H0].
    lam_frames_tac H.
    - destruct H0 as [H|[]].
      unfold rlcbm_decomposition, decompose_strategy in H.
      rewrite rlcbm_decompose_unfold in H.
      unfold sequence_decompose in H.
      repeat (try apply option_append_Some in H; destruct H as [H|H]);
      simpl in H; inversion H.
    - destruct H0 as [H0|H0].
      { unfold rlcbm_decomposition, decompose_strategy in H0.
        replace (recompose _) with (lam x (plug x0 c)) in H0 by auto.
        rewrite rlcbm_decompose_lam in H0.
        inversion H0. }
      cbn.
      simpl in H0.
      unfold rcbm_decomposition, decompose_strategy in H0.
      simpl in H0.
      rewrite H0.
      reflexivity.
  + intros H. inversion H.
  + intros [H H0].
    lam_frames_tac H.
    - destruct H0 as [[]|[_ [[]|[_ [[_ []]|[_ []]]]]]].
    - destruct H0 as [[]|[_ [[]|[_ [[[] _]|[_ []]]]]]].
  + intros H.
    simpl in H.
    unfold sequence_decompose in H.
    replace (down_decompose _ _) with (@None decomposition) in H by reflexivity.
    rewrite option_append_None_r in H.
    apply (decomposes_into_in_variance _ _ rlcbm_decomposition);
    [exact (λ _ H, or_introl H)|].
    apply option_append_Some2 in H; destruct H as [H|[H0 H]].
    { apply option_map_Some in H.
      destruct H as [[C' c'] [H  H5]].
      apply pair_equal_spec in H5; destruct H5; subst.
      assert (H5 := H). apply IHt1r in H5.
      destruct H5 as [? H5]. subst. simpl in H.
      split; [reflexivity|].
      unfold rlcbm_decomposition, decompose_strategy. simpl. unfold sequence_decompose.
      simpl. rewrite H. reflexivity. }
    apply option_map_None in H0.
    apply option_append_Some2 in H; destruct H as [H|[H1 H]].
    { apply option_map_Some in H.
      destruct H as [[C' c'] [H  H5]].
      apply pair_equal_spec in H5; destruct H5; subst.
      assert (H5 := H). apply IHt2r in H5.
      destruct H5 as [? H5]. subst. simpl in H.
      split; [reflexivity|].
      unfold rlcbm_decomposition, decompose_strategy. simpl. unfold sequence_decompose.
      simpl. rewrite H0, H. reflexivity. }
    apply option_map_None in H1.
    apply option_append_Some2 in H; destruct H as [H|[H2 H]].
    { apply contrex_decompose_app_Some_inv in H.
      destruct H as [H []]. subst.
      invert_by abstraction_inv H.
      split; [reflexivity|].
      unfold rlcbm_decomposition, decompose_strategy. simpl. unfold sequence_decompose.
      simpl. rewrite H1. reflexivity. }
    { apply option_map_Some in H.
      destruct H as [[C' c'] [H  H5]].
      apply pair_equal_spec in H5; destruct H5; subst.
      assert (H5 := H). apply IHt2 in H5.
      destruct H5 as [? H5]. subst. simpl in H.
      split; [reflexivity|].
      unfold rlcbm_decomposition, decompose_strategy. simpl. unfold sequence_decompose.
      simpl. simpl in H0, H1, H2. rewrite H0, H1, H2, H. reflexivity. }
  + intros [H H0].
    app_frames_tac H; destruct H0 as [H0|[]];
    unfold rlcbm_decomposition, decompose_strategy in H0.
    - replace (recompose _) with (app t1 t2) in H0 by reflexivity.
      rewrite lcbm_decompose_unfold.
      unfold sequence_decompose.
      rewrite H0. reflexivity.
    - replace (recompose _) with (app (plug x c) t2) in H0 by reflexivity.
      rewrite lcbm_decompose_unfold.
      unfold sequence_decompose.
      rewrite H0. reflexivity.
    - replace (recompose _) with (app t1 (plug x c)) in H0 by reflexivity.
      rewrite lcbm_decompose_unfold.
      unfold sequence_decompose.
      rewrite H0. reflexivity.
  + intros H.
    simpl in H.
    apply option_append_Some2 in H; destruct H as [H|[H0 H]].
    { apply option_map_Some in H.
      destruct H as [[C' c'] [H  H5]].
      apply pair_equal_spec in H5; destruct H5; subst.
      assert (H5 := H). apply IHt1r in H5.
      destruct H5 as [? H5]. subst. simpl in H.
      split; [reflexivity|].
      unfold rlcbm_decomposition, decompose_strategy. simpl. unfold sequence_decompose.
      left. simpl. apply H. }
    apply option_map_None, (decompose_correct_on_None _ _ _ IHt1r) in H0.
    apply option_append_Some2 in H; destruct H as [H|[H1 H]].
    { apply option_map_Some in H.
      destruct H as [[C' c'] [H  H5]].
      apply pair_equal_spec in H5; destruct H5; subst.
      assert (H5 := H). apply IHt2r in H5.
      destruct H5 as [? H5]. subst. simpl in H.
      split; [reflexivity|].
      right. split.
      { apply normal_left_strategy_app.
        intros [d [? H6]]. subst.
        apply IHt1r in H6.
        exact (H0 (ex_intro _ d H6)). }
      left. simpl. apply H. }
    apply option_map_None, (decompose_correct_on_None _ _ _ IHt2r) in H1.
    apply option_append_Some2 in H; destruct H as [H|[H2 H]].
    { apply contrex_decompose_app_Some_inv in H.
      destruct H as [H []]. subst.
      invert_by abstraction_inv H.
      split; [reflexivity|].
      right. split.
      { apply normal_left_strategy_app.
        intros [d [H5 H6]]. rewrite H5 in IHt1r.
        apply IHt1r in H6.
        apply H0. exists d. rewrite H5. exact H6. }
      right. split.
      { apply normal_right_strategy_app.
        intros [d [H5 H6]]. rewrite H5 in IHt2r.
        apply IHt2r in H6.
        apply H1. exists d. rewrite H5. exact H6. }
      left. repeat constructor. }
    { apply option_map_Some in H.
      destruct H as [[C' c'] [H  H5]].
      apply pair_equal_spec in H5; destruct H5; subst.
      assert (H5 := H). apply IHt2 in H5.
      destruct H5 as [? H5]. subst. simpl in H.
      split; [reflexivity|].
      right. split.
      { simpl. apply normal_left_strategy_app.
        intros [d [H7 H6]]. subst. simpl in IHt2r.
        apply IHt1r in H6.
        simpl in H0.
        exact (H0 (ex_intro _ d H6)). }
      right. split.
      { simpl. apply normal_right_strategy_app.
        intros [d [H7 H6]]. simpl in IHt2r. rewrite H7 in IHt2r.
        apply IHt2r in H6.
        apply H1. exists d. simpl. rewrite H7. exact H6. }
      right. split.
      { eapply decompose_complete_None in H2;
        [exact H2|apply only_β_contraction_correct]. }
      { exact H. } }
  + intros [H H0].
    app_frames_tac H.
    - destruct H0 as [[]|[H1 [[]|[H2 [[_ [H0 _]]|[_ []]]]]]].
      * apply  normal_left_strategy_app, (normal_form_decompose_strategy _ _ _ IHt1r) in H1.
        apply normal_right_strategy_app, (normal_form_decompose_strategy _ _ _ IHt2r) in H2.
        simpl. unfold sequence_decompose. simpl.
        rewrite H1, H2.
        destruct (is_abs t1) as [_|H0']; [|contradiction (H0' H0)].
        reflexivity.
    - destruct H0 as [H0|[_ [[]|[_ [[[]]|[_ []]]]]]].
      * simpl in H0. unfold rlcbm_decomposition, decompose_strategy in H0. simpl in H0.
        simpl. unfold sequence_decompose. simpl.
        rewrite H0. reflexivity.
    - destruct H0 as [[]|[H1 [H0|[H2 [[[]]|[H3 H0]]]]]].
      * apply  normal_left_strategy_app, (normal_form_decompose_strategy _ _ _ IHt1r) in H1.
        simpl. unfold sequence_decompose. simpl.
        simpl in H0. unfold rrcbm_decomposition, decompose_strategy in H0. simpl in H0.
        rewrite H0, H1. reflexivity.
      * simpl. unfold sequence_decompose. simpl.
        simpl in H0. unfold lcbm_decomposition, decompose_strategy in H0. simpl in H0.
        apply  normal_left_strategy_app, (normal_form_decompose_strategy _ _ _ IHt1r) in H1.
        apply normal_right_strategy_app, (normal_form_decompose_strategy _ _ _ IHt2r) in H2.
        apply (decompose_sound_normal_form _ _ _
          (proj1 only_β_contraction_correct) (proj1 (proj2 only_β_contraction_correct))) in H3.
        simpl in H3.
        rewrite H1, H2, H3, H0. reflexivity.
Qed.

Corollary  lcbm_decomposition_phased_form :  lcbm_decomposition ==  lcbm_decomposition_phased.
Proof. apply decompose_strategy_correct, lcbm_rlcbm_decompose_lemma. Qed.
Corollary rlcbm_decomposition_phased_form : rlcbm_decomposition == rlcbm_decomposition_phased.
Proof. apply decompose_strategy_correct, lcbm_rlcbm_decompose_lemma. Qed.

Theorem normal_lcbm_rlcbm_decomposition :
    normal_form  lcbm_decomposition ==  nf
  ∧ normal_form rlcbm_decomposition == mnf.
Proof.
  apply conj_forall.
  intros t.
  induction t as [|x t [IHt IHtr]|t1 [IHt1 IHt1r] t2 [IHt2 IHt2r]]; split; split.
  + intros _. constructor.
  + intros _. apply normal_var. intros H. inversion H.
  + intros _. constructor.
  + intros _. apply normal_var. intros H. inversion H.
  + intros H.
    apply IHt. clear IHt.
    intros [[C c] [H0 H1]].
    apply normal_lam, proj2 in H.
    apply (H C c H0).
    unfold lcbm_decomposition, decompose_strategy in H1. simpl in H1.
    unfold lcbm_decomposition, decompose_strategy. simpl.
    unfold sequence_decompose. simpl. rewrite H1. reflexivity.
  + intros H.
    apply IHt in H.
    apply (normal_form_proper _ _ lcbm_decomposition_phased_form).
    intros [[C c] [H0 [H1|H1]]].
    { apply rlcbm_decomposition_phased_form in H1.
      lam_frames_tac H0.
      + destruct H1 as [[]|[_ [[]|[_ [[_ []]|[_ []]]]]]].
      + destruct H1 as [[]|[_ [[]|[_ [[[] _]|[_ []]]]]]]. }
    lam_frames_tac H0; [contradiction H1|].
    apply H.
    exists (x0, c).
    split; [reflexivity|exact H1].
  + intros _. constructor.
  + intros _.
    apply (normal_form_proper _ _ rlcbm_decomposition_phased_form).
    repeat (try (apply normal_form_sequence_strategy; split)).
    - apply normal_left_strategy_lam.
    - apply normal_right_strategy_lam.
    - apply normal_only_β_contraction_lam.
    - apply normal_right_strategy_lam.
  + intros H.
    apply (normal_form_proper _ _ lcbm_decomposition_phased_form),
          normal_union_strategy, proj1,
          (normal_form_proper _ _ rlcbm_decomposition_phased_form),
          normal_form_sequence_strategy in H.
    destruct H as [mnf_t1 H].
    apply normal_form_sequence_strategy, proj2, normal_form_sequence_strategy in H.
    destruct H as [nabs_t1 nf_t2].
    apply normal_left_strategy_app, IHt1r in mnf_t1.
    apply normal_only_β_contraction_app in nabs_t1.
    apply normal_right_strategy_app, IHt2 in nf_t2.
    apply mnf_grammar in mnf_t1.
    destruct mnf_t1 as [abs_t1|neu_t1].
    { contradiction (nabs_t1 abs_t1). }
    exact (conj neu_t1 nf_t2).
  + intros [neu_t1 nf_t2].
    apply (normal_form_proper _ _ lcbm_decomposition_phased_form),
          normal_union_strategy.
    split; [|apply normal_down_strategy_app].
    apply (normal_form_proper _ _ rlcbm_decomposition_phased_form).
    repeat (try (apply normal_form_sequence_strategy; split)).
    - apply  normal_left_strategy_app, IHt1r, mnf_grammar, or_intror, neu_t1.
    - apply normal_right_strategy_app, IHt2r, nf_is_mnf, nf_t2.
    - apply normal_only_β_contraction_app.
      apply neu_is_inert, inert_is_rigid in neu_t1.
      exact (rigid_abstraction_disjoint _ neu_t1).
    - apply normal_right_strategy_app, IHt2, nf_t2.
  + intros H.
    apply (normal_form_proper _ _ rlcbm_decomposition_phased_form),
          normal_form_sequence_strategy in H.
    destruct H as [mnf_t1 H].
    apply normal_form_sequence_strategy, proj2,
          normal_form_sequence_strategy in H.
    destruct H as [nabs_t1 nf_t2].
    apply  normal_left_strategy_app, IHt1r in mnf_t1.
    apply normal_only_β_contraction_app in nabs_t1.
    apply normal_right_strategy_app, IHt2 in nf_t2.
    apply mnf_grammar in mnf_t1.
    destruct mnf_t1 as [abs_t1|neu_t1].
    { contradiction (nabs_t1 abs_t1). }
    exact (conj neu_t1 nf_t2).
  + intros [neu_t1 nf_t2].
    apply (normal_form_proper _ _ rlcbm_decomposition_phased_form).
    repeat (try (apply normal_form_sequence_strategy; split)).
    - apply  normal_left_strategy_app, IHt1r, mnf_grammar, or_intror, neu_t1.
    - apply normal_right_strategy_app, IHt2r, nf_is_mnf, nf_t2.
    - apply normal_only_β_contraction_app.
      apply neu_is_inert, inert_is_rigid in neu_t1.
      exact (rigid_abstraction_disjoint _ neu_t1).
    - apply normal_right_strategy_app, IHt2, nf_t2.
Qed.

Definition l_cbm_rl_cbm_eqn (s rs: strategy) : Prop :=
     s == rs ∪ ↓ s
  ∧ rs == ↙ rs;; ↘ rs;; β;; ↘ s.

Lemma l_cbm_rl_cbm_unique : ∀ s rs,
  l_cbm_rl_cbm_eqn s rs →
  s == lcbm_decomposition ∧ rs == rlcbm_decomposition.
Proof.
  intros s rs [sH rsH].
  eapply conj_bifunctor; try apply strategies_equal_on_all_terms.
  apply conj_forall. intros t.
  induction t as [|x t [IHt IHtr]|t1 [IHt1 IHt1r] t2 [IHt2 IHt2r]]; split;
  intros [C c] H.
  + var_frames_tac H. split; intros H; [|inversion H].
    apply  sH in H. destruct H as [|[]].
    apply rsH in H. destruct H as [[]|[_ [[]|[_ [[_ []]|[_ []]]]]]].
  + var_frames_tac H. split; intros H; [|inversion H].
    apply rsH in H. destruct H as [[]|[_ [[]|[_ [[_ []]|[_ []]]]]]].
  + lam_frames_tac H; split; intros H.
    - apply  sH in H. destruct H as [|[]].
      apply rsH in H. destruct H as [[]|[_ [[]|[_ [[_ []]|[_ []]]]]]].
    - apply lcbm_rlcbm_decompose_lemma in H.
      destruct H as [|[]].
      apply lcbm_rlcbm_decompose_lemma in H.
      destruct H as [[]|[_ [[]|[_ [[_ []]|[_ []]]]]]].
    - apply  sH in H. destruct H as [|].
      * apply rsH in H. destruct H as [[]|[_ [[]|[_ [[[] _]|[_ []]]]]]].
      * apply lcbm_rlcbm_decompose_lemma, or_intror, (IHt (x0, c) eq_refl), H.
    - apply lcbm_rlcbm_decompose_lemma in H.
      destruct H as [|].
      * apply lcbm_rlcbm_decompose_lemma in H.
        destruct H as [[]|[_ [[]|[_ [[[] _]|[_ []]]]]]].
      * apply sH, or_intror, (IHt (x0, c) eq_refl), H.
  + lam_frames_tac H; split; intros H.
    - apply rsH in H. destruct H as [[]|[_ [[]|[_ [[_ []]|[_ []]]]]]].
    - apply lcbm_rlcbm_decompose_lemma in H.
      destruct H as [[]|[_ [[]|[_ [[_ []]|[_ []]]]]]].
    - apply rsH in H. destruct H as [[]|[_ [[]|[_ [[[] _]|[_ []]]]]]].
    - apply lcbm_rlcbm_decompose_lemma in H.
      destruct H as [[]|[_ [[]|[_ [[[] _]|[_ []]]]]]].
  + app_frames_tac H; split; intros H;
      (apply sH; apply lcbm_rlcbm_decompose_lemma in H)
    + (apply sH in H; apply lcbm_rlcbm_decompose_lemma).
    - destruct H as [|[]]. left.
      * apply rsH in H. apply lcbm_rlcbm_decompose_lemma.
        destruct H as [[]|[H1 [[]|[H2 [H|[_ []]]]]]].
        apply  normal_left_strategy_app, (normal_on_term _ _ _ IHt1r) in H1.
        apply normal_right_strategy_app, (normal_on_term _ _ _ IHt2r) in H2.
        right. split.
        { apply  normal_left_strategy_app, H1. }
        right. split.
        { apply normal_right_strategy_app, H2. }
        left. exact H.
    - destruct H as [|[]]. left.
      * apply rsH. apply lcbm_rlcbm_decompose_lemma in H.
        destruct H as [[]|[H1 [[]|[H2 [H|[_ []]]]]]].
        apply  normal_left_strategy_app, (normal_on_term _ _ _ IHt1r) in H1.
        apply normal_right_strategy_app, (normal_on_term _ _ _ IHt2r) in H2.
        right. split.
        { apply  normal_left_strategy_app, H1. }
        right. split.
        { apply normal_right_strategy_app, H2. }
        left. exact H.
    - destruct H as [|[]]. left.
      apply rsH in H. apply lcbm_rlcbm_decompose_lemma.
      destruct H as [H|[_ [[]|[_ [[[] _]|[_ []]]]]]].
      left. apply IHt1r; [reflexivity|]. exact H.
    - destruct H as [|[]]. left.
      apply rsH. apply lcbm_rlcbm_decompose_lemma in H.
      destruct H as [H|[_ [[]|[_ [[[] _]|[_ []]]]]]].
      left. apply IHt1r; [reflexivity|]. exact H.
    - destruct H as [|[]]. left.
      apply rsH in H. apply lcbm_rlcbm_decompose_lemma.
      destruct H as [[]|[H1 [H|[H2 [[[] _]|[H3 H]]]]]].
      * apply  normal_left_strategy_app, (normal_on_term _ _ _ IHt1r) in H1.
        right. split.
        { apply  normal_left_strategy_app, H1. }
        left. apply IHt2r; [reflexivity|]. exact H.
      * apply  normal_left_strategy_app, (normal_on_term _ _ _ IHt1r) in H1.
        apply normal_right_strategy_app, (normal_on_term _ _ _ IHt2r) in H2.
        right. split.
        { apply  normal_left_strategy_app, H1. }
        right. split.
        { apply normal_right_strategy_app, H2. }
        right. split.
        { exact H3. }
        apply IHt2; [reflexivity|]. exact H.
    - destruct H as [|[]]. left.
      apply rsH. apply lcbm_rlcbm_decompose_lemma in H.
      destruct H as [[]|[H1 [H|[H2 [[[] _]|[H3 H]]]]]].
      * apply  normal_left_strategy_app, (normal_on_term _ _ _ IHt1r) in H1.
        right. split.
        { apply  normal_left_strategy_app, H1. }
        left. apply IHt2r; [reflexivity|]. exact H.
      * apply  normal_left_strategy_app, (normal_on_term _ _ _ IHt1r) in H1.
        apply normal_right_strategy_app, (normal_on_term _ _ _ IHt2r) in H2.
        right. split.
        { apply  normal_left_strategy_app, H1. }
        right. split.
        { apply normal_right_strategy_app, H2. }
        right. split.
        { exact H3. }
        apply IHt2; [reflexivity|]. exact H.
  + app_frames_tac H; split; intros H;
      (apply rsH; apply lcbm_rlcbm_decompose_lemma in H)
    + (apply rsH in H; apply lcbm_rlcbm_decompose_lemma).
    - destruct H as [[]|[H1 [[]|[H2 [H|[_ []]]]]]].
      apply  normal_left_strategy_app, (normal_on_term _ _ _ IHt1r) in H1.
      apply normal_right_strategy_app, (normal_on_term _ _ _ IHt2r) in H2.
      right. split.
      { apply  normal_left_strategy_app, H1. }
      right. split.
      { apply normal_right_strategy_app, H2. }
      left. exact H.
    - destruct H as [[]|[H1 [[]|[H2 [H|[_ []]]]]]].
      apply  normal_left_strategy_app, (normal_on_term _ _ _ IHt1r) in H1.
      apply normal_right_strategy_app, (normal_on_term _ _ _ IHt2r) in H2.
      right. split.
      { apply  normal_left_strategy_app, H1. }
      right. split.
      { apply normal_right_strategy_app, H2. }
      left. exact H.
    - destruct H as [H|[_ [[]|[_ [[[] _]|[_ []]]]]]].
      left. apply IHt1r; [reflexivity|]. exact H.
    - destruct H as [H|[_ [[]|[_ [[[] _]|[_ []]]]]]].
      left. apply IHt1r; [reflexivity|]. exact H.
    - destruct H as [[]|[H1 [H|[H2 [[[] _]|[H3 H]]]]]].
      * apply  normal_left_strategy_app, (normal_on_term _ _ _ IHt1r) in H1.
        right. split.
        { apply  normal_left_strategy_app, H1. }
        left. apply IHt2r; [reflexivity|]. exact H.
      * apply  normal_left_strategy_app, (normal_on_term _ _ _ IHt1r) in H1.
        apply normal_right_strategy_app, (normal_on_term _ _ _ IHt2r) in H2.
        right. split.
        { apply  normal_left_strategy_app, H1. }
        right. split.
        { apply normal_right_strategy_app, H2. }
        right. split.
        { exact H3. }
        apply IHt2; [reflexivity|]. exact H.
    - destruct H as [[]|[H1 [H|[H2 [[[] _]|[H3 H]]]]]].
      * apply  normal_left_strategy_app, (normal_on_term _ _ _ IHt1r) in H1.
        right. split.
        { apply  normal_left_strategy_app, H1. }
        left. apply IHt2r; [reflexivity|]. exact H.
      * apply  normal_left_strategy_app, (normal_on_term _ _ _ IHt1r) in H1.
        apply normal_right_strategy_app, (normal_on_term _ _ _ IHt2r) in H2.
        right. split.
        { apply  normal_left_strategy_app, H1. }
        right. split.
        { apply normal_right_strategy_app, H2. }
        right. split.
        { exact H3. }
        apply IHt2; [reflexivity|]. exact H.
Qed.

Lemma l_cbm_rl_cbm_decomposition_form :
  l_cbm == lcbm_decomposition ∧ rl_cbm == rlcbm_decomposition.
Proof.
  eapply conj_bifunctor; try apply strategies_equal_on_all_terms.
  apply conj_forall.
  intros t. induction t as [|x t [IHt IHtr]|t1 [IHt1 IHt1r] t2 [IHt2 IHt2r]]; split;
  intros [C c] H; split; intros H0.
  + var_frames_tac H. destruct H0 as [_ []].
  + var_frames_tac H. inversion H0.
  + var_frames_tac H. destruct H0 as [_ []].
  + var_frames_tac H. inversion H0.
  + lam_frames_tac H.
    - destruct H0 as [_ []].
    - apply lcbm_rlcbm_decompose_lemma, or_intror, IHt; [reflexivity|].
      exact H0.
  + lam_frames_tac H.
    - apply lcbm_rlcbm_decompose_lemma in H0.
      destruct H0 as [H0|[]].
      apply lcbm_rlcbm_decompose_lemma in H0.
      destruct H0 as [[]|[_ [[]|[_ [[_ []]|[_ []]]]]]].
    - apply lcbm_rlcbm_decompose_lemma in H0.
      destruct H0 as [H0|H0].
      * apply lcbm_rlcbm_decompose_lemma in H0.
        destruct H0 as [[]|[_ [[]|[_ [[[] _]|[_ []]]]]]].
      * apply IHt in H0; [|reflexivity]. apply H0.
  + lam_frames_tac H.
    - destruct H0 as [_ []].
    - destruct H0 as [[] _].
  + lam_frames_tac H.
    - apply lcbm_rlcbm_decompose_lemma in H0.
      destruct H0 as [[]|[_ [[]|[_ [[_ []]|[_ []]]]]]].
    - apply lcbm_rlcbm_decompose_lemma in H0.
      destruct H0 as [[]|[_ [[]|[_ [[[] _]|[_ []]]]]]].
  + app_frames_tac H;
    apply lcbm_rlcbm_decompose_lemma, or_introl, lcbm_rlcbm_decompose_lemma.
    - destruct H0 as [_ [abs_t1 mnf_t2]].
      right. split.
      { apply normal_left_strategy_app, normal_lcbm_rlcbm_decomposition.
        apply mnf_grammar, or_introl, abs_t1. }
      right. split.
      { apply normal_right_strategy_app, normal_lcbm_rlcbm_decomposition, mnf_t2. }
      left. repeat constructor. exact abs_t1.
    - left. apply IHt1r; [reflexivity|].
      exact H0.
    - destruct H0 as [[[mnf_t1 HC]|[neu_t1 HC]] Hc].
      * right. split.
      { apply normal_left_strategy_app, normal_lcbm_rlcbm_decomposition, mnf_t1. }
        left. apply IHt2r; [reflexivity|]. exact (conj HC Hc).
      * destruct (is_mnf (plug x c)) as [mnf_t2|nmnf_t2].
        { right. split.
          { apply normal_left_strategy_app, normal_lcbm_rlcbm_decomposition,
                  mnf_grammar, or_intror, neu_t1. }
          right. split.
          { apply normal_right_strategy_app, normal_lcbm_rlcbm_decomposition, mnf_t2. }
          right. split.
          { apply normal_only_β_contraction_app.
            apply neu_is_inert, inert_is_rigid, rigid_abstraction_disjoint in neu_t1.
            exact neu_t1. }
          apply IHt2; [reflexivity|].
          exact (conj HC Hc). }
        right. split.
        { apply normal_left_strategy_app, normal_lcbm_rlcbm_decomposition,
                mnf_grammar, or_intror, neu_t1. }
        left. apply IHt2r; [reflexivity|].
        refine (conj (L_CBM_on_not_mnf _ _ nmnf_t2 HC) Hc).
  + app_frames_tac H;
    apply lcbm_rlcbm_decompose_lemma in H0; destruct H0 as [H0|[]];
    apply lcbm_rlcbm_decompose_lemma in H0.
    - destruct H0 as [[]|[_ [[]|[mnf_t2 [[_ [abs_t1 _]]|[_ []]]]]]].
      apply normal_right_strategy_app, normal_lcbm_rlcbm_decomposition in mnf_t2.
      exact (conj I (conj abs_t1 mnf_t2)).
    - destruct H0 as [H0|[_ [[]|[_ [[[] _]|[_ []]]]]]].
      apply IHt1r in H0; [|reflexivity].
      exact H0.
    - destruct H0 as [[]|[mnf_t1 [H0|[mnf_t2 [[[] _]|[nabs_t1 H0]]]]]].
      * apply normal_left_strategy_app, normal_lcbm_rlcbm_decomposition in mnf_t1.
        apply IHt2r in H0; [|reflexivity].
        destruct H0 as [HC Hc].
        refine (conj (or_introl (conj mnf_t1 HC)) Hc).
      * simpl in *.
        apply  normal_left_strategy_app, normal_lcbm_rlcbm_decomposition, mnf_grammar in mnf_t1.
        apply normal_right_strategy_app, normal_lcbm_rlcbm_decomposition in mnf_t2.
        apply normal_only_β_contraction_app in nabs_t1.
        destruct mnf_t1 as [abs_t1|neu_t1]; [contradiction (nabs_t1 abs_t1)|].
        apply IHt2 in H0; [|reflexivity].
        destruct H0 as [HC Hc].
        refine (conj (or_intror (conj neu_t1 HC)) Hc).
  + app_frames_tac H;
    apply lcbm_rlcbm_decompose_lemma.
    - destruct H0 as [_ [abs_t1 mnf_t2]].
      right. split.
      { apply normal_left_strategy_app, normal_lcbm_rlcbm_decomposition.
        apply mnf_grammar, or_introl, abs_t1. }
      right. split.
      { apply normal_right_strategy_app, normal_lcbm_rlcbm_decomposition, mnf_t2. }
      left. repeat constructor. exact abs_t1.
    - left. apply IHt1r; [reflexivity|].
      exact H0.
    - destruct H0 as [[[mnf_t1 HC]|[neu_t1 HC]] Hc].
      * right. split.
      { apply normal_left_strategy_app, normal_lcbm_rlcbm_decomposition, mnf_t1. }
        left. apply IHt2r; [reflexivity|]. exact (conj HC Hc).
      * destruct (is_mnf (plug x c)) as [mnf_t2|nmnf_t2].
        { right. split.
          { apply normal_left_strategy_app, normal_lcbm_rlcbm_decomposition,
                  mnf_grammar, or_intror, neu_t1. }
          right. split.
          { apply normal_right_strategy_app, normal_lcbm_rlcbm_decomposition, mnf_t2. }
          right. split.
          { apply normal_only_β_contraction_app.
            apply neu_is_inert, inert_is_rigid, rigid_abstraction_disjoint in neu_t1.
            exact neu_t1. }
          apply IHt2; [reflexivity|].
          exact (conj HC Hc). }
        right. split.
        { apply normal_left_strategy_app, normal_lcbm_rlcbm_decomposition,
                mnf_grammar, or_intror, neu_t1. }
        left. apply IHt2r; [reflexivity|].
        refine (conj (L_CBM_on_not_mnf _ _ nmnf_t2 HC) Hc).
  + app_frames_tac H;
    apply lcbm_rlcbm_decompose_lemma in H0.
    - destruct H0 as [[]|[_ [[]|[mnf_t2 [[_ [abs_t1 _]]|[_ []]]]]]].
      apply normal_right_strategy_app, normal_lcbm_rlcbm_decomposition in mnf_t2.
      exact (conj I (conj abs_t1 mnf_t2)).
    - destruct H0 as [H0|[_ [[]|[_ [[[] _]|[_ []]]]]]].
      apply IHt1r in H0; [|reflexivity].
      exact H0.
    - destruct H0 as [[]|[mnf_t1 [H0|[mnf_t2 [[[] _]|[nabs_t1 H0]]]]]].
      * apply normal_left_strategy_app, normal_lcbm_rlcbm_decomposition in mnf_t1.
        apply IHt2r in H0; [|reflexivity].
        destruct H0 as [HC Hc].
        refine (conj (or_introl (conj mnf_t1 HC)) Hc).
      * simpl in *.
        apply  normal_left_strategy_app, normal_lcbm_rlcbm_decomposition, mnf_grammar in mnf_t1.
        apply normal_right_strategy_app, normal_lcbm_rlcbm_decomposition in mnf_t2.
        apply normal_only_β_contraction_app in nabs_t1.
        destruct mnf_t1 as [abs_t1|neu_t1]; [contradiction (nabs_t1 abs_t1)|].
        apply IHt2 in H0; [|reflexivity].
        destruct H0 as [HC Hc].
        refine (conj (or_intror (conj neu_t1 HC)) Hc).
Qed.

Corollary normal_l_cbm_nf : normal_form l_cbm == nf.
Proof.
  etransitivity.
  + exact (normal_form_proper _ _ (proj1 l_cbm_rl_cbm_decomposition_form)).
  + apply normal_lcbm_rlcbm_decomposition.
Qed.

Corollary l_cbm_phased_form : l_cbm == l_cbm_phased.
Proof.
  etransitivity; [apply l_cbm_rl_cbm_decomposition_form|].
  etransitivity; [apply lcbm_decomposition_phased_form|].
  apply union_proper; symmetry.
  + apply l_cbm_rl_cbm_decomposition_form.
  + apply down_strategy_proper.
    apply l_cbm_rl_cbm_decomposition_form.
Qed.

Corollary normal_rl_cbm_mnf : normal_form rl_cbm == mnf.
Proof.
  etransitivity.
  + exact (normal_form_proper _ _ (proj2 l_cbm_rl_cbm_decomposition_form)).
  + apply normal_lcbm_rlcbm_decomposition.
Qed.

Corollary rl_cbm_phased_form : rl_cbm == rl_cbm_phased.
Proof.
  etransitivity; [apply l_cbm_rl_cbm_decomposition_form|].
  etransitivity; [apply rlcbm_decomposition_phased_form|].
  symmetry.
  apply sequence_strategy_proper.
  { apply left_strategy_proper, l_cbm_rl_cbm_decomposition_form. }
  apply sequence_strategy_proper.
  { apply right_strategy_proper, l_cbm_rl_cbm_decomposition_form. }
  apply sequence_strategy_proper.
  { reflexivity. }
  { apply right_strategy_proper, l_cbm_rl_cbm_decomposition_form. }
Qed.
