Require Import Utf8.
Require Import Coq.Program.Basics. (* flip *)
Require Import Coq.Logic.Decidable.
Require Import Bool Coq.Arith.PeanoNat Coq.Lists.List.
Require Import Coq.funind.Recdef. (* iter *)
Require Import Coq.Classes.Morphisms. (* ++> *)
Require Import Coq.Strings.String.
Require Import Preliminaries Common.
Import ListNotations.

Inductive term : Type :=
| var (x : string) : term
| lam (x : string) (s : term) : term
| app (s : term) (t : term) : term.

Lemma var_inj : ∀ n1 n2, var n1 = var n2 → n1 = n2.
Proof. prove_std_inversion_lemma. Qed.

Lemma lam_inj2 : ∀ x1 x2 e1 e2, lam x1 e1 = lam x2 e2 → e1 = e2.
Proof. prove_std_inversion_lemma. Qed.

Lemma app_inj : ∀ e1 e2 e3 e4, app e1 e2 = app e3 e4 → e1 = e3 ∧ e2 = e4.
Proof. prove_std_inversion_lemma. Qed.

Fixpoint term_dec (x y:term) : dec (x = y).
Proof.
  destruct x,y; try (right; intros H; inversion H; fail).
  + eapply dec_prop_iff; [|exact (string_dec x x0)].
    split; [intros; f_equal; assumption|apply var_inj].
  + destruct (string_dec x x1);
    [subst|right; intros H; inversion H; contradiction].
    eapply dec_prop_iff; [|exact (term_dec x0 y)].
    split; [intros; f_equal; assumption|apply lam_inj2].
  + eapply dec_prop_iff; [|exact (and_dec _ _ (term_dec x1 y1) (term_dec x2 y2))].
    split; [intros []; f_equal; assumption|apply app_inj].
Qed.

Example term_dec' : eq_dec term := term_dec.

(** *** example terms *)

Definition term_I := lam "x" (var "x").
Definition term_pair := lam "x" (lam "y" (lam "f" (app (app (var "f") (var "x")) (var "y")))).
Definition term_ω := lam "x" (app (var "x") (var "x")).
Definition term_Ω := app term_ω term_ω.
Definition term_church (n:nat) := lam "f" (lam "x" (iter _ n (app (var "f")) (var "x"))).

(** *** families constructors *)

Definition variable (e:term) : Prop :=
  match e with var _ => True | _ => False end.

Definition abstraction (e:term) : Prop :=
  match e with | lam _ _ => True | _ => False end.

Definition is_abs_weak (e:term) : bool :=
  match e with | lam _ _ => true | _ => false end.

Definition application (e:term) : Prop :=
  match e with app e1 e2 => True | _ => False end.

Definition abs_of (t':term → Prop) (e:term) : Prop :=
  match e with lam _ e' => t' e' | _ => False end.

Definition app_of (t1 t2:term → Prop) (e:term) : Prop :=
  match e with app e1 e2 => t1 e1 ∧ t2 e2 | _ => False end.

Definition β_contrex  : term → Prop := app_of abstraction ●.

Definition βλ_contrex : term → Prop := app_of abstraction abstraction.

#[global]
Instance abs_of_monotone : Proper (subset ++> subset) abs_of.
Proof. intros X Y XY [| |]; simpl; try exact id. apply XY. Qed.

#[global]
Instance app_of_monotone : Proper (subset ++> subset ++> subset) app_of.
Proof.
  intros X Y XY Z W ZW [| |]; simpl; try exact id.
  intros [Xs Zt]. exact (conj (XY _ Xs) (ZW _ Zt)).
Qed.

Lemma app_abs_disjoint : application ⊍ abstraction.
Proof. intros [| |]; tauto. Qed.

Example Ω_β_contrex : β_contrex term_Ω.
Proof. repeat constructor. Qed.

(** *** inversion lemmas *)

Lemma variable_inv : ∀ e, variable e → ∃ x, e = var x.
Proof. refine (λ e,
  match e with
  | var n => λ _, ex_intro _ n eq_refl
  |     _ => False_ind _
  end).
Qed.

Lemma abs_of_inv : ∀ t' e, abs_of t' e → ∃ x e', e = lam x e' ∧ t' e'.
Proof. refine (λ t' e,
  match e with
  | lam x e' => λ t'e', ex_intro _ x (ex_intro _ e' (conj eq_refl t'e'))
  |        _ => False_ind _
  end).
Qed.

Lemma app_of_inv : ∀ t1 t2 e, app_of t1 t2 e → ∃ e1 e2,
  e = app e1 e2 ∧ t1 e1 ∧ t2 e2.
Proof.
  destruct e.
  1-2: intros H; contradiction H.
  intros H. destruct H.
  exists e1, e2. auto.
Qed.

Lemma abstraction_inv : ∀ e, abstraction e → ∃ x e', e = lam x e'.
Proof. refine (λ e,
  match e with
  | lam x e' => λ _, ex_intro _ x (ex_intro _ e' eq_refl)
  |        _ => False_ind _
  end).
Qed.

Lemma application_inv : ∀ e, application e → ∃ e1 e2, e = app e1 e2.
Proof. refine (λ e,
  match e with
  | app e1 e2 => λ _, ex_intro _ e1 (ex_intro _ e2 eq_refl)
  | _         => False_ind _
  end).
Qed.

Fact abs_of_full : abstraction == abs_of ●.
Proof. intros []; split; intros []; repeat constructor. Qed.

Fact app_of_full : application == app_of ● ●.
Proof. intros []; split; intros []; repeat constructor. Qed.

Example βλ_contrex_is_β_contrex : βλ_contrex ⊆ β_contrex.
Proof. apply app_of_monotone; [reflexivity|apply subset_full]. Qed.

(** *** rigid, hnf and whnf *)

Fixpoint rigid (t:term) : Prop := (* or head-neutral *)
  match t with
  | var _ => True
  | app s _ => rigid s
  | _ => False
  end.

Lemma rigid_grammar : rigid == variable ∪ app_of rigid ●.
Proof.
  intros [| |]; split; try union_tauto.
  intros H. right.  split; [assumption|constructor].
Qed.

Fact rigid_lam_inv : ∀ x e, ¬ rigid (lam x e).
Proof. intros x t H. exact H. Qed.

Lemma rigid_abstraction_disjoint : rigid ⊍ abstraction.
Proof. intros e H H0. invert_by abstraction_inv H0. exact (rigid_lam_inv _ _ H).
Qed.

Lemma rigid_β_contrex_disjoint : rigid ⊍ β_contrex.
Proof.
  intros e H H0. invert_by app_of_inv H0.
  exact (rigid_abstraction_disjoint x H H1).
Qed.

Definition whnf : term → Prop := abstraction ∪ rigid.

Example abs_is_whnf : abstraction ⊆ whnf.
Proof. exact (λ _, (@or_introl _ _)). Qed.

Example rigid_is_whnf : rigid ⊆ whnf.
Proof. exact (λ _, (@or_intror _ _)). Qed.

Fixpoint hnf (t:term) : Prop :=
  match t with
  | lam _ t' => hnf t'
  | _        => rigid t
  end.

Lemma hnf_grammar : hnf == rigid ∪ abs_of hnf.
Proof. intros [| |]; split; union_tauto. Qed.

Lemma rigid_is_hnf : rigid ⊆ hnf.
Proof. intros t H. apply hnf_grammar. left. exact H. Qed.

Lemma hnf_is_whnf : hnf ⊆ whnf.
Proof.
  intros t H. apply hnf_grammar in H.
  destruct H; [right; exact H|left].
  eapply abs_of_full, (abs_of_monotone _ _ (subset_full _)), H.
Qed.

Example hnf_with_Ω : hnf (app (var "x") term_Ω).
Proof. constructor. Qed.

Lemma rigid_rigid_wnf : rigid == rigid ∩ hnf.
Proof. intros []; split; intersection_tauto. Qed.

Lemma rigid_nabs_wnf : rigid == hnf ∖ abstraction.
Proof. intros []; split; difference_tauto. Qed.

Lemma hnf_lam_inv : ∀ x e, hnf (lam x e) → hnf e.
Proof. intros x e. exact id. Qed.

Lemma hnf_ind : ∀ (P : term → Prop),
  (∀ t, rigid t → P t) →
  (∀ x t, P t → P (lam x t)) →
  ∀ t, hnf t → P t.
Proof.
  intros P IHb IHl.
  induction t; simpl.
  + intros _. apply IHb. reflexivity.
  + intros H. exact (IHl _ _ (IHt H)).
  + intros H. apply IHb. simpl. exact H.
Qed.

Definition βwhnf_contrex : term → Prop := app_of abstraction whnf.
Definition hnfβ_contrex : term → Prop := app_of (abs_of hnf) ●.

Lemma abs_of_hnf_is_hnf : abs_of hnf ⊆ hnf.
Proof. intros t H. apply hnf_grammar. right. exact H. Qed.

Lemma abs_of_hnf_abs_hnf : abs_of hnf == abstraction ∩ hnf.
Proof.
  intros t. split.
  + intros H. split.
    - eapply abs_of_full, (abs_of_monotone _ _ (subset_full _)), H.
    - apply abs_of_hnf_is_hnf, H.
  + intros [H H0].
    apply hnf_grammar in H0. destruct H0.
    - contradiction (rigid_abstraction_disjoint _ H0 H).
    - exact H0.
Qed.

Lemma βwhnf_contrex_is_β_contrex : βwhnf_contrex ⊆ β_contrex.
Proof. eapply app_of_monotone; [reflexivity|apply subset_full]. Qed.

Lemma hnfβ_contrex_is_β_contrex : hnfβ_contrex ⊆ β_contrex.
Proof.
  apply app_of_monotone; [|intros ? _; exact I].
  intros t. apply abs_of_monotone, subset_full.
Qed.

(** *** nf and neu *)

Fixpoint neu (a:term) : Prop :=
  match a with
  | var x   => True
  | app a n => neu a ∧ nf n
  | _       => False
  end
with nf (n:term) : Prop :=
  match n with
  | lam x n => nf n
  (* _      => neu n *)
  | var x   => True
  | app a n => neu a ∧ nf n
  end.

Lemma nf_grammar : nf == abs_of nf ∪ neu.
Proof. intros [| |]; split; union_tauto. Qed.

Lemma neu_grammar : neu == variable ∪ app_of neu nf.
Proof. intros [| |]; split; union_tauto. Qed.

Lemma neu_is_nf : neu ⊆ nf.
Proof. intros t H. apply nf_grammar, or_intror, H. Qed.

Lemma neu_rigid_nf : neu == rigid ∩ nf.
Proof.
  intros t. induction t; split; try intersection_tauto.
  intros H. split; [|apply neu_is_nf, H].
  apply proj1, IHt1, proj1 in H. exact H.
Qed.

Example pair_nf : nf term_pair.
Proof. repeat constructor. Qed.

Example eight_nf : nf (term_church 8).
Proof. repeat constructor. Qed.

Example Ω_not_nf : ¬ nf term_Ω.
Proof. exact (@proj1 _ _). Qed.

Lemma nf_is_hnf : nf ⊆ hnf.
Proof.
  intros t. induction t; simpl.
  + exact id.
  + exact IHt.
  + intros [H _]. apply neu_rigid_nf, H.
Qed.

Definition nfβnf_contrex : term → Prop :=
  app_of (abs_of nf) nf.

Lemma abs_of_nf_is_nf : abs_of nf ⊆ nf.
Proof. intros t H. apply nf_grammar. left. exact H. Qed.

Lemma abs_of_nf_abs_nf : abs_of nf == abstraction ∩ nf.
Proof.
  intros t. split.
  + intros H. split.
    - eapply abs_of_full, (abs_of_monotone _ _ (subset_full _)), H.
    - apply abs_of_nf_is_nf, H.
  + intros [H H0].
    apply nf_grammar in H0. destruct H0.
    - exact H0.
    - apply neu_rigid_nf, proj1 in H0.
      contradiction (rigid_abstraction_disjoint _ H0 H).
Qed.

Lemma nfβnf_contrex_is_hnfβ_contrex : nfβnf_contrex ⊆ hnfβ_contrex.
Proof.
  apply app_of_monotone; [|intros ? _; exact I].
  apply abs_of_monotone, nf_is_hnf.
Qed.

(** *** wnf and inert *)

Fixpoint inert (e:term) : Prop := (* or weakly neutral *)
  match e with
  | var _   => True
  | lam _ _ => False
  | app i e => inert i ∧ wnf e
  end
with wnf (e:term) : Prop :=
  match e with
  | lam _ e' => True
  (* _       => inert e *)
  | app e1 e2 => inert e1 ∧ wnf e2
  | var _     => True
  end.

Lemma wnf_grammar : wnf == abstraction ∪ inert.
Proof. intros [| |]; split; union_tauto. Qed.

Lemma inert_grammar : inert == variable ∪ app_of inert wnf.
Proof. intros [| |]; split; union_tauto. Qed.

Lemma inert_is_wnf : inert ⊆ wnf.
Proof. intros t H. apply wnf_grammar, or_intror, H. Qed.

Lemma inert_rigid_wnf : inert == rigid ∩ wnf.
Proof.
  intros t. induction t; split; try intersection_tauto.
  intros H. split; [|apply inert_is_wnf, H].
  apply proj1, IHt1, proj1 in H. exact H.
Qed.

Lemma inert_is_rigid : inert ⊆ rigid.
Proof. intros t H. apply inert_rigid_wnf, proj1 in H. exact H. Qed.

Lemma abstraction_is_wnf : abstraction ⊆ wnf.
Proof. intros t H. exact (proj2 (wnf_grammar _) (or_introl H)). Qed.

Lemma wnf_is_whnf : wnf ⊆ whnf.
Proof.
  intros t H. apply wnf_grammar in H.
  destruct H; [left; exact H|right].
  apply inert_is_rigid, H.
Qed.

Example wnf_with_Ω : wnf (lam "x" term_Ω).
Proof. constructor. Qed.

Example wnf_hnf_with_Ω : (wnf ∩ hnf) (lam "x" (app (var "x") term_Ω)).
Proof. repeat constructor. Qed.

Definition βwnf_contrex : term → Prop := app_of abstraction wnf.

Lemma abs_is_wnf : abstraction ⊆ wnf.
Proof. intros t H. apply wnf_grammar. left. exact H. Qed.

Lemma nf_is_wnf_and_neu_is_inert : nf ⊆ wnf ∧ neu ⊆ inert.
Proof. apply conj_forall. intros t. induction t; simpl; tauto. Qed.

Lemma nf_is_wnf : nf ⊆ wnf.
Proof. exact (proj1 nf_is_wnf_and_neu_is_inert). Qed.

Lemma neu_is_inert : neu ⊆ inert.
Proof. exact (proj2 nf_is_wnf_and_neu_is_inert). Qed.

Lemma βλ_contrex_is_βwnf_contrex : βλ_contrex ⊆ βwnf_contrex.
Proof. apply app_of_monotone; [reflexivity|apply abs_is_wnf]. Qed.

Lemma βwnf_contrex_is_βwhnf_contrex : βwnf_contrex ⊆ βwhnf_contrex.
Proof. apply app_of_monotone; [reflexivity|apply wnf_is_whnf]. Qed.

Lemma βwnf_contrex_is_β_contrex : βwnf_contrex ⊆ β_contrex.
Proof. apply app_of_monotone; [reflexivity|apply subset_full]. Qed.

Lemma nfβnf_contrex_is_βwnf_contrex : nfβnf_contrex ⊆ βwnf_contrex.
Proof.
  apply app_of_monotone; [|apply nf_is_wnf].
  etransitivity; [eapply abs_of_monotone, subset_full|].
  apply two_inclusions, abs_of_full.
Qed.

(** *** middle normal forms *)

Definition mnf (m:term) : Prop :=
  match m with
  | lam x t => True
  | _       => neu m
  end.

Lemma mnf_grammar : mnf == abstraction ∪ neu.
Proof. intros [| |]; split; union_tauto. Qed.

Lemma abs_is_mnf : abstraction ⊆ mnf.
Proof. intros t H. exact (proj2 (mnf_grammar _) (or_introl H)). Qed.

Lemma nf_is_mnf : nf ⊆ mnf.
Proof.
  intros t H. apply nf_grammar in H. destruct H.
  + apply abs_is_mnf, abs_of_full.
    exact (abs_of_monotone _ _ (subset_full _) _ H).
  + apply mnf_grammar, or_intror, H.
Qed.

Lemma mnf_is_wnf : mnf ⊆ wnf.
Proof.
  intros t H.
  apply wnf_grammar. apply mnf_grammar in H. destruct H.
  + left. exact H.
  + right. apply neu_is_inert, H.
Qed.

Definition βmnf_contrex : term → Prop := app_of abstraction mnf.

Lemma βλ_contrex_is_βmnf_contrex : βλ_contrex ⊆ βmnf_contrex.
Proof. apply app_of_monotone; [reflexivity|apply abs_is_mnf]. Qed.

Lemma nfβnf_contrex_is_βmnf_contrex : nfβnf_contrex ⊆ βmnf_contrex.
Proof.
  apply app_of_monotone; [|apply nf_is_mnf].
  etransitivity; [eapply abs_of_monotone, subset_full|].
  apply two_inclusions, abs_of_full.
Qed.

Lemma βmnf_contrex_is_βwnf_contrex : βmnf_contrex ⊆ βwnf_contrex.
Proof. apply app_of_monotone; [reflexivity|apply mnf_is_wnf]. Qed.

(** *** stuck terms *)

Fixpoint sdet_stuck (t:term) : Prop :=
  match t with
  | var _   => True
  | lam _ _ => False
  | app s t => sdet_stuck s ∨ ¬ abstraction t
  end.

Lemma sdet_stuck_abstraction_disjoint : sdet_stuck ⊍ abstraction.
Proof. intros e H H0. invert_by abstraction_inv H0. exact H. Qed.

Fixpoint l_cbv_stuck (t:term) : Prop :=
  match t with
  | var _   => True
  | lam _ _ => False
  | app s t => l_cbv_stuck s ∨ (abstraction s ∧ l_cbv_stuck t)
  end.

Lemma l_cbv_stuck_abstraction_disjoint : l_cbv_stuck ⊍ abstraction.
Proof. intros e H H0. invert_by abstraction_inv H0. exact H. Qed.

Fixpoint r_cbv_stuck (t:term) : Prop :=
  match t with
  | var _   => True
  | lam _ _ => False
  | app s t => r_cbv_stuck t ∨ (r_cbv_stuck s ∧ abstraction t)
  end.

Lemma r_cbv_stuck_abstraction_disjoint : r_cbv_stuck ⊍ abstraction.
Proof. intros e H H0. invert_by abstraction_inv H0. exact H. Qed.

(** *** evaluator's pseudocode *)

(*

Parameter subst : string → term → term → term.

Fixpoint eval (t : term) :=
  match t with
  | app s t => match eval s with
               | lam x u => eval (subst x (eval t) u)
               |       u =>         app u (eval t)
               end
  |       t => t
  end.

*)