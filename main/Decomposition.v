Require Import Utf8.
Require Import Coq.Program.Basics Coq.Classes.Morphisms.
Require Import Coq.Lists.List.
Require Import Coq.Strings.String.
Require Import Preliminaries Common Term Context.
Import ListNotations.

(** ** Decomposition *)

Definition decomposition : Type := context * term.

Definition decomposition_dec : eq_dec decomposition :=
  pair_eq_dec context_dec term_dec.

Definition recompose : decomposition → term := uncurry plug.

Theorem decomposition_ind_by_term : ∀ P,
  (∀ t d, t = recompose d → P d) → ∀ d, P d.
Proof. intros P H d. exact (H _ _ eq_refl). Qed.

Lemma recompose_cons : ∀ f C c,
  recompose (f :: C, c) = reframe f (recompose (C, c)).
Proof. intros f C c. reflexivity. Qed.

Definition strategy : Type := decomposition → Prop.

Definition decomposes_into_in
  (t : term) (d : decomposition) (s : strategy) : Prop :=
  t = recompose d ∧ d ∈ s.

Lemma decomposes_into_in_variance : ∀ t d,
  Proper (subset ++> impl) (decomposes_into_in t d).
Proof. intros t d s0 s1 s01 [H H0]. exact (conj H (s01 _ H0)). Qed.

Definition det_strategy (s:strategy) : Prop :=
  ∀ t, ∃≤1 d, decomposes_into_in t d s.

Definition normal_form (s:strategy) (t:term) : Prop :=
  ¬ ∃ d, decomposes_into_in t d s.

Definition neutral_form (s:strategy) (t1:term) : Prop :=
  family_arrow (app t1) (normal_form s) (normal_form s).

Lemma normal_form_variance : Proper (subset --> subset) normal_form.
Proof.
  intros S1 S0 S01 e H [d []]. unfold flip in S01.
  apply H. exists d. split; [exact H0|exact (S01 _ H1)].
Qed.

Lemma normal_form_proper : Proper (set_eq ==> set_eq) normal_form.
Proof.
  intros S0 S1 S01.
  apply two_inclusions in S01.
  apply two_inclusions. split; apply normal_form_variance, S01.
Qed.

Lemma det_strategy_variance : Proper (subset --> impl) det_strategy.
Proof.
  unfold Proper, respectful, flip.
  intros F G GF H e d d' [H0 H1] [H2 H3].
  apply GF in H1. apply GF in H3.
  exact (H e d d' (conj H0 H1) (conj H2 H3)).
Qed.

Definition strategies_equal_on_term (t : term) (s1 s2 : strategy) : Prop :=
  ∀ d, t = recompose d → s1 d ↔ s2 d.

#[global]
Instance strategies_equal_on_term_sym : ∀ t,
  Symmetric (strategies_equal_on_term t).
Proof. intros t s1 s2 H d H0. symmetry. exact (H _ H0). Qed.

Lemma strategies_equal_on_all_terms : ∀ s1 s2,
  (∀ t, strategies_equal_on_term t s1 s2) → s1 == s2.
Proof. intros s1 s2 H d. exact (H _ _ eq_refl). Qed.

Ltac strategies_equal_on_all_terms_tac :=
  let t := fresh "t" in let C := fresh "C" in
  let c := fresh "c" in let H := fresh "H" in
  apply strategies_equal_on_all_terms; intros t; induction t;
  intros [C c] H;
  [var_frames_tac H|lam_frames_tac H|app_frames_tac H].

(** *** strategy tools *)

(** **** parametric context independent strategy *)
Definition pcc_strategy (K:Type) (Context:K→context→Prop) (contrex:K→term→Prop) : strategy :=
  λ d, match d with (C,c) => ∃ k, Context k C ∧ contrex k c end.

(** **** context dependent strategy *)

(** It's only example because the redex is capable of storing whole
  strategy information *)
Example cd_strategy (Context:context→Prop) (redex:context→term→Prop) : strategy :=
  λ d, match d with (C,c) => Context C ∧ redex C c end.

Example cd_strategy_tautology : ∀ S:strategy,
  cd_strategy ● (curry S) == S.
Proof. intros S [C c]. repeat split; [|assumption]. intros [_ H]. exact H. Qed.

Example pcc_strategy_tautology : ∀ F r,
  cd_strategy F r == pcc_strategy context (eq ∩₂ const F) r.
Proof.
  intros F r [C c]. cbn. split.
  + intros []. exists C. repeat split; assumption.
  + intros [k [[H H0] H1]]. subst. auto.
Qed.

(** *** example strategies *)

Example empty_β_strategy : ∅ × β_contrex == (∅: strategy).
Proof. intros [C c]. split; [|intros []]. apply proj1. Qed.

Definition full_β : strategy := ● × β_contrex.

Definition only_β_contraction     : strategy := Hole × β_contrex.
Definition only_βλ_contraction    : strategy := Hole × βλ_contrex.
Definition only_βwhnf_contraction : strategy := Hole × βwhnf_contrex.

Definition sdet : strategy := SDET × βλ_contrex.

Definition cbn : strategy := CBN × β_contrex.

Definition head : strategy := Head × β_contrex.
Definition  ihs : strategy :=   HS × hnfβ_contrex.
Definition   hs : strategy :=   HS × β_contrex.

Definition no  : strategy :=  LO × β_contrex.
Definition no' : strategy := RLO × β_contrex.

Definition lis : strategy := LS × hnfβ_contrex.

Definition l_cbv : strategy := L_CBV × βλ_contrex.

Definition r_cbv : strategy := R_CBV × βλ_contrex.

Definition cbwh : strategy := L_CBV × βwhnf_contrex.

Definition cbv : strategy := Weak × βλ_contrex.

Definition cbw : strategy := Weak × βwnf_contrex.

Definition weak : strategy := Weak × β_contrex.

Definition   low : strategy :=   LOW × β_contrex.
Definition l_cbw : strategy := L_CBW × βwnf_contrex.
Definition r_cbw : strategy := R_CBW × βwnf_contrex.

Definition ll_cbw : strategy := LL_CBW × βwnf_contrex.
Definition rl_cbw : strategy := RL_CBW × βwnf_contrex.
Definition lr_cbw : strategy := LR_CBW × βwnf_contrex.
Definition rr_cbw : strategy := RR_CBW × βwnf_contrex.
Definition   scbw : strategy :=   SCBW × βwnf_contrex.

Definition  l_cbm : strategy :=  L_CBM × βmnf_contrex.
Definition rl_cbm : strategy := RL_CBM × βmnf_contrex.
Definition  r_cbm : strategy :=  R_CBM × βmnf_contrex.
Definition rr_cbm : strategy := RR_CBM × βmnf_contrex.
Definition    cbm : strategy :=    CBM × βmnf_contrex.
Definition rigid_cbm : strategy := Rigid_CBM × βmnf_contrex.

Definition li  : strategy := LI × nfβnf_contrex.
Definition ri  : strategy := RI × nfβnf_contrex.
Definition inn : strategy := ● × nfβnf_contrex.

(* cbnd *)

Example not_cc_strategy : ∃ s : strategy, ¬ ∃ C c, s == C × c.
Proof.
  exists (cbn ∪ cbv).
  intros [C [c H]].
  enough ((C × c) ([Lapp term_I], app term_I (app term_I term_I))).
  { apply H in H0.
    destruct H0.
    + exact (proj1 (proj1 H0)).
    + exact (proj2 (proj2 H0)). }
  split.
  + enough ((cbn ∪ cbv) ([Lapp term_I], app term_I term_I)).
    { apply H in H0. exact (proj1 H0). }
    right. repeat split.
  + enough ((cbn ∪ cbv) ([], app term_I (app term_I term_I))).
    { apply H in H0. exact (proj2 H0). }
    left. repeat split.
Qed.

(** *** substrategies *)

Ltac monotone_substrategy_tactic :=
  apply cartesian_product_monotone; try reflexivity;
  try (apply Uniform_monotone;
    intros [| |]; simpl; try exact id; try tauto);
  try (apply app_of_monotone; try reflexivity).

Lemma sdet_is_cbn : sdet ⊆ cbn.
Proof. monotone_substrategy_tactic. apply subset_full. Qed.

Lemma cbn_is_head : cbn ⊆ head.
Proof. monotone_substrategy_tactic. exact CBN_is_Head. Qed.

Lemma head_is_no : head ⊆ no.
Proof. monotone_substrategy_tactic. exact Head_is_LO. Qed.

Lemma head_is_not_weak : ¬ (head ⊆ weak).
Proof.
  apply counterexample.
  exists ([Lam "x"], (app (lam "x" (var "x")) (var "x"))).
  simpl. unfold Weak, CBN, full_set, const. simpl. tauto.
Qed.


Lemma head_is_hs : head ⊆ hs.
Proof. monotone_substrategy_tactic. apply Head_is_HS. Qed.

Lemma ihs_is_hs : ihs ⊆ hs.
Proof.
  monotone_substrategy_tactic. etransitivity;
  [eapply abs_of_monotone, subset_full|apply two_inclusions, abs_of_full].
Qed.

Lemma ihs_is_lis : ihs ⊆ lis.
Proof. monotone_substrategy_tactic. apply HS_is_LS. Qed.

Lemma l_cbv_is_cbv : l_cbv ⊆ cbv.
Proof. monotone_substrategy_tactic. Qed.

Lemma cbv_is_cbw : cbv ⊆ cbw.
Proof. monotone_substrategy_tactic. exact abstraction_is_wnf. Qed.

Lemma l_cbv_is_cbwh : l_cbv ⊆ cbwh.
Proof. monotone_substrategy_tactic. apply abs_is_whnf. Qed.

Lemma l_cbv_is_l_cbw : l_cbv ⊆ l_cbw.
Proof. monotone_substrategy_tactic; apply abstraction_is_wnf. Qed.

Lemma r_cbv_is_r_cbw : r_cbv ⊆ r_cbw.
Proof. monotone_substrategy_tactic; apply abstraction_is_wnf. Qed.
Lemma l_cbw_is_cbw : l_cbw ⊆ cbw.
Proof. monotone_substrategy_tactic. Qed.
Lemma r_cbw_is_cbw : r_cbw ⊆ cbw.
Proof. monotone_substrategy_tactic. Qed.

Lemma l_cbw_is_ll_cbw : l_cbw ⊆ ll_cbw.
Proof. monotone_substrategy_tactic. apply W_CBW_is_GS_CBW. Qed.
Lemma l_cbw_is_rl_cbw : l_cbw ⊆ rl_cbw.
Proof. monotone_substrategy_tactic. apply W_CBW_is_GS_CBW. Qed.
Lemma r_cbw_is_lr_cbw : r_cbw ⊆ lr_cbw.
Proof. monotone_substrategy_tactic. apply W_CBW_is_GS_CBW. Qed.
Lemma r_cbw_is_rr_cbw : r_cbw ⊆ rr_cbw.
Proof. monotone_substrategy_tactic. apply W_CBW_is_GS_CBW. Qed.

Lemma    cbw_is_scbw :    cbw ⊆ scbw.
Proof. monotone_substrategy_tactic. apply W_CBW_is_GS_CBW. Qed.
Lemma ll_cbw_is_scbw : ll_cbw ⊆ scbw.
Proof. monotone_substrategy_tactic. apply LL_CBW_is_SCBW. Qed.
Lemma rl_cbw_is_scbw : rl_cbw ⊆ scbw.
Proof. monotone_substrategy_tactic. apply RL_CBW_is_SCBW. Qed.
Lemma lr_cbw_is_scbw : lr_cbw ⊆ scbw.
Proof. monotone_substrategy_tactic. apply LR_CBW_is_SCBW. Qed.
Lemma rr_cbw_is_scbw : rr_cbw ⊆ scbw.
Proof. monotone_substrategy_tactic. apply RR_CBW_is_SCBW. Qed.

Lemma l_cbv_is_l_cbm : l_cbv ⊆ l_cbm.
Proof.
  apply cartesian_product_monotone.
  + etransitivity; [apply L_CBV_is_RL_CBM|apply RL_CBM_is_L_CBM].
  + exact βλ_contrex_is_βmnf_contrex.
Qed.

Lemma r_cbv_is_r_cbm : r_cbv ⊆ r_cbm.
Proof.
  apply cartesian_product_monotone.
  + etransitivity; [apply R_CBV_is_RR_CBM|apply RR_CBM_is_R_CBM].
  + exact βλ_contrex_is_βmnf_contrex.
Qed.

Lemma cbv_is_cbm : cbv ⊆ cbm.
Proof.
  monotone_substrategy_tactic.
  + etransitivity.
    - exact Weak_is_Rigid_CBM.
    - exact Rigid_CBM_is_CBM.
  + exact abs_is_mnf.
Qed.

Lemma low_is_weak : low ⊆ weak.
Proof. monotone_substrategy_tactic. Qed.
Lemma cbw_is_weak : cbw ⊆ weak.
Proof. monotone_substrategy_tactic. apply subset_full. Qed.

Lemma weak_is_full_β : weak ⊆ full_β.
Proof. monotone_substrategy_tactic. apply subset_full. Qed.

Lemma sdet_is_r_cbv : sdet ⊆ r_cbv.
Proof. monotone_substrategy_tactic. Qed.

Lemma sdet_cbn_r_cbv : sdet == cbn ∩ r_cbv.
Proof.
  apply two_inclusions. split.
  { apply subset_of_intersection. split; [apply sdet_is_cbn|apply sdet_is_r_cbv]. }
  intros [C c] [[NC Nc] [VC Vc]].
  split; [|assumption].
  apply SDET_CBN_R_CBV.
  split; assumption.
Qed.

(** *** nondeterministic examples *)

Example ndet_inn : ¬ det_strategy inn.
Proof.
  apply counterexample. exists (app term_Ω term_Ω).
  apply counterexample. exists (([Rapp term_Ω], term_Ω)).
  apply counterexample. exists (([Lapp term_Ω], term_Ω)).
  repeat (apply counterexample; eexists; [repeat constructor|]).
  intros H.
  inversion H.
Qed.

Example ndet_cbv : ¬ det_strategy cbv.
Proof.
  apply counterexample. exists (app term_Ω term_Ω).
  apply counterexample. exists (([Rapp term_Ω], term_Ω)).
  apply counterexample. exists (([Lapp term_Ω], term_Ω)).
  repeat (apply counterexample; eexists; [repeat constructor|]).
  intros H.
  inversion H.
Qed.

Example ndet_cbw : ¬ det_strategy cbw.
Proof. exact (λ H, ndet_cbv ((det_strategy_variance _ _ cbv_is_cbw) H)). Qed.

Example ndet_scbw : ¬ det_strategy scbw.
Proof. exact (λ H, ndet_cbw ((det_strategy_variance _ _ cbw_is_scbw) H)). Qed.

Example ndet_cbm : ¬ det_strategy cbm.
Proof. exact (λ H, ndet_cbv ((det_strategy_variance _ _ cbv_is_cbm) H)). Qed.

Example ndet_weak : ¬ det_strategy weak.
Proof. exact (λ H, ndet_cbw ((det_strategy_variance _ _ cbw_is_weak) H)). Qed.

Example ndet_full_β : ¬ det_strategy full_β.
Proof. exact (λ H, ndet_weak ((det_strategy_variance _ _ weak_is_full_β) H)). Qed.

Example ndet_hs : ¬ det_strategy hs.
Proof.
  apply counterexample. eexists.
  apply counterexample. eexists (([], _)).
  apply counterexample. exists (([Rapp term_ω; Lam "x"], term_Ω)).
  repeat (apply counterexample; eexists; [repeat constructor|]).
  + repeat constructor.
  + intros H. inversion H.
Qed.

Theorem nondeterministic_strategies :
  ∀ s, In s [cbv; cbw; weak; hs; scbw; cbm; inn; full_β] → ¬ det_strategy s.
Proof.
  intros s [H|[H|[H|[H|[H|[H|[H|[H|[]]]]]]]]]; subst.
  + exact ndet_cbv.
  + exact ndet_cbw.
  + exact ndet_weak.
  + exact ndet_hs.
  + exact ndet_scbw.
  + exact ndet_cbm.
  + exact ndet_inn.
  + exact ndet_full_β.
Qed.

(** *** normal forms *)

Lemma normal_var : ∀ S x, normal_form S (var x) ↔ ¬ S ([], var x).
Proof.
  intros S x. split; intros H H0; apply H; clear H.
  + eexists. split; [|exact H0]. reflexivity.
  + destruct H0 as [[? ?] [H0 ?]].
    apply var_frames in H0. simpl in H0. destruct H0. subst.
    assumption.
Qed.

Lemma normal_lam : ∀ S x t, normal_form S (lam x t) ↔
    ¬ S ([], lam x t)
  ∧ ∀ C c, t = plug C c → ¬ S (Lam x :: C, c).
Proof.
  intros S t. split.
  + intros H. split; [intros H0|intros C c H1 H0];
    apply H; clear H;
    eexists; (split; [|exact H0]); subst; reflexivity.
  + intros [H H0] [[C c] [H1 H2]].
    apply lam_frames in H1. simpl in H1. destruct H1 as [[]|[C' []]]; subst.
    - exact (H H2).
    - exact (H0 _ _ eq_refl H2).
Qed.

Lemma normal_app : ∀ S s t, normal_form S (app s t) ↔
    ¬ S ([], app s t)
  ∧ (∀ C c, s = plug C c → ¬ S (Rapp t :: C, c))
  ∧  ∀ C c, t = plug C c → ¬ S (Lapp s :: C, c).
Proof.
  intros S t. split.
  + intros H. repeat split; intros C c H1 H0 + intros H0;
    apply H; clear H;
    eexists; (split; [|exact H0]); subst; reflexivity.
  + intros [H [H0 H1]] [[C c] [H2 H3]].
    apply app_frames in H2.
    destruct H2 as [[]|[[C' []]|[C' []]]]; simpl in *; subst.
    - exact (H H3).
    - exact (H0 _ _ eq_refl H3).
    - exact (H1 _ _ eq_refl H3).
Qed.

Lemma abstraction_is_weak_normal_form : abstraction ⊆ normal_form weak.
Proof.
  intros t H [[C c] [H0 H1]].
  apply abstraction_inv in H. destruct H as [x [t' H]]. subst.
  lam_frames_tac H0.
  + destruct H1 as [_ []].
  + destruct H1 as [[[] _] _].
Qed.

Example r_cbv_normal_not_wnf : ¬ (wnf == normal_form r_cbv).
Proof.
  apply counterexample. exists (app term_I (var "x")).
  intros [_ H].
  apply H. clear H.
  apply normal_app. repeat split; intros C c H H0 + intros H0.
  + exact (proj2 (proj2 H0)).
  + lam_frames_tac H.
    - apply H0.
    - var_frames_tac H1. apply H0.
  +   var_frames_tac H.  apply H0.
Qed.
