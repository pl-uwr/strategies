Require Import Utf8.
Require Import Coq.Program.Basics Coq.Classes.Morphisms Coq.Logic.Decidable.
Require Import Bool Coq.Arith.PeanoNat Coq.Lists.List.
Require Import Preliminaries.
Import ListNotations.

(* available since 8.13 in Coq.Init.Datatypes *)
Definition uncurry {A B C:Type} (f:A -> B -> C)
  (p:A * B) : C := match p with (x, y) => f x y end.

Notation "f @@ x" := (f x) (at level 15, right associativity, only parsing).
Notation "x |> f" := (f x) (at level 15, only parsing).

Definition ex_le1 {A:Type} (P : A → Prop) : Prop :=
  ∀ x x', P x → P x' → x = x'.

Notation "∃≤1 x , p" := (ex_le1 (λ x, p))
  (at level 200, right associativity) : type_scope.

Definition rev_app {A B} (x:A) (f:A→B) := f x.

Lemma disj_of_neg : ∀ (P Q:Prop), ¬ P ∨ ¬ Q → ¬ (P ∧ Q).
Proof. firstorder. Qed.

Lemma true_iff_iff : ∀ A, (True ↔ A) ↔ A.
Proof. tauto. Qed.

Lemma counterexample {A:Type} : ∀ (P:A→Prop), (∃ x, ¬ P x) → ¬ ∀ x, P x.
Proof. firstorder. Qed.

Lemma not_impl : ∀ A B : Prop, A → ¬ B → ¬ (A → B).
Proof. tauto. Qed.

Lemma conj_forall {A:Type} : ∀ (P Q:A→Prop),
  ((∀ x, P x) ∧ (∀ x, Q x)) ↔ ∀ x, P x ∧ Q x.
Proof. firstorder. Qed.

Lemma conj_conjs1 : ∀ P Q R S : Prop,
  (P ∧ Q) ∧ (R ∧ S) → (P ∧ R) ∧ (Q ∧ S).
Proof. tauto. Qed.

Lemma conj_bifunctor : ∀ (P Q R S : Prop),
  (P → R) → (Q → S) → P ∧ Q →  R ∧ S.
Proof. exact (λ P Q R S f g x, (conj (f (proj1 x)) (g (proj2 x)))). Qed.

#[global]
Instance Forall2_sym {A : Type}: ∀ (R: A → A → Prop),
  Symmetric R → Symmetric (Forall2 R).
Proof.
  intros R R_sym xs. induction xs; intros ys Hs;
  inversion Hs; [apply Forall2_nil|subst].
  constructor; [apply R_sym, H1|apply (IHxs _ H3)].
Qed.

Definition fst_map {A B C: Type} (f: A → C) (p: A * B) : C * B :=
  match p with (x, y) => (f x, y) end.

Definition snd_map {A B C: Type} (f: B → C) (p: A * B) : A * C :=
  match p with (x, y) => (x, f y) end.

(*Definition cross {A B:Type} (f:A→B) {A' B':Type} (f':A'→B') (p:A*A') : B*B' :=
  match p with (a, a') => (f a, f' a') end.*)

Ltac and_ex_destruct :=
  match goal with
  | [H : and _ _ |- _ ] => destruct H; and_ex_destruct
  | [H : ex _ |- _ ] => destruct H; and_ex_destruct
  | _ => idtac
  end.

Ltac invert_by inv_lemma H :=
  apply inv_lemma in H; and_ex_destruct; subst.

Ltac prove_std_inversion_lemma :=
  intros; inversion H; auto.

(* Ltac inv H := inversion H; subst; clear H. *)

(** ** Optionals *)

Lemma Some_inj {A:Type} : ∀ (x x':A), Some x = Some x' → x = x'.
Proof. prove_std_inversion_lemma. Qed.

#[global]
Instance option_eq_dec {A : Type} :
  eq_dec A → eq_dec (option A).
Proof.
  intros A_dec [] [];
  try (right; intros H; inversion H; fail);
  [|left; reflexivity].
  eapply dec_prop_iff; [| exact (A_dec a a0)].
  split; [intros; f_equal; assumption|apply Some_inj].
Qed.

Definition option_bind {A B:Type} (xs : option A) (f : A → option B) : option B :=
  match xs with Some x => f x | None => None end.

Definition option_else {A:Type} (default : A) (xs : option A) : A :=
  match xs with Some x => x | None => default end.

Definition option_append {A:Type} (xs ys: option A) : option A :=
  match xs with Some _ => xs | None => ys end.

Definition option_arrow {A B C:Type} (f : A → option B) (g : B → option C) : A → option C :=
  λ x, option_bind (f x) g.

Lemma option_map_None {A B:Type} : ∀ (f:A→B) xs,
  option_map f xs = None → xs = None.
Proof. intros f [|]; [|reflexivity]. intros H. inversion H. Qed.

Lemma option_map_Some {A B:Type} : ∀ (f:A→B) xs y,
  option_map f xs = Some y → ∃ x, xs = Some x ∧ y = f x.
Proof.
  intros f [x|] y H; [|inversion H].
  exists x. split; [reflexivity|]. symmetry. apply (Some_inj _ _ H).
Qed.

Lemma option_append_Some {A : Type} : ∀ xs ys (x : A),
  option_append xs ys = Some x → xs = Some x ∨ ys = Some x.
Proof. intros [|]; tauto. Qed.

Lemma option_append_Some2 {A : Type} : ∀ xs ys (x : A),
  option_append xs ys = Some x → xs = Some x ∨ xs = None ∧ ys = Some x.
Proof. intros [|]; tauto. Qed.

Lemma option_append_None_l {A : Type} : ∀ xs: option A,
  option_append None xs = xs.
Proof. reflexivity. Qed.

Lemma option_append_None_r {A : Type} : ∀ xs: option A,
  option_append xs None = xs.
Proof. intros []; reflexivity. Qed.

Lemma option_append_Some_l {A : Type} : ∀ (x : A) xs,
  option_append (Some x) xs = Some x.
Proof. reflexivity. Qed.

(** ** Logical combinators *)

Example    nul_op (A:Type) := A.
Definition  un_op (A:Type) := A → A. (* endofunctions *)
Definition bin_op (A:Type) := A → A → A.

Example    par_nul_op  {X A:Type} : A → X → A := const.
Example    par_nul_op' {X A:Type} : nul_op A → nul_op (X→A) := par_nul_op.

Example    par_un_op  {X A B:Type} : (A→B) → (X→A) → X → B := compose.
Example    par_un_op' {X A:Type}   : un_op A → un_op (X→A) := par_un_op.

Definition par_bin_op  {X A B C:Type} (f:A→B→C) (s:X→A) (t:X→B) (x:X) : C := f (s x) (t x).
Example    par_bin_op' {X A:Type} : bin_op A → bin_op (X→A) := par_bin_op.

(** ** Set theory *)

Notation "'𝓟' A" := (A → Prop) (at level 55, only parsing). (* 1d4df *)
Notation "x '∈' A" := (A x)     (at level 70, only parsing).
Notation "x '∉' A" := (¬ x ∈ A) (at level 70, only parsing).

Definition   ext_eq {A B:Type} (f g:A→B) := ∀ x, f x = g x.
Definition   subset {A:Type} (s t:𝓟 A) : Prop := ∀ x, x ∈ s → x ∈ t.
Definition   set_eq {A:Type} (s t:𝓟 A) : Prop := ∀ x, x ∈ s ↔ x ∈ t.
Definition disjoint {A:Type} (s t:𝓟 A) : Prop := ∀ x, x ∈ s → x ∈ t → False.
Definition decidable_set {A:Type} (s :𝓟 A) : Prop := ∀ x, decidable (x ∈ s).
Definition dec_set       {A:Type} (s :𝓟 A) : Type := ∀ x, dec (x ∈ s).
Definition is_full       {A:Type} (s :𝓟 A) : Prop := ∀ x, x ∈ s.

Definition    empty_set {A:Type} : 𝓟 A := const False.
Definition     full_set {A:Type} : 𝓟 A := const True.
Definition   complement {A:Type} : ( un_op (𝓟 A)) := compose not.

Definition intersection {A:Type} : (bin_op (𝓟 A)) := par_bin_op and.
Definition        union {A:Type} : (bin_op (𝓟 A)) := par_bin_op or.
Definition    subset_on {A:Type} : (bin_op (𝓟 A)) := par_bin_op impl.
Definition    set_eq_on {A:Type} : (bin_op (𝓟 A)) := par_bin_op iff.
Definition family_intersection {A:Type} (𝓐 : 𝓟 (𝓟 A)) : 𝓟 A :=
  λ x, ∀ A, A ∈ 𝓐 → x ∈ A.
Definition        family_union {A:Type} (𝓐 : 𝓟 (𝓟 A)) : 𝓟 A :=
  λ x, ∃ A, A ∈ 𝓐 ∧ x ∈ A.
Definition cartesian_product {A B:Type} (s:𝓟 A) (t:𝓟 B) : 𝓟 (A * B) :=
  λ x, let (a, b) := x in a ∈ s ∧ b ∈ t.

Notation "∅" := empty_set.
Notation "●" := full_set. (* 25cf *)
Infix    "=ext" := ext_eq (at level 70).
Infix    "⊆"    := subset (at level 70).
Infix    "=="   := set_eq (at level 70).
Infix    "⊍"    := disjoint     (at level 70). (* 228d *)
Infix    "∩"    := intersection (at level 60).
Infix    "∪"    := union        (at level 65).
Infix    "⊆[]"  := subset_on    (at level 67).
Infix    "==[]" := set_eq_on    (at level 67).
Notation "s '⊇' t" := (subset t s) (at level 70, only parsing).
Notation "s '⊈' t" := (¬ s ⊆ t) (at level 70).
Infix    "×"    :=   cartesian_product (at level 50).
Notation "⋂"    := family_intersection.
Notation "⋃"    :=        family_union.
Notation "s ∁"  :=      (complement s) (at level 55).
Definition difference {A:Type} (s t:𝓟 A) : 𝓟 A  := s ∩ t∁.
Definition proper_subset {A:Type} (s t:𝓟 A) :    Prop  := s ⊆ t ∧ t ⊈ s.
Definition family_arrow {X Y:Type} (f:X → Y) (A : 𝓟 X) (B : 𝓟 Y) := A ⊆ compose B f.
Infix     "∖"   := difference    (at level 60).
Infix     "⊂"   := proper_subset (at level 70).

Definition    empty_set2 {A B:Type} :         A → B → Prop  := const empty_set.
Definition intersection2 {A B:Type} : bin_op (A → B → Prop) := par_bin_op intersection.
Definition        union2 {A B:Type} : bin_op (A → B → Prop) := par_bin_op union.
Definition subset2 {A B:Type} (s t:A→B→Prop) : Prop := is_full (par_bin_op subset s t).
Definition set_eq2 {A B:Type} (s t:A→B→Prop) : Prop := is_full (par_bin_op set_eq s t).

Notation "R ∗" :=  (flip R) (at level 55). (* ^op or ^T *)
Notation "∅₂" := empty_set2.
Notation "s '⊆₂' t" := (subset2 s t) (at level 70).
Notation "s '==₂' t" := (set_eq2 s t) (at level 70).
Infix     "∩₂"  :=    intersection2 (at level 65).
Infix     "∪₂"  :=        union2    (at level 65).

Ltac union_tauto      := unfold union, par_bin_op     ; cbn     ; tauto.
Ltac union_tauto_in H := unfold union, par_bin_op in H; cbn in H; tauto.
Ltac intersection_tauto := unfold intersection, par_bin_op;  cbn; tauto.
Ltac difference_tauto := unfold difference, complement, compose; intersection_tauto.

#[global]
Instance ext_eq_equiv {A B:Type} : Equivalence (@ext_eq A B).
Proof.
  split.
  + intros ? ?. reflexivity.
  + intros ? ? H ?. symmetry. apply H.
  + intros x y z H H0 a. transitivity (y a).
    - apply H. - apply H0.
Qed.

#[global]
Instance subset_preorder {A:Type} : PreOrder (@subset A).
Proof.
  split.
  + intros ? ?. exact id.
  + intros x y z xy yz a xa. exact (yz _ (xy _ xa)).
Qed.

#[global]
Instance set_eq_equiv {A:Type} : Equivalence (@set_eq A).
Proof.
  split.
  + intros ? ?. reflexivity.
  + intros ? ? H ?. symmetry. apply H.
  + intros x y z H H0 a. transitivity (y a).
    - apply H. - apply H0.
Qed.

#[global]
Instance proper_subset_strictorder {A:Type} : StrictOrder (@proper_subset A).
Proof.
  split.
  + intros ? H. apply proj2 in H. apply H. reflexivity.
  + intros x y z [xy xy0] [yz yz0].
    exact (conj (transitivity xy yz) (λ zx, xy0 (transitivity yz zx))).
Qed.

Definition both_dir {A:Type} (R:A→A→Prop) : A→A→Prop := R ∩₂ R∗.

Lemma two_inclusions {A:Type} : @set_eq A ==₂ both_dir subset.
Proof.
  intros s t. split.
  + intros H. split; intros x; apply H.
  + intros H. intros x. split; apply H.
Qed.

Lemma double_complement {A:Type} : ∀ s:𝓟 A, s ⊆ s ∁ ∁.
Proof. exact (λ _ _, rev_app). Qed.

Lemma complement_idempotent {A:Type} : ∀ (s:𝓟 A), decidable_set s → s == s ∁ ∁.
Proof.
  intros s s_dec.
  split; [apply double_complement|].
  destruct (s_dec x) as [|H]; [auto|].
  intros H0.
  contradiction (H0 H).
Qed.

Lemma subset_of_intersection {A : Type} : ∀ X Y Z : 𝓟  A, X ⊆ Y ∧ X ⊆ Z ↔ X ⊆ Y ∩ Z.
Proof.
  exact (λ X Y Z, conj
    (λ XYZ x Xx, conj (proj1 XYZ _ Xx) (proj2 XYZ _ Xx))
    (λ XYZ, conj (λ x Xx, (proj1 (XYZ _ Xx))) (λ x Xx, (proj2 (XYZ _ Xx))))).
Qed.

(* [left|right] is better than [right|left] *)

Lemma union_variance {A:Type} : Proper (subset ++> subset ++> subset) (@union A).
Proof.
  intros X Y XY Z W ZW a [H|H]; [left|right]; [exact (XY _ H)|exact (ZW _ H)].
Qed.

#[global]
Instance union_proper {A:Type} : Proper (set_eq ==> set_eq ==> set_eq) (@union A).
Proof.
  intros X Y XY Z W ZW. split;
  apply union_variance; apply two_inclusions;
  assumption + (symmetry; assumption).
Qed.

#[global]
Instance cartesian_product_monotone {A B:Type} :
  Proper (subset ++> subset ++> subset) (@cartesian_product A B).
Proof.
  intros F G FG f g fg [C c] [].
  split; [apply FG|apply fg]; assumption.
Qed.

(* Lemma resum {El:Type} : ∀ (A B:El→Prop), A ∖ B ∪ B ⊆ A ∪ B.
Proof. intros A B x [[H _]|H]; [left|right]; assumption. Qed. *)

Definition bin_closed_on {A:Type} (op:bin_op A) (s:𝓟 A) : Prop :=
  ∀ x y, s x → s y → s (op x y).

Definition least_element {A:Type} (Aleq:A→A→Prop) (e:A) : Prop :=
  ∀ x, Aleq e x.

Definition greatest_element {A:Type} (Aleq:A→A→Prop) (e:A) : Prop :=
  ∀ x, Aleq x e.

Definition lower_set {A:Type} (Aleq:A→A→Prop) (s:𝓟 A) : Prop :=
  ∀ x y, Aleq x y → s y → s x.

Definition directed_set {A:Type} (Aleq:A→A→Prop) (s:𝓟 A) : Prop :=
  ∀ x y, s x → s y → ∃ z, Aleq x z ∧ Aleq y z.

Definition ideal {A:Type} : (A→A→Prop) → 𝓟 𝓟 A := lower_set ∩₂ directed_set.

Lemma lower_sets_closed_on_intersecion {A:Type} : ∀ Aleq:A→A→Prop,
  bin_closed_on intersection (lower_set Aleq).
Proof.
  intros Aleq X Y H H0 x y H1 [H2 H3].
  exact (conj (H _ _ H1 H2) (H0 _ _ H1 H3)).
Qed.

Lemma subset_empty {A:Type} : least_element (@subset A) ∅.
Proof. intros _ _ []. Qed.

Lemma subset_full {A:Type} : greatest_element (@subset A) ●.
Proof. intros _ ? _. exact I. Qed.

(** ** Other common *)

Example Commutative {T Z:Type} (Zeq:Z→Z→Prop) (op:T→T→Z) : Prop :=
  ∀ t₁ t₂:T, Zeq (op t₁ t₂) (op t₂ t₁).

Example Symmetric_Commutative {A:Type} : (@Symmetric A) == Commutative iff.
Proof.
  intros R. split; intros H.
  + split; exact (λ H0, H _ _ H0).
  + intros ? ? H0. apply H, H0.
Qed.
