Require Import Utf8.
Require Import Coq.Program.Basics Coq.Classes.Morphisms.
Require Import Coq.Lists.List.
Require Import Coq.Strings.String.
Require Import Preliminaries Common Term.
Import ListNotations.

(** ** Context *)

Inductive frame : Type :=
|  Lam : string → frame
| Rapp : term → frame
| Lapp : term → frame.

Definition reframe (f:frame) (h:term) : term :=
  match f with
  | Lam x  => lam x h
  | Rapp e => app h e
  | Lapp e => app e h
  end.

Definition frame_dec : eq_dec frame.
Proof.
  intros [|e|e] [|e0|e0];
  try (right; intros H; inversion H; fail);
  try (left; reflexivity);
  (eapply dec_prop_iff; [|exact (term_dec e e0) + exact (string_dec s s0)]);
  (split; [intros; f_equal; assumption|]);
  intros H; inversion H; reflexivity.
Qed.

Definition context : Type := list frame.

Definition context_dec : eq_dec context := list_eq_dec frame_dec.

(* plug oi *)
Definition plug (C:context) (e:term) : term := fold_right reframe e C.

Definition plug_io (C:context) (e:term) : term := plug (rev C) e.

Definition subterm e' e := ∃ C, e = plug C e'.

Example ω_Ω_subterm : subterm term_ω term_Ω.
Proof. exists ([Rapp term_ω]). reflexivity. Qed.

Definition prefix C e := ∃ e', e = plug C e'.

Definition prefixes e : 𝓟 context := prefix ∗ e.

Lemma prefix_nil : ∀ t, prefix [] t.
Proof. intros C. exists C. reflexivity. Qed.

Example Ω_prefix : prefix [Lapp term_ω] term_Ω.
Proof. exists term_ω. reflexivity. Qed.

Definition Prefix {A:Type} C E := ∃ E':list A, E = C ++ E'.

#[global]
Instance Prefix_refl {A:Type} : Reflexive (@Prefix A).
Proof. exists []. symmetry. apply app_nil_r. Qed.

Lemma Prefix_nil {A:Type} : least_element (@Prefix A) [].
Proof. intros C. exists C. reflexivity. Qed.

Lemma Prefix_cons {A:Type} : ∀ (x:A) xs ys,
  Prefix xs ys ↔ Prefix (x::xs) (x::ys).
Proof.
  intros x xs ys.
  split; intros [zs H]; exists zs.
  + simpl. f_equal. exact H.
  + inversion H. reflexivity.
Qed.

Example Prefix_example : Prefix [Lam "y"] [Lam "y"; Lapp term_ω].
Proof. apply Prefix_cons, Prefix_nil. Qed.

Lemma prefixes_lower_set : ∀ e, lower_set Prefix (prefixes e).
Proof.
  intros e C E [E' H] [e' H0]. subst.
  exists (plug E' e').
  induction C; [reflexivity|].
  simpl. rewrite IHC. reflexivity.
Qed.

Lemma prefixes_not_directed_set : ¬ ∀ e, directed_set Prefix (prefixes e).
Proof.
  apply counterexample. exists (app (var "x") (var "x")).
  apply counterexample. exists [Lapp (var "x")].
  apply counterexample. exists [Rapp (var "x")].
  repeat (apply counterexample; eexists; [repeat constructor|]);
  try (eexists; reflexivity).
  intros [C [[C0 H0] [C1 H1]]].
  subst. simpl in H1. inversion H1.
Qed.

Definition context_arrow : context → (𝓟 context) → (𝓟 context) → Prop :=
  λ C, family_arrow (List.app C).

Lemma context_arrow_id : ∀ F, context_arrow [] F F.
Proof. intros t C. exact id. Qed.

Lemma context_arrow_compose : ∀ A B C f g,
  context_arrow f A B →
  context_arrow g B C →
  context_arrow (g ++ f) A C.
Proof.
  intros A B C f g fAB gBC x Ax.
  unfold compose. rewrite <- app_assoc.
  exact (gBC _ (fAB _ Ax)).
Qed.

(** *** families constructors *)

Definition Hole {A:Type} (xs:list A) : Prop :=
  match xs with [] => True | _ => False end.

Definition Lam_of (T':𝓟 context) (C:context) : Prop :=
  match C with Lam _ :: C' => T' C' | _ => False end.

Definition Lapp_of (t1:𝓟 term) (T2:𝓟 context) (C:context) : Prop :=
  match C with Lapp e1 :: C' => t1 e1 ∧ T2 C' | _ => False end.

Definition Rapp_of (T1:𝓟 context) (t2:𝓟 term) (C:context) : Prop :=
  match C with Rapp e2 :: C' => t2 e2 ∧ T1 C' | _ => False end.

#[global]
Instance Lapp_of_monotone : Proper (subset ++> subset ++> subset) Lapp_of.
Proof.
  intros X Y XY Z W ZW [|[]]; simpl; try exact id.
  intros [Xs Zt]. exact (conj (XY _ Xs) (ZW _ Zt)).
Qed.

#[global]
Instance Rapp_of_monotone : Proper (subset ++> subset ++> subset) Rapp_of.
Proof.
  intros X Y XY Z W ZW [|[]]; simpl; try exact id.
  intros [Zt Xs]. exact (conj (ZW _ Zt) (XY _ Xs)).
Qed.

(** *** inversion lemmas *)

Lemma Hole_inv {A:Type} : ∀ C:list A, Hole C → C = [].
Proof. intros []; [| intros []]. intros _. reflexivity. Qed.

Lemma Lam_of_inv : ∀ F C, Lam_of F C →
  ∃ x1 C', C = Lam x1 :: C' ∧ F C'.
Proof.
  intros ? [|[]] H; try contradiction H.
  do 2 eexists. repeat split; apply H.
Qed.
(*

Lemma Rapp_of_inv : ∀ F f C, Rapp_of F f C →
  ∃ t2 C', C = Rapp t2 :: C' ∧ F C' ∧ f t2.
Proof.
  intros ? ? [|[]] H; try contradiction H.
  do 2 eexists. repeat split; apply H.
Qed.

Lemma Lapp_of_inv : ∀ F f C, Lapp_of f F C →
  ∃ t1 C', C = Lapp t1 :: C' ∧ F C' ∧ f t1.
Proof.
  intros ? ? [|[]] H; try contradiction H.
  do 2 eexists. repeat split; apply H.
Qed.
*)

Lemma var_frames : ∀ x C r, var x = plug C r →
  C = [] ∧ r = var x.
Proof.
  intros x [|[| |]] r; subst.
  1: auto.
  all: intros H; discriminate H.
Qed.

Lemma lam_frames : ∀ x e' C e,
  lam x e' = plug C e →
         C = [] ∧ e = lam x e'
    ∨ ∃ C', C = Lam x::C' ∧ e' = plug C' e.
Proof.
  intros x e' [|f C'] e.
  { left. auto. }
  right. exists C'.
  destruct f; inversion H. auto.
Qed.

Lemma app_frames : ∀ e1 e2 C e, app e1 e2 = plug C e →
    C = [] ∧ e = app e1 e2
  ∨ (∃ C', C = Rapp e2 :: C' ∧ e1 = plug C' e)
  ∨ (∃ C', C = Lapp e1 :: C' ∧ e2 = plug C' e).
Proof.
  intros e1 e2 [|f C'] e H.
  { left. auto. }
  right.
  destruct f; inversion H; subst; [left|right]; exists C'; auto.
Qed.

(*
Ltac Hole_inv_tac H :=
  apply Hole_inv in H; subst.

Ltac Rapp_of_inv_tac H :=
  apply Rapp_of_inv in H; destruct H as [? [? [? [? ?]]]]; subst.

Ltac Lapp_of_inv_tac H :=
  apply Lapp_of_inv in H; destruct H as [? [? [? [? ?]]]]; subst.
*)

Ltac var_frames_tac H :=
  apply var_frames in H; simpl in H; destruct H; subst.

Ltac lam_frames_tac H :=
  apply lam_frames in H; simpl in H; destruct H; and_ex_destruct; subst.

Ltac app_frames_tac H :=
  apply app_frames in H; simpl in H; destruct H as [H|[H|H]]; and_ex_destruct; subst.

(** *** context families *)

Fixpoint Uniform (F:frame → Prop) (C:context) : Prop :=
  match C with
  | []     => True
  | f :: C => F f ∧ Uniform F C
  end.

Definition Non_uniform (C : context → Prop) : Prop :=
  ¬ ∃ F, C == Uniform F.

Lemma Uniform_ind : ∀ (F:frame → Prop) (P:context → Prop),
  P [] →
  (∀ f C', F f → P C' → P (f::C')) →
  ∀ C, Uniform F C → P C.
Proof.
  intros F P H H0 C.
  induction C as [|f C' IH].
  + intros _. exact H.
  + intros [H1 H2]. exact (H0 f C' H1 (IH H2)).
Qed.

Lemma Uniform_monotone : Proper (subset ++> subset) Uniform.
Proof.
  intros F1 F2 H C. induction C; [reflexivity|].
  intros [H0 H1]. split.
  + apply H, H0.
  + apply IHC, H1.
Qed.

Lemma Uniform_proper : Proper (set_eq ==> set_eq) Uniform.
Proof.
  intros F1 F2 H.
  split; apply Uniform_monotone; apply two_inclusions;
  assumption + (symmetry; assumption).
Qed.

Lemma context_arrow_uniform : ∀ (F : frame → Prop) f,
  F f → context_arrow [f] (Uniform F) (Uniform F).
Proof. intros F f Ff C FC. split; assumption. Qed.

Lemma Uniform_intersection : ∀ F G, Uniform F ∩ Uniform G == Uniform (F ∩ G).
Proof.
  intros F G C.
  induction C as [|f C IH]; [cbv; tauto|].
  split.
  + intros [[Ff FC] [Gf GC]]. repeat split; try (apply IH; split); assumption.
  + intros [[Ff Gf] FGC]. apply IH in FGC. destruct FGC as [FC GC]. repeat split; assumption.
Qed.

Lemma Uniform_lower_set : ∀ F, lower_set Prefix (Uniform F).
Proof.
  intros F C E [E' H] H0. subst.
  induction C; [constructor|].
  simpl in H0. destruct H0 as [H H0].
  split; [exact H|].
  apply IHC, H0.
Qed.

Definition CBN_frame (f:frame) : Prop :=
  match f with
  | Rapp _ => True
  | _      => False
  end.

Lemma CBN_frame_inv : ∀ f, CBN_frame f → ∃ e, f = Rapp e.
Proof. intros [] []. eexists. reflexivity. Qed.

Definition CBN : context → Prop := Uniform CBN_frame.

Lemma CBN_grammar : CBN == Hole ∪ Rapp_of CBN ●.
Proof.
  intros C. split; destruct C as [|f C'].
  + intros  _. left.  constructor.
  + intros []. right. destruct f; destruct H. split; [constructor|assumption].
  + intros _.         constructor.
  + intros [[]|].     destruct f; destruct H; split; [constructor|assumption].
Qed.

Definition SDET_frame (f:frame) : Prop :=
  match f with
  | Rapp v => abstraction v
  | _      => False
  end.

Definition Weak_frame (f:frame) : Prop :=
  match f with
  | Lam _ => False
  | _     => True
  end.

Definition SDET : context → Prop := Uniform SDET_frame.
Definition Weak : context → Prop := Uniform Weak_frame.

Lemma SDET_is_CBN : SDET ⊆ CBN.
Proof.
  apply Uniform_monotone.
  intros [| |]; try exact id.
  exact (λ _, I).
Qed.

Fixpoint Rigid (C:context) : Prop :=
  match C with
  | [] => True
  | Lapp e::_ => rigid e
  | Rapp _::C => Rigid C
  | _  => False
  end.

Lemma Rigid_grammar :
  Rigid == Hole ∪ Rapp_of Rigid ● ∪ Lapp_of rigid ●.
Proof.
  intros [|[]]; split; try union_tauto;
  intros; [left|]; right; split; constructor + assumption.
Qed.

Lemma Rigid_on_rigid : ∀ B, Rigid B → family_arrow (plug B) rigid rigid.
Proof.
  intros B HB b Hb. induction B as [|f B' IH]; [exact Hb|].
  destruct f; [|exact (IH HB)|]; exact HB.
Qed.

Lemma Rigid_on_not_abstraction : ∀ B, Rigid B →
  family_arrow (plug B) (abstraction ∁) (abstraction ∁).
Proof.
  intros B HB b Hb. destruct B as [|f B']; [exact Hb|].
  destruct f; intros []. exact HB.
Qed.

Fact CBN_is_Rigid : CBN ⊆ Rigid.
Proof.
  intros C. induction C.
  + intros _. constructor.
  + intros [].
    destruct (CBN_frame_inv _ H). subst.
    apply IHC. assumption.
Qed.

Lemma CBN_is_Weak : CBN ⊆ Weak.
Proof. apply Uniform_monotone. intros [| |]; simpl; tauto. Qed.

Fixpoint Head (C:context) : Prop :=
  match C with
  | Lam _ :: C' => Head C'
  | _           => CBN C
  end.

Lemma Head_grammar : Head == CBN ∪ Lam_of Head.
Proof. intros [|[]]; split; union_tauto. Qed.

Fact CBN_is_Head : CBN ⊆ Head.
Proof. intros ? ?. apply Head_grammar. left. assumption. Qed.

Lemma CBN_Weak_Head : CBN == Weak ∩ Head.
Proof.
  apply two_inclusions; split.
  + intros C H. split; [apply CBN_is_Weak|apply CBN_is_Head]; exact H.
  + intros C [H H0].
    apply Head_grammar in H0.
    destruct H0; [exact H0|].
    destruct C as [|[]]; contradiction H0 + contradiction (proj1 H).
Qed.

Theorem Head_non_uniform : Non_uniform Head.
Proof.
  intros [F H].
  apply                (proj2 (H [Rapp term_ω; Lam "x"])).
  split; [apply ((proj1 (H [Lam "x"; Rapp _])) (conj I I))|].
  split; [exact (proj1 (proj1 (H [Lam "x"]) I))|].
  constructor.
Qed.

Fixpoint head_variable (t : term) : string :=
  match t with
  | var x    => x
  | lam _ t' => head_variable t'
  | app s _  => head_variable s
  end.

Definition HS_frame (f:frame) : Prop :=
  match f with
  | Lapp _ => False
  | _      => True
  end.

Definition HS : context → Prop := Uniform HS_frame.

Fixpoint LS (C:context) : Prop :=
  match C with
  | Lam _ :: C' => LS C'
  (* | _         => HS C ∨ RLS C *)
  | []            => True
  | Rapp t2 :: C' => HS C' ∨ RLS C'
  | Lapp t1 :: C' => neu t1 ∧ LS C'
  end
with RLS (C:context) : Prop :=
  match C with
  | Rapp t2 :: C' => RLS C'
  | Lapp t1 :: C' => neu t1 ∧ LS C'
  | _             => False
  end.

Lemma LS_grammar : LS == Lam_of LS ∪ HS ∪ RLS.
Proof.
  intros C. induction C as [|[]]; split; try union_tauto.
  intros H.
  destruct H as [[H|H]|H].
  + exact H.
  + apply IHC. left. right. apply H.
  + contradiction H.
Qed.

Lemma RLS_grammar : RLS == Rapp_of RLS ● ∪ Lapp_of neu LS.
Proof.
  intros [|[]]; split; try union_tauto.
  intros ?. left. split; [constructor|assumption].
Qed.

Lemma CBN_is_HS : CBN ⊆ HS.
Proof.
  apply Uniform_monotone.
  intros [| |]; try exact id.
  exact (λ _, I).
Qed.

Lemma Head_is_HS : Head ⊆ HS.
Proof.
  intros C. induction C as [|[]]; cbn; try tauto.
  intros [_ H]. exact (conj I (CBN_is_HS _ H)).
Qed.

Lemma HS_is_LS : HS ⊆ LS.
Proof. intros C H. apply LS_grammar. left. right. exact H. Qed.

Lemma RLS_is_LS : RLS ⊆ LS.
Proof. intros C H. apply LS_grammar. right. exact H. Qed.

Lemma plug_RLS_rigid : ∀ C c, RLS C → rigid (plug C c).
Proof.
  intros C. induction C as [|[]].
  + intros _ [].
  + intros _ [].
  + exact IHC.
  + simpl. intros _ [neu_t _]. apply inert_is_rigid, neu_is_inert, neu_t.
Qed.

Theorem LS_non_uniform : Non_uniform LS.
Proof.
  intros [F H].
  enough (H0 : LS [Rapp term_ω; Lam "x"; Lapp (var "x")]).
  { destruct H0 as [[_ [[] _]]|[]]. }
  apply (proj2 (H _)).
  repeat split.
  + apply (H [Rapp  _]). left. constructor.
  + apply (H [Lam "x"]). constructor.
  + apply (H [Lapp  _]). split; constructor.
Qed.

Fixpoint RLO (C:context) : Prop :=
  match C with
  | []           => True
  | Rapp e :: C' => RLO C'
  | Lapp e :: C' => neu e ∧ LO C'
  | _            => False
  end
with LO (C:context) : Prop :=
  match C with
  | Lam _ :: C' => LO C'
  (* _          => RLO C *)
  | []           => True
  | Rapp e :: C' => RLO C'
  | Lapp e :: C' => neu e ∧ LO C'
  end.

Lemma LO_grammar : LO == Lam_of LO ∪ RLO.
Proof. intros [|[]]; split; union_tauto. Qed.

Lemma RLO_grammar : RLO == Hole ∪ Rapp_of RLO ● ∪ Lapp_of neu LO.
Proof.
  intros [|[]]; split; try union_tauto.
  intros ?. left. right. split; [constructor|assumption].
Qed.

Example Lapp_neu_on_LO : ∀ t, neu t → context_arrow [Lapp t] LO RLO.
Proof. intros t H C H0. simpl. split; assumption. Qed.

Fact RLO_is_LO : RLO ⊆ LO.
Proof. intros C H. apply LO_grammar. right. assumption. Qed.

Theorem LO_non_uniform : Non_uniform LO.
Proof.
  intros [F H].
  apply                (proj2 (H [Rapp term_ω; Lam "x"])).
  split; [exact (proj1 (proj1 (H [Rapp term_ω]) I))|].
  split; [exact (proj1 (proj1 (H [Lam     "x"]) I))|].
  constructor.
Qed.

Lemma CBN_is_RLO : CBN ⊆ RLO.
Proof.
  intros C. induction C as [|f C']; [intros; constructor|].
  destruct f; intros [[]].
  apply IHC', H.
Qed.

Lemma Head_is_LO : Head ⊆ LO.
Proof.
  intros C H. apply LO_grammar. induction C as [|f C']; [right; constructor|].
  destruct f.
  + left.
    apply IHC', LO_grammar in H. exact H.
  + right.
    apply CBN_is_RLO, H.
  + destruct H as [[]].
Qed.

Lemma RLO_Rigid_LO : RLO == Rigid ∩ LO.
Proof.
  intros C. induction C as [|[]]; split; try intersection_tauto.
  + intros H. split; [apply IHC|]; exact H.
  + intros H. split; [apply neu_rigid_nf, H|tauto].
Qed.

Lemma CBN_is_LO : CBN ⊆ LO.
Proof. etransitivity; [apply CBN_is_Head|apply Head_is_LO]. Qed.

Lemma CBN_not_Weak_LO : ¬ CBN == Weak ∩ LO.
Proof.
  apply counterexample. exists ([Lapp (var "x")]).
  intersection_tauto.
Qed.

Definition L_CBV_frame (f:frame) : Prop :=
  match f with
  | Lapp e => abstraction e
  | Rapp e => True
  | _      => False
  end.

Definition R_CBV_frame (f:frame) : Prop :=
  match f with
  | Lapp e => True
  | Rapp e => abstraction e
  | _      => False
  end.

Definition L_CBW_frame (f:frame) : Prop :=
  match f with
  | Lapp e => wnf e
  | Rapp e => True
  | _      => False
  end.

Definition R_CBW_frame (f:frame) : Prop :=
  match f with
  | Lapp e => True
  | Rapp e => wnf e
  | _      => False
  end.

Definition LOW_frame (f:frame) : Prop :=
  match f with
  | Lapp e => inert e
  | Rapp e => True
  | _      => False
  end.

Definition LI_frame (f:frame) : Prop :=
  match f with
  | Lam _  => True
  | Rapp t => True
  | Lapp t => nf t
  end.

Definition RI_frame (f:frame) : Prop :=
  match f with
  | Lam _ => True
  | Rapp t => nf t
  | Lapp t => True
  end.

Definition L_CBV  : context → Prop := Uniform L_CBV_frame.
Definition R_CBV  : context → Prop := Uniform R_CBV_frame.
Definition L_CBW  : context → Prop := Uniform L_CBW_frame.
Definition R_CBW  : context → Prop := Uniform R_CBW_frame.
Definition LOW    : context → Prop := Uniform LOW_frame.
Definition LI     : context → Prop := Uniform LI_frame.
Definition RI     : context → Prop := Uniform RI_frame.

Lemma  CBN_is_L_CBV :  CBN ⊆ L_CBV.
Proof. apply Uniform_monotone. intros [| |]; simpl; tauto. Qed.
Lemma SDET_is_R_CBV : SDET ⊆ R_CBV.
Proof. apply Uniform_monotone. intros [| |]; simpl; tauto. Qed.
Lemma  CBN_is_LOW   :  CBN ⊆ LOW.
Proof. apply Uniform_monotone. intros [| |]; simpl; tauto. Qed.

Lemma L_CBW_grammar  : L_CBW  == Hole ∪ Lapp_of  wnf  L_CBW ∪ Rapp_of  L_CBW    ●.
Proof. intros [|[]]; split; union_tauto. Qed.
Lemma R_CBW_grammar  : R_CBW  == Hole ∪ Lapp_of    ●  R_CBW ∪ Rapp_of  R_CBW  wnf.
Proof. intros [|[]]; split; union_tauto. Qed.
Lemma   LOW_grammar  :   LOW  == Hole ∪ Lapp_of inert   LOW ∪ Rapp_of    LOW    ●.
Proof. intros [|[]]; split; union_tauto. Qed.

Lemma L_CBV_is_L_CBW : L_CBV ⊆ L_CBW.
Proof. apply Uniform_monotone. intros [| |]; simpl; tauto + apply abs_is_wnf. Qed.
Lemma R_CBV_is_R_CBW : R_CBV ⊆ R_CBW.
Proof. apply Uniform_monotone. intros [| |]; simpl; tauto + apply abs_is_wnf. Qed.
Lemma   LOW_is_L_CBW :   LOW ⊆ L_CBW.
Proof. apply Uniform_monotone. intros [| |]; simpl; tauto + apply inert_is_wnf. Qed.
Lemma L_CBW_is_Weak : L_CBW ⊆ Weak.
Proof. apply Uniform_monotone. intros [| |]; simpl; tauto. Qed.
Lemma R_CBW_is_Weak : R_CBW ⊆ Weak.
Proof. apply Uniform_monotone. intros [| |]; simpl; tauto. Qed.
Lemma   LOW_is_Weak :   LOW ⊆ Weak.
Proof. apply Uniform_monotone. intros [| |]; simpl; tauto. Qed.

Lemma SDET_CBN_R_CBV : SDET == CBN ∩ R_CBV.
Proof.
  apply two_inclusions. split.
  { apply subset_of_intersection. split; [apply SDET_is_CBN|apply SDET_is_R_CBV]. }
  unfold flip. etransitivity.
  { apply two_inclusions. symmetry. apply Uniform_intersection. }
  apply Uniform_monotone.
  intros [|[]|]; cbv; tauto.
Qed.

Fixpoint GS_CBW (W :𝓟 context) (l r :𝓟 term) (C : context) : Prop :=
  W C ∨
  match C with
  | Lam _  :: C' =>  GS_CBW W l r C'
  (* | _         => RGS_CBW W l r C *)
  | Lapp t :: C' => l t ∧ GS_CBW W l r C'
  | Rapp t :: C' => RGS_CBW W l r C' ∧ r t
  | []           => False
  end
with RGS_CBW (W :𝓟 context) (l r :𝓟 term) (C : context) : Prop :=
  match C with
  | Lam _  :: C' => False
  | Lapp t :: C' => l t ∧ GS_CBW W l r C'
  | Rapp t :: C' => RGS_CBW W l r C' ∧ r t
  | []           => False
  end.

Lemma GS_CBW_grammar : ∀ W l r,
  GS_CBW W l r == W ∪ RGS_CBW W l r ∪ Lam_of (GS_CBW W l r).
Proof. intros ? ? ? [|[]]; split; union_tauto. Qed.

Lemma RGS_CBW_grammar : ∀ W l r,
  RGS_CBW W l r == Lapp_of l (GS_CBW W l r) ∪ Rapp_of (RGS_CBW W l r) r.
Proof. intros ? ? ? [|[]]; split; try union_tauto. Qed.

Definition  LL_CBW : 𝓟 context :=  GS_CBW L_CBW neu   wnf.
Definition RLL_CBW : 𝓟 context := RGS_CBW L_CBW neu   wnf.
Definition  RL_CBW : 𝓟 context :=  GS_CBW L_CBW inert nf.
Definition RRL_CBW : 𝓟 context := RGS_CBW L_CBW inert nf.
Definition  LR_CBW : 𝓟 context :=  GS_CBW R_CBW neu   wnf.
Definition RLR_CBW : 𝓟 context := RGS_CBW R_CBW neu   wnf.
Definition  RR_CBW : 𝓟 context :=  GS_CBW R_CBW inert nf.
Definition RRR_CBW : 𝓟 context := RGS_CBW R_CBW inert nf.
Definition    SCBW : 𝓟 context :=  GS_CBW  Weak inert wnf.
Definition   RSCBW : 𝓟 context := RGS_CBW  Weak inert wnf.

Lemma R_LL_CBW_is_SCBW : LL_CBW ⊆ SCBW ∧ RLL_CBW ⊆ RSCBW.
Proof.
  apply conj_forall.
  intros C. induction C as [|[| |] C [IH IHB]]; split; cbn; try tauto.
  + intros [[]|[]]; [left|right].
    - refine (conj I _). apply L_CBW_is_Weak, H0.
    - refine (conj _ H0). apply IHB, H.
  + intros [[]|[]]; [left|right; split].
    - refine (conj I _). apply L_CBW_is_Weak, H0.
    - apply neu_is_inert, H.
    - apply IH, H0.
  + intros []. split.
    - apply neu_is_inert, H.
    - apply IH, H0.
Qed.

Lemma R_RL_CBW_is_SCBW : RL_CBW ⊆ SCBW ∧ RRL_CBW ⊆ RSCBW.
Proof.
  apply conj_forall.
  intros C. induction C as [|[| |] C [IH IHB]]; split; cbn; try tauto.
  + intros [[]|[]]; [left|right; split].
    - refine (conj I _). apply L_CBW_is_Weak, H0.
    - apply IHB, H.
    - apply nf_is_wnf, H0.
  + intros []. split.
    - apply IHB, H.
    - apply nf_is_wnf, H0.
  + intros [[]|[]]; [left|right; split].
    - refine (conj I _). apply L_CBW_is_Weak, H0.
    - apply H.
    - apply IH, H0.
Qed.

Lemma R_LR_CBW_is_SCBW : LR_CBW ⊆ SCBW ∧ RLR_CBW ⊆ RSCBW.
Proof.
  apply conj_forall.
  intros C. induction C as [|[| |] C [IH IHB]]; split; cbn; try tauto.
  + intros [[]|[]]; [left|right].
    - refine (conj I _). apply R_CBW_is_Weak, H0.
    - refine (conj _ H0). apply IHB, H.
  + intros [[]|[]]; [left|right; split].
    - refine (conj I _). apply R_CBW_is_Weak, H0.
    - apply neu_is_inert, H.
    - apply IH, H0.
  + intros []. split.
    - apply neu_is_inert, H.
    - apply IH, H0.
Qed.

Lemma R_RR_CBW_is_SCBW : RR_CBW ⊆ SCBW ∧ RRR_CBW ⊆ RSCBW.
Proof.
  apply conj_forall.
  intros C. induction C as [|[| |] C [IH IHB]]; split; cbn; try tauto.
  + intros [[]|[]]; [left|right; split].
    - refine (conj I _). apply R_CBW_is_Weak, H0.
    - apply IHB, H.
    - apply nf_is_wnf, H0.
  + intros []. split.
    - apply IHB, H.
    - apply nf_is_wnf, H0.
  + intros [[]|[]]; [left|right; split].
    - refine (conj I _). apply R_CBW_is_Weak, H0.
    - apply H.
    - apply IH, H0.
Qed.

Lemma LL_CBW_is_SCBW : LL_CBW ⊆ SCBW.
Proof. exact (proj1 R_LL_CBW_is_SCBW). Qed.
Lemma LR_CBW_is_SCBW : LR_CBW ⊆ SCBW.
Proof. exact (proj1 R_LR_CBW_is_SCBW). Qed.
Lemma RL_CBW_is_SCBW : RL_CBW ⊆ SCBW.
Proof. exact (proj1 R_RL_CBW_is_SCBW). Qed.
Lemma RR_CBW_is_SCBW : RR_CBW ⊆ SCBW.
Proof. exact (proj1 R_RR_CBW_is_SCBW). Qed.

Lemma W_CBW_is_GS_CBW : ∀ W l r, W ⊆ GS_CBW W l r.
Proof. intros W l r C H. destruct C; left; exact H. Qed.

Lemma RGS_CBW_is_GS_CBW : ∀ W l r, RGS_CBW W l r ⊆ GS_CBW W l r.
Proof. intros W l r [|[]]; [|intros []| |]; try apply or_intror. Qed.

Lemma RGS_CBW_is_Rigid : ∀ W l r, l ⊆ rigid → RGS_CBW W l r ⊆ Rigid.
Proof.
  intros W l r rigid_l C; induction C as [|[]].
  + exact (const I).
  + exact id.
  + intros H. exact (IHC @@ proj1 H).
  + intros H. exact (rigid_l _ @@ proj1 H).
Qed.

Lemma Weak_GS_CBW : ∀ W l r,
  Hole ∪ Lapp_of l W ∪ Rapp_of W r ⊆ W  →  W ⊆ Weak →
  W == Weak ∩ GS_CBW W l r.
Proof.
  intros W l r W_grammar W_is_Weak.
  apply two_inclusions.
  refine (conj (λ C H, conj (W_is_Weak C H) (W_CBW_is_GS_CBW _ _ _ C H)) _).
  intros C. induction C; intros [H H0].
  { apply W_grammar. left. left. constructor. }
  destruct H0 as [H0|H0]; [exact H0|].
  destruct a.
  + contradiction (proj1 H).
  + apply W_grammar. right. simpl.
    refine (conj (proj2 H0) (IHC (conj (proj2 H) _))).
    apply RGS_CBW_is_GS_CBW, H0.
  + apply W_grammar. left. right. simpl.
    refine (conj (proj1 H0) (IHC (conj (proj2 H) (proj2 H0)))).
Qed.

Lemma GS_CBW_on_not_wnf : ∀ W l r C c,
  W ⊇ Hole ∪ Lapp_of l W ∪ Rapp_of W r →
  l ⊆ inert → r ⊆ wnf →
  ¬ wnf (plug C c) →
  GS_CBW W l r C →
  β_contrex c →
  W C.
Proof.
  intros W l r C c W_grammar l_is_inert r_is_wnf.
  induction C as [|[]]; intros H HC Hc.
  + apply W_grammar. left. left. constructor.
  + contradiction (H I).
  + destruct HC as [HC|HC]; [exact HC|].
    assert (H0 : ¬ wnf (plug C c)).
    { assert (l_is_rigid : l ⊆ rigid).
      { etransitivity; [exact l_is_inert|exact inert_is_rigid]. }
      apply (app_of_monotone _ _ (subset_full _) _ _ (subset_full _)),
            app_of_full, app_abs_disjoint,
            (Rigid_on_not_abstraction _ @@
              RGS_CBW_is_Rigid _ _ _ l_is_rigid _ @@ proj1 HC) in Hc.
      intros H0. apply wnf_grammar in H0.
      destruct H0; [exact (Hc H0)|].
      exact (H (conj H0 (r_is_wnf _ @@ proj2 HC))). }
    specialize (IHC H0 (RGS_CBW_is_GS_CBW _ _ _ _ @@ proj1 HC) Hc).
    apply W_grammar. right. exact (conj (proj2 HC) IHC).
  + destruct HC as [HC|HC]; [exact HC|].
    assert (H0 : ¬ wnf (plug C c)).
    { intros H0. apply H; clear H.
      refine (conj (l_is_inert _ (proj1 HC)) H0). }
    specialize (IHC H0 (proj2 HC) Hc).
    apply W_grammar. left. right. exact (conj (proj1 HC) IHC).
Qed.

Lemma GS_CBW_on_inert : ∀ W l r C c,
  ¬ W C →
  inert (plug C c) →
  GS_CBW W l r C →
  β_contrex c →
  RGS_CBW W l r C.
Proof.
  intros W l r C c WC H HC Hc.
  destruct C as [|[]].
  + apply inert_is_rigid in H.
    contradiction (rigid_β_contrex_disjoint _ H Hc).
  + exact H.
  + destruct HC as [HC|HC]; [contradiction (WC HC)|exact HC].
  + destruct HC as [HC|HC]; [contradiction (WC HC)|exact HC].
Qed.

Lemma L_CBW_is_LL_CBW : L_CBW ⊆ LL_CBW.
Proof. apply W_CBW_is_GS_CBW. Qed.
Lemma L_CBW_is_RL_CBW : L_CBW ⊆ RL_CBW.
Proof. apply W_CBW_is_GS_CBW. Qed.
Lemma R_CBW_is_LR_CBW : R_CBW ⊆ LR_CBW.
Proof. apply W_CBW_is_GS_CBW. Qed.
Lemma R_CBW_is_RR_CBW : R_CBW ⊆ RR_CBW.
Proof. apply W_CBW_is_GS_CBW. Qed.
Lemma Weak_is_SCBW : Weak ⊆ SCBW.
Proof. apply W_CBW_is_GS_CBW. Qed.

Theorem LL_CBW_non_uniform : Non_uniform LL_CBW.
Proof.
  intros [F H].
  enough (H0: LL_CBW [Rapp term_ω; Lam "x"]) by (cbn in H0; tauto).
  apply H. repeat split; eapply (H [_]); cbn; tauto.
Qed.

Theorem RL_CBW_non_uniform : Non_uniform RL_CBW.
Proof.
  intros [F H].
  enough (H0: RL_CBW [Rapp term_ω; Lam "x"]) by (cbn in H0; tauto).
  apply H. repeat split; eapply (H [_]); cbn; tauto.
Qed.

Theorem LR_CBW_non_uniform : Non_uniform LR_CBW.
Proof.
  intros [F H].
  enough (H0: LR_CBW [Rapp term_ω; Lam "x"]) by (cbn in H0; tauto).
  apply H. repeat split; eapply (H [_]); cbn; tauto.
Qed.

Theorem RR_CBW_non_uniform : Non_uniform RR_CBW.
Proof.
  intros [F H].
  enough (H0: RR_CBW [Rapp term_ω; Lam "x"]) by (cbn in H0; tauto).
  apply H. repeat split; eapply (H [_]); cbn; tauto.
Qed.

Theorem SCBW_non_uniform : Non_uniform SCBW.
Proof.
  intros [F H].
  enough (H0: SCBW [Rapp term_ω; Lam "x"]) by (cbn in H0; tauto).
  apply H. repeat split; eapply (H [_]); cbn; tauto.
Qed.

(** *** CBM *)

Fixpoint RR_CBM (C:context) : Prop :=
  match C with
  | []           => True
  | Rapp e :: C' => RR_CBM C' ∧ mnf e
  | Lapp e :: C' => RR_CBM C' ∨ (neu e ∧ R_CBM C')
  | _            => False
  end
with R_CBM (C:context) : Prop :=
  match C with
  | Lam _ :: C' => R_CBM C'
  (* _          => RR_CBM C *)
  | []           => True
  | Rapp e :: C' => RR_CBM C' ∧ mnf e
  | Lapp e :: C' => RR_CBM C' ∨ (neu e ∧ R_CBM C')
  end.

Lemma R_CBM_grammar : R_CBM == RR_CBM ∪ Lam_of R_CBM.
Proof. intros [|[]]; split; union_tauto. Qed.

Lemma RR_CBM_grammar : RR_CBM == Hole ∪ Lapp_of ● RR_CBM ∪ Rapp_of RR_CBM mnf ∪ Lapp_of neu R_CBM.
Proof.
  intros [|[]]; split; try union_tauto.
  intros [H|H].
  + left. left. right. repeat constructor. exact H.
  + right. exact H.
Qed.

Lemma R_CBV_is_RR_CBM : R_CBV ⊆ RR_CBM.
Proof.
  intros C. induction C as [|[]].
  + constructor.
  + intros [[] _].
  + intros [abs_t2 HC]. refine (conj (IHC HC) _).
    apply mnf_grammar. left. exact abs_t2.
  + intros [abs_t1 HC]. left. exact (IHC HC).
Qed.

Lemma RR_CBM_is_R_CBM : RR_CBM ⊆ R_CBM.
Proof. intros C. induction C as [|[]]; simpl; tauto. Qed.

Theorem R_CBM_non_uniform : Non_uniform R_CBM.
Proof.
  intros [F H].
  enough (H0: R_CBM [Rapp term_ω; Lam "x"]) by (cbn in H0; tauto).
  apply H. repeat split; eapply (H [_]); cbn; tauto.
Qed.

Lemma R_CBM_on_not_mnf : ∀ C c,
  ¬ mnf (plug C c) →
  R_CBM C →
  RR_CBM C.
Proof.
  induction C as [|[]]; intros c H HC.
  + constructor.
  + contradiction (H I).
  + exact HC.
  + destruct HC as [HC|HC]; [left|right]; exact HC.
Qed.

Fixpoint RL_CBM (C:context) : Prop :=
  match C with
  | []           => True
  | Rapp e :: C' => RL_CBM C'
  | Lapp e :: C' => (mnf e ∧ RL_CBM C') ∨ (neu e ∧ L_CBM C')
  | _            => False
  end
with L_CBM (C:context) : Prop :=
  match C with
  | Lam _ :: C' => L_CBM C'
  (* _          => RL_CBM C *)
  | []           => True
  | Rapp e :: C' => RL_CBM C'
  | Lapp e :: C' => (mnf e ∧ RL_CBM C') ∨ (neu e ∧ L_CBM C')
  end.

Lemma L_CBM_grammar : L_CBM == RL_CBM ∪ Lam_of L_CBM.
Proof. intros [|[]]; split; union_tauto. Qed.

Lemma RL_CBM_grammar : RL_CBM == Hole ∪ Rapp_of RL_CBM ● ∪ Lapp_of mnf RL_CBM ∪ Lapp_of neu L_CBM.
Proof.
  intros [|[]]; split; try union_tauto.
  intros H. left. left. right. repeat constructor. exact H.
Qed.

Lemma L_CBV_is_RL_CBM : L_CBV ⊆ RL_CBM.
Proof.
  intros C. induction C as [|[]].
  + constructor.
  + intros [[] _].
  + intros [_ HC]. exact (IHC HC).
  + intros [abs_t1 HC]. left. refine (conj _ (IHC HC)).
    apply mnf_grammar. left. exact abs_t1.
Qed.

Lemma RL_CBM_is_L_CBM : RL_CBM ⊆ L_CBM.
Proof. intros C. induction C as [|[]]; simpl; tauto. Qed.

Theorem L_CBM_non_uniform : Non_uniform L_CBM.
Proof.
  intros [F H].
  enough (H0: L_CBM [Rapp term_ω; Lam "x"]) by (cbn in H0; tauto).
  apply H. repeat split; eapply (H [_]); cbn; tauto.
Qed.

Lemma L_CBM_on_not_mnf : ∀ C c,
  ¬ mnf (plug C c) →
  L_CBM C →
  RL_CBM C.
Proof.
  induction C as [|[]]; intros c H HC.
  + constructor.
  + contradiction (H I).
  + exact HC.
  + destruct HC as [HC|HC]; [left|right]; exact HC.
Qed.

Fixpoint Rigid_CBM (C:context) : Prop :=
  match C with
  | []           => True
  | Rapp e :: C' => Rigid_CBM C'
  | Lapp e :: C' => Rigid_CBM C' ∨ (neu e ∧ CBM C')
  | _            => False
  end
with CBM (C:context) : Prop :=
  match C with
  | Lam _ :: C' => CBM C'
  (* _          => Rigid_CBM C *)
  | []           => True
  | Rapp e :: C' => Rigid_CBM C'
  | Lapp e :: C' => Rigid_CBM C' ∨ (neu e ∧ CBM C')
  end.

Lemma CBM_grammar : CBM == Rigid_CBM ∪ Lam_of CBM.
Proof. intros [|[]]; split; union_tauto. Qed.

Lemma Rigid_CBM_grammar :
  Rigid_CBM == Hole ∪ Rapp_of Rigid_CBM ● ∪ Lapp_of ● Rigid_CBM ∪ Lapp_of neu CBM.
Proof.
  intros [|[]]; split; try union_tauto.
  + intros H. left. left. right. repeat constructor. exact H.
  + intros [H|H].
    - left. right. repeat constructor. exact H.
    - right. exact H.
Qed.

Lemma R_CBM_is_CBM_aux : RR_CBM ⊆ Rigid_CBM ∧ R_CBM ⊆ CBM.
Proof.
  apply conj_forall.
  intros C. induction C as [|[| |]]; split; try tauto.
  + intros [HC _]. apply IHC, HC.
  + intros HC. apply IHC, HC.
  + intros [HC|[neu_t1 HC]]; [left|right].
    - apply IHC, HC.
    - refine (conj neu_t1 _). apply IHC, HC.
  + intros [HC|[neu_t1 HC]]; [left|right].
    - apply IHC, HC.
    - refine (conj neu_t1 _). apply IHC, HC.
Qed.

Lemma R_CBM_is_CBM : R_CBM ⊆ CBM.
Proof. exact (proj2 R_CBM_is_CBM_aux). Qed.

Lemma L_CBM_is_CBM_aux : RL_CBM ⊆ Rigid_CBM ∧ L_CBM ⊆ CBM.
Proof.
  apply conj_forall.
  intros C. induction C as [|[| |]]; split; try tauto.
  + intros [HC|[neu_t1 HC]]; [left|right].
    - apply IHC, HC.
    - refine (conj neu_t1 _). apply IHC, HC.
  + intros [HC|[neu_t1 HC]]; [left|right].
    - apply IHC, HC.
    - refine (conj neu_t1 _). apply IHC, HC.
Qed.

Lemma L_CBM_is_CBM : L_CBM ⊆ CBM.
Proof. exact (proj2 L_CBM_is_CBM_aux). Qed.

Lemma Weak_is_Rigid_CBM : Weak ⊆ Rigid_CBM.
Proof.
  intros C. induction C as [|[]].
  + constructor.
  + intros [[] _].
  + intros [_ H]. apply IHC, H.
  + intros [_ H]. left. apply IHC, H.
Qed.

Lemma Rigid_CBM_is_CBM : Rigid_CBM ⊆ CBM.
Proof. intros C. induction C as [|[]]; simpl; tauto. Qed.

Theorem CBM_non_uniform : Non_uniform CBM.
Proof.
  intros [F H].
  enough (H0: CBM [Rapp term_ω; Lam "x"]) by (cbn in H0; tauto).
  apply H. repeat split; eapply (H [_]); cbn; tauto.
Qed.

Lemma CBM_on_not_mnf : ∀ C c,
  ¬ mnf (plug C c) →
  CBM C →
  Rigid_CBM C.
Proof.
  induction C as [|[]]; intros c H HC.
  + constructor.
  + contradiction (H I).
  + exact HC.
  + destruct HC as [HC|HC]; [left|right]; exact HC.
Qed.

(** *** uniformness *)

Theorem uniform_strategies : ∀ C, In C [
    SDET;     CBN;
    R_CBV;    L_CBV;
    R_CBW;    L_CBW;    LOW;
    Weak;
                                HS;
    RI;       LI;
    ●
] → ∃ F, C == Uniform F.
Proof.
  intros C [H|[H|[H|[H|[H|[H|[H|[H|[H|[H|[H|[H|[]]]]]]]]]]]]]; subst;
  try (eexists; unfold SDET, CBN, R_CBV, L_CBV, R_CBW, L_CBW, LOW, Weak, HS, RI, LI; reflexivity).
  exists ●.
  intros C.
  split; intros _; [|constructor].
  induction C; [constructor|]. exact (conj I IHC).
Qed.

Theorem non_uniform_strategies : ∀ C,
  In C [Head; LO; LS; LL_CBW; LR_CBW; RL_CBW; RR_CBW; SCBW; L_CBM; R_CBM; CBM] → ¬ ∃ F, C == Uniform F.
Proof.
  intros C [H|[H|[H|[H|[H|[H|[H|[H|[H|[H|[H|[]]]]]]]]]]]]; subst.
  + exact Head_non_uniform.
  + exact LO_non_uniform.
  + exact LS_non_uniform.
  + exact LL_CBW_non_uniform.
  + exact LR_CBW_non_uniform.
  + exact RL_CBW_non_uniform.
  + exact RR_CBW_non_uniform.
  + exact SCBW_non_uniform.
  + exact L_CBM_non_uniform.
  + exact R_CBM_non_uniform.
  + exact CBM_non_uniform.
Qed.
