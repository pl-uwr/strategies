export GTK_IM_MODULE=uim
coqide -R . Top \
main/Preliminaries.v \
main/Common.v \
main/Term.v \
main/Context.v \
main/Decomposition.v \
main/Phased.v \
main/Decider.v \
main/PhasedForm.v \
main/Determinism.v \
main/Decompose.v

